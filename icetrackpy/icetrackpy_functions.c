// to compile :  gcc -fPIC -shared -o icetrackpy_functions.co icetrackpy_functions.c  -llapacke -lblas 
/* sudo apt-get install build-essential
sudo apt-get install liblapack*
sudo apt-get install libblas 
sudo apt-get install liblapack-dev
*/


#include <stdio.h>
#include <stdlib.h>
#include <cblas.h>
#include <math.h>
#include <lapacke.h>

// tetra nodes in wedge
int tetra_shape[12]={0,1,5,2,0,5,4,3,0,1,5,4};

// whnge of base from tetra to wedge
float tetra_1[16]={1,0,0,1,0,0,1,0,0,1,1,1,1,1,1,1};
float tetra_2[16]={1,0,0,0,0,1,0,1,0,0,1,1,1,1,1,1};

float N_mat[16]={1,0,0,0,0,1,0,0,0,0,1,0,-1,-1,-1,1};
		
int size_C[2]={4,4};
int size_B[2]={4,1};
int size_C3[2]={3,3};




int* point_to_wedge(int *mesh_wedges, int nb_points, int nb_wedges) {
	// this function gives out the list of wedges that are connected to  a given point, this version is made for single core use
   	int *points_to_wedge= (int*) malloc((20*nb_points)*sizeof(int));	

	for (int point_id=0;point_id<nb_points;point_id++){
		int pointer=0;
		for (int wedge_id=0;wedge_id<nb_wedges;wedge_id++){
			if (point_id == mesh_wedges[wedge_id*6+0] || point_id == mesh_wedges[wedge_id*6+1] || point_id == mesh_wedges[wedge_id*6+2] || point_id == mesh_wedges[wedge_id*6+3] || point_id == mesh_wedges[wedge_id*6+4] || point_id == mesh_wedges[wedge_id*6+5]){ 
  				points_to_wedge[point_id*20+pointer]=wedge_id;
				pointer++;
			}
		}
		while (pointer < 20){
			points_to_wedge[point_id*20+pointer]=-1;
			pointer++;
		}
	}
        return points_to_wedge;
}

int* point_to_wedge_parallel(int *mesh_wedges,  int start_point, int end_point,int nb_points,int nb_wedges) {
	// this function gives out the list of wedges that are connected to  a given point, this version is made for multi core use
   	int *points_to_wedge= (int*) malloc((20*(nb_points))*sizeof(int));	

	for (int point_id=0;point_id<nb_points;point_id++){
		int pointer=0;
		for (int wedge_id=0;wedge_id<nb_wedges;wedge_id++){
			if ((int) point_id + start_point == mesh_wedges[wedge_id*6+0] || (int) point_id + start_point == mesh_wedges[wedge_id*6+1] || (int) point_id + start_point == mesh_wedges[wedge_id*6+2] || (int) point_id + start_point == mesh_wedges[wedge_id*6+3] || (int) point_id + start_point == mesh_wedges[wedge_id*6+4] || (int) point_id + start_point == mesh_wedges[wedge_id*6+5]){ 
  				points_to_wedge[(int) point_id*20+pointer]=wedge_id;
				pointer++;
			}
		}
		while (pointer < 20){
			points_to_wedge[point_id*20+pointer]=-1;
			pointer++;
		}
	}
    return points_to_wedge;
}

void free_int(int* a){
	// this function frees a int from memory
	free(a);
}


float *mat_mult_lapack(float *A, float *B,int *size_A, int *size_B, float *Rez){
	// this functions uses lapack tu multiplie 2 matrices 

    int M = size_A[0];
    int N = size_B[1];
    int K = size_A[1];
    double ALPHA = 1.0;
    int LDA = size_A[1];
    int LDB = size_B[1];
   	double BETA = 0.0;
    double C[(int) size_A[0]*size_B[1]];
    int LDC = size_B[1];

	double A_d[(int) size_A[0]*size_A[1]];
	double B_d[(int) size_B[0]*size_B[1]];


	for(int i = 0; i< size_A[0]*size_A[1];i++){A_d[i]=(double) A[i];}
	for(int i = 0; i< size_B[0]*size_B[1];i++){B_d[i]=(double) B[i];}
	
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,M,N,K,ALPHA,A_d,LDA,B_d,LDB,BETA,C,LDC);

	for(int i = 0; i< size_A[0]*size_B[1];i++){Rez[i]=(float) C[i];}

	return Rez;
}


float* solve_lapack(float *M,float *b){
	// this function solves a linear system using lapack

   	int N= 4;
	int NRHS= 1;
	int LDA = N;
	int LDB= NRHS;

	int n = N, nrhs = NRHS, lda = LDA, ldb = LDB;
	int ipiv[N];

	LAPACKE_sgesv(LAPACK_ROW_MAJOR,n,nrhs,M,lda,ipiv,b,ldb);

	return b;
}

float score(float *A, float *X,float *point){
	// this function calculates the score like in the python version

	float pA[4]={0,0,0,1};
	float *rpA= (float*) malloc((4)*sizeof(float));
	float old_pA=0;

	if (A[0]>=0 && A[1]>=0 && A[2]>=2 && (1-A[0]-A[1]-A[2])<0 ){
		pA[0]=(1+2*A[0] -A[1] - A[2])/3;
		pA[1]=(1-A[0] +2*A[1] - A[2])/3;
		pA[2]=(1-A[0] -A[1] +2*A[2])/3;	
		if( (pA[0]>=0 && pA[1]>=0 && pA[2]<0) || (pA[0]<0 && pA[1]>=0 && pA[2]<0) ){
			old_pA=pA[0];
			pA[0]=(1+pA[0]-pA[1])/2;
			pA[1]=(1-old_pA+pA[1])/2;
			pA[2]=0;
			if (pA[0]<0){
				pA[0]=0;
				pA[1]=1;
				pA[2]=0;
			}
			if (pA[1]<0){
				pA[0]=1;
				pA[1]=0;
				pA[2]=0;
			}
		}
		if( (pA[0]<0 && pA[1]>=0 && pA[2]>=0) || (pA[0]<0 && pA[1]<0 && pA[2]>=0) ){
			old_pA=pA[1];
			pA[0]=0;
			pA[1]=(1+pA[1]-pA[2])/2;
			pA[2]=(1-old_pA+pA[2])/2;
			if (pA[1]<0){
				pA[0]=0;
				pA[1]=0;
				pA[2]=1;
			}
			if (pA[2]<0){
				pA[0]=0;
				pA[1]=1;
				pA[2]=0;
			}
		}
		if( (pA[0]>=0 && pA[1]<0 && pA[2]>=0) || (pA[0]>=0 && pA[1]<0 && pA[2]<0) ){
			old_pA=pA[0];
			pA[0]=(1+pA[0]-pA[2])/2;
			pA[1]=0;
			pA[2]=(1-old_pA+pA[2])/2;
			if (pA[0]<0){
				pA[0]=0;
				pA[1]=0;
				pA[2]=1;
			}
			if (pA[2]<0){
				pA[0]=1;
				pA[1]=0;
				pA[2]=0;
			}
		}
	}
	else if(A[0] < 0 && A[1]>=0 && A[2]>=0){
		pA[0]=0;
		pA[1]=fmaxf(A[1],0);
		pA[2]=fmaxf(A[2],0);
		if ((1-pA[1])<pA[2]){	
			old_pA=pA[1];
			pA[0]=0;
			pA[1]=(1+pA[1]-pA[2])/2;
			pA[2]=(1-old_pA+pA[2])/2;
		}
	}
	else if(A[0] >= 0 && A[1]<0 && A[2]>=0){
		pA[0]=fmaxf(A[0],0);
		pA[1]=0;
		pA[2]=fmaxf(A[2],0);
		if ((1-pA[0])<pA[2]){
			old_pA=pA[0];
			pA[0]=(1+pA[0]-pA[2])/2;
			pA[1]=0;
			pA[2]=(1-old_pA+pA[2])/2;
		}
	}
	else if(A[0] >= 0 && A[1]>=0 && A[2]<0){
		pA[0]=fmaxf(A[0],0);
		pA[1]=fmaxf(A[1],0);
		pA[2]=0;
		if ((1-pA[0])<pA[1]){
			old_pA=pA[0];
			pA[0]=(1+pA[0]-pA[1])/2;
			pA[1]=(1-old_pA+pA[1])/2;
			pA[2]=0;
		}
	}
	else {
		pA[0]=0;
		pA[1]=0;
		pA[2]=0;
	}

	float *rez_mat_mult_1= (float*) malloc(( (int) size_C[0]*size_C[1])*sizeof(float));
	mat_mult_lapack(X,N_mat,size_C,size_C,rez_mat_mult_1);

	mat_mult_lapack(rez_mat_mult_1,pA,size_C,size_B,rpA);

	float rez =  (float) sqrt(pow((double) (rpA[0]-point[0]),2)+ pow((double) (rpA[1]-point[1]),2)+ pow((double) (rpA[2]-point[2]),2)) ;
	
	free(rez_mat_mult_1);
	free(rpA);

	return  rez;
}

float* is_inside_wedge(int *mesh_wedges, float *mesh_points,float *point,int  id_wedge){
	// this function tells if a point is inside a given wedge, gives the local coordinates and the score

   	float *return_list= (float*) malloc((5)*sizeof(float));
	float *A= (float*) malloc((4)*sizeof(float));
	float *rez_mat_mult= (float*) malloc(( (int) size_C[0]*size_C[1])*sizeof(float));

	return_list[4]=0;

	for(int tetra =0; tetra<3;tetra++){
		float X[16]={0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1};

		for(int i=0;i<4;i++){
			for(int j=0;j<3;j++){
				X[4*j+i]=mesh_points[(int) 3*(mesh_wedges[(int) 6*id_wedge+(tetra_shape[4*tetra+i])])+j];	
			}
		}
		
		float A_loc[4]={point[0],point[1],point[2],1};	
		
		mat_mult_lapack(X,N_mat,size_C,size_C,rez_mat_mult);
		solve_lapack(rez_mat_mult,A_loc);
		
		if ((A_loc[0]>=0) && (A_loc[1]>=0) && (A_loc[2]>=0) && (1-A_loc[0]-A_loc[1]-A_loc[2]>=0 )  ){
		
			if (tetra == 0){
				
				A[0]=A_loc[0];
				A[1]=A_loc[1];
				A[2]=A_loc[2];
				A[3]=A_loc[3];
				
				A[2]=2*A[2]-1;
				
				return_list[0]=1.0;
				return_list[1]=A[0];
				return_list[2]=A[1];
				return_list[3]=A[2];
				return_list[4]=0.0;
				return return_list;
			}
			if (tetra == 1){

				mat_mult_lapack(tetra_1,N_mat,size_C,size_C,rez_mat_mult);
				mat_mult_lapack(rez_mat_mult,A_loc,size_C,size_B,A);
				
				A[2]=2*A[2]-1;
				
				return_list[0]=1.0;
				return_list[1]=A[0];
				return_list[2]=A[1];
				return_list[3]=A[2];
				return_list[4]=0.0;
				return return_list;
			}
			if (tetra == 2){
				mat_mult_lapack(tetra_2,N_mat,size_C,size_C,rez_mat_mult);
				mat_mult_lapack(rez_mat_mult,A_loc,size_C,size_B,A);

				A[2]=2*A[2]-1;
				
				return_list[0]=1.0;
				return_list[1]=A[0];
				return_list[2]=A[1];
				return_list[3]=A[2];
				return_list[4]=0.0;
				return return_list;
			}
		}
		else {
			float prism_score=score(A_loc,X,point);
			if (tetra == 0){	
				A[0]=A_loc[0];
				A[1]=A_loc[1];
				A[2]=A_loc[2];
				A[3]=A_loc[3];
				
				A[2]=2*A[2]-1;
				
				return_list[0]=0.0;
				return_list[1]=A[0];
				return_list[2]=A[1];
				return_list[3]=A[2];
				return_list[4]=prism_score;
			}
			if ((tetra == 1) && (prism_score < return_list[4])){
				mat_mult_lapack(tetra_1,N_mat,size_C,size_C,rez_mat_mult);
				mat_mult_lapack(rez_mat_mult,A_loc,size_C,size_B,A);
							
				A[2]=2*A[2]-1;
				
				return_list[0]=0.0;
				return_list[1]=A[0];
				return_list[2]=A[1];
				return_list[3]=A[2];
				return_list[4]=prism_score;
			}
			if ((tetra == 2) && (prism_score < return_list[4])){
				mat_mult_lapack(tetra_2,N_mat,size_C,size_C,rez_mat_mult);
				mat_mult_lapack(rez_mat_mult,A_loc,size_C,size_B,A);


				A[2]=2*A[2]-1;
				
				return_list[0]=0.0;
				return_list[1]=A[0];
				return_list[2]=A[1];
				return_list[3]=A[2];
				return_list[4]=prism_score;
			}
		}
		
	}
	free(A);
	free(rez_mat_mult);
	return  return_list;
}

void free_float(float* a){
	// this function frees a float from memory

	free(a);
}


int find_best_sommet(int *mesh_wedges, float *mesh_points, int *point_wedge_list, float *coord_objectif, int start_wedge){
	// this function finds the best node which is the nearest to a given point

   	int best_sommet = 0;
	int old_best_sommet =0;
	int iter=0;
	float dist;
	float min_dist_loc;

	while((best_sommet != old_best_sommet) || (iter<=2)){
		old_best_sommet=best_sommet;
		iter+=1;
		min_dist_loc=0;
		for(int i =0; i<6; i++){
			dist=0;
			for (int j=0;j<3;j++){ dist = dist + fabs(mesh_points[3*mesh_wedges[6*start_wedge+i]+j] -coord_objectif[j]); }

			if( i==0 || dist <min_dist_loc){   
				min_dist_loc=dist;
				best_sommet=mesh_wedges[6*start_wedge+i];
			}		
		}

		min_dist_loc=0;
		for(int i=0;i<20;i++){
			dist=0;
			if(point_wedge_list[20*best_sommet+i]!=-1){
				for(int j=0;j<6;j++){
					for (int k =0; k<3;k++){ dist = dist + (fabs(  mesh_points[3*mesh_wedges[ 6*point_wedge_list[20*best_sommet+i]+j]+k] -coord_objectif[k]))/6;}			
				}	

				if ((i==0) || (dist<min_dist_loc)){
					min_dist_loc=dist;
					start_wedge=point_wedge_list[20*best_sommet+i];
				}
			}
		}
	}
	return best_sommet;
}

