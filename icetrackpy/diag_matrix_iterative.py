import numpy as np
from numpy.polynomial import Polynomial
from scipy import linalg as la
from scipy.optimize import fsolve


def eig_DM(D:np.array,M:np.array):
    """
    This functions  allows to find a good approximation of the eigne values and eigen vectors of the product D by M where the determinant of D and M or 1 and D is diagonal 
    
    :param D: Diagonal matrix of size 3 and of determiant equal to 1
    :type D: np.array
    :param M: Matrix of size 3 by 3 that should be symmetric and of determinant 1 
    :type M: np.array
    :return: gives out the eigen values and eigen vectors of the product DM
    :rtype: np.array,np.array
    """

    if np.allclose(M,M.T,10e-10)==False:
        return False

    # Reordering the matrix to make sur that that the values in D or order properly and do the same permuttation on the matrix M so every hting is consistent
    for i in range(3-2):
        for i in range(3-1):
            if D[i,i] > D[i+1,i+1]:
                D[i,i], D[i+1,i+1]=D[i+1,i+1], D[i,i]

                M[i,0],M[i+1,0],M[i,1],M[i+1,1],M[i,2],M[i+1,2]=M[i+1,0],M[i,0],M[i+1,1],M[i,1],M[i+1,2],M[i,2]
                M[0,i],M[0,i+1],M[1,i],M[1,i+1],M[2,i],M[2,i+1]=M[0,i+1],M[0,i],M[1,i+1],M[1,i],M[2,i+1],M[2,i]


    # We calculate the values of the carracteristic polynomial
    a=sum([D[i,i]*M[i,i] for i in range(3)])
    b=D[2,2]*D[1,1]*(M[2,2]*M[1,1]-M[2,1]*M[1,2])+D[2,2]*D[0,0]*(M[2,2]*M[0,0]-M[2,0]*M[0,2])+D[0,0]*D[1,1]*(M[0,0]*M[1,1]-M[0,1]*M[1,0])
    bda=D[1,1]*(M[1,1] - (M[2,1]**2)/M[2,2])+D[0,0]*(M[0,0] - (M[2,0]**2)/M[2,2])

    eigval=np.zeros(3)
    eigvect=np.zeros([3,3])

    if a!=0:
        p=Polynomial([1/a,-bda,1])
        # p=Polynomial([la.det(D@M)/a,-bda,1])
        l1p,l2p=np.sort(p.roots().real)
    else :
        l1p,l2p=0,0

    p=Polynomial([-1,b,-a,1])
    # p=Polynomial([-la.det(D@M),b,-a,1])
    l1,l2,l3=np.sort(p.roots().real)

    if abs(p(l1))>abs(p(l1p)):
        eigval[0]=l1p
    else :
        eigval[0]=l1

    if abs(p(l2))>abs(p(l2p)):
        eigval[1]=l2p
    else :
        eigval[1]=l2

    eigval[2]=l3

    # minimisation that is used to find the eigen vectors, vector is equal to 0 when X is the eigen vector
    def f_to_solve(X,i):
        return abs(D @ M @X - eigval[i]*X) + (1/3)*abs(la.norm(X)-1)*np.ones(3)

    # approximation of the jacobian of f_to_solve to make the computation facter
    def jac_f_to_solve(X,i):
        dx1=D@M[:,0]-eigval[i]*np.array([1,0,0])+2*X[0]*np.ones(3)*(1/3)
        dx2=D@M[:,1]-eigval[i]*np.array([0,1,0])+2*X[1]*np.ones(3)*(1/3)
        dx3=D@M[:,2]-eigval[i]*np.array([0,0,1])+2*X[2]*np.ones(3)*(1/3)
        return [dx1,dx2,dx3]

    # utalization of the already implement python algo to find a good starting point
    good_approx=la.eigh(D@M)[1]

    # looping over the eigen values to get the eigen vectors 
    for i in range(3):
        eigvect[:,i]=fsolve(f_to_solve,good_approx[:,i],args=(i,),fprime=jac_f_to_solve,xtol=0.5,full_output=0)

    # orthogonalisation of the vectors
    eigvect,__=la.qr(eigvect)

    # computation of the error that has beeen made and giving out warning messages
    error=la.norm(D@M - (eigvect) @ np.diag(eigval) @ (eigvect).T)/la.norm(D@M)
    if error>0.1:
        print("Warning : The error of the diagonalized matrix is high")
        print("Relative error is : " +str(error))

    return eigval.real,eigvect.real


def int_strain(strain_list:list):
    """
    Calculates the cumulated strain from a list of elementry small strains that should be summed in the incomrpessible case
    
    :param strain_list: list of strain tensor (represented by 3 by 3 arrays) idealy they are incompressible strain rates tensor multiplied by a small step time
    :type strain_list: list
    :return: returns a list of eigen values and an orthogonal matrix, when taking the product PDP^T we get the cumulated strain 
    :rtype: np.array,np.array
    """

    # cheking that the shape is ok
    if np.shape(strain_list[0])!=(3,3) and np.allclose(strain_list[0],strain_list[0].T,10e-10)==False:
        print("Bad shape or not symmetric or strain is not an array")
        return False

    len_path=len(strain_list)

    # computing the deformation tensor 
    F_list=[la.sqrtm(2*eps + np.eye(3)) for eps in strain_list]

    # making shure that we are incompressible in the big deformation definiition 
    for i in range(len_path):
        F_list[i]=F_list[i]/(la.det(F_list[i])**(1/3))

    D_tot=np.eye(3)
    P_tot=np.eye(3)

    D_list=[]
    P_list=[]

    # calculating interatively the eigen values and eigen vectors
    for i in range(len_path):
        M=P_tot.T @ F_list[i] @ P_tot

        eig_vals,eig_vects=eig_DM(D_tot, M)

        D_tot=np.diag(eig_vals)
        P_tot=P_tot @ eig_vects

        D_list.append(np.diag(D_tot))
        P_list.append(P_tot)

    return [(1/2)*((D**2) -np.ones(3))for D in D_list],P_list









