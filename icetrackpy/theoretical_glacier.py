import meshio # librairy to creat the vtu files 
import numpy as np


class Glacier:
    '''
    This class is able to creat a theoretical glacier where all of the values of tensors fields are mathematically known.

    '''

    def __init__(self,vx=1.0,vz=-1.0,mesh_factor=[1,1,1],square_size=[50,5*2,10],type="3d"):
        '''
        The functions creats an object of Glacier.

        
        :param vx: velocity of the particule in the x direction, (m/year)
        :type vx: float

        :param vz: velocity of the particule in the z direction, (m/year)
        :type vz: float

        :param mesh_factor: compaction of the mesh in the 3 directions in numbers of nodes for 1 meter,  should be a list of size 3 of int types elements bigger or equal to 1. [Default] :code:'[1,1,1]'
        :type mesh_factor: list

        :param square_size: size of the 3D volume where the particules will evolve, should be a list of size 3 of floats, the volume cant be negative or null. [Default] :code:'[50,10,10]'
        :type square_size: list

        :param type: type of glacier that will be used for creating the fields on the mesh, 2 options are available, "compression" or "3d", the first is juste a single bar in compression and the second one is a more realistic approximation of a glacier 
        :type type: str

        '''
        
        self.type=type
        self.vx=vx 
        self.vz=vz
        self.mesh_factor=mesh_factor
        self.square_size=square_size

                # number of elements in every directions
        nb_elt=[self.mesh_factor[0]*(self.square_size[0]+1),self.mesh_factor[1]*(self.square_size[1]+1),self.mesh_factor[2]*(self.square_size[2]+1)]

        x=np.linspace(0,self.square_size[0],nb_elt[0])
        y=np.linspace(0,self.square_size[1],nb_elt[1])
        z=np.linspace(0,self.square_size[2],nb_elt[2])

        if self.type=="compression":
            offset=self.square_size[2]/2
        if self.type=="3d":
            offset=0

        # creations of all of the nodes 
        self.points=[]
        for k in range(nb_elt[2]):
            for j in range(nb_elt[1]):
                for i in range(nb_elt[0]):
                    self.points.append([x[i],y[j]-self.square_size[1]/2,z[k]-offset])
        self.points = np.array(self.points)

        # creation of the list of triangles at the bottom of the cube
        self.triangle_bottom=[]
        for j in range(nb_elt[1]-1):
            for i in range(nb_elt[0]-1):
                self.triangle_bottom.append([i+j*nb_elt[0],i+1+j*nb_elt[0],i+(j+1)*nb_elt[0] ])
                self.triangle_bottom.append([i+1+j*nb_elt[0],i+1+(j+1)*nb_elt[0],i+(j+1)*nb_elt[0] ])

        # creation of the wedges 
        self.wedge=[]
        for k in range(nb_elt[2]-1):
            for t in range(2*(nb_elt[1]-1)*(nb_elt[0]-1)):
                self.wedge.append([self.triangle_bottom[t][0]+nb_elt[1]*nb_elt[0]*k,self.triangle_bottom[t][1]+nb_elt[1]*nb_elt[0]*k,self.triangle_bottom[t][2] +nb_elt[1]*nb_elt[0]*k,self.triangle_bottom[t][0]+nb_elt[1]*nb_elt[0]*(k+1),self.triangle_bottom[t][1]+nb_elt[1]*nb_elt[0]*(k+1),self.triangle_bottom[t][2] +nb_elt[1]*nb_elt[0]*(k+1)])

        # creation of the list of triangles at the top of the cube
        self.triangle_top=[]
        for t in range(2*(nb_elt[1]-1)*(nb_elt[0]-1)):
            self.triangle_top.append([self.triangle_bottom[t][0]+nb_elt[1]*nb_elt[0]*(nb_elt[2]-1),self.triangle_bottom[t][1]+nb_elt[1]*nb_elt[0]*(nb_elt[2]-1),self.triangle_bottom[t][2] +nb_elt[1]*nb_elt[0]*(nb_elt[2]-1)])

        # creation of the square elemetens on the sides 
        self.square_1=[]
        for j in range(nb_elt[2]-1):
            for i in range(nb_elt[0]-1):
                self.square_1.append([i+(nb_elt[1])*(nb_elt[0])*(j),  i+1 + (nb_elt[1])*(nb_elt[0])*(j),i+1+(nb_elt[1])*(nb_elt[0])*(j+1) , i+(nb_elt[1])*(nb_elt[0])*(j+1) ])
        
        # creation of the square elemetens on the sides 
        self.square_2=[]
        for j in range(nb_elt[2]-1):
            for i in range(nb_elt[0]-1):
                self.square_2.append([(nb_elt[1]-1)*(nb_elt[0])+i+(nb_elt[1])*(nb_elt[0])*(j), (nb_elt[1]-1)*(nb_elt[0])+ i+1 + (nb_elt[1])*(nb_elt[0])*(j),(nb_elt[1]-1)*(nb_elt[0])+i+1+(nb_elt[1])*(nb_elt[0])*(j+1) ,(nb_elt[1]-1)*(nb_elt[0])+ i+(nb_elt[1])*(nb_elt[0])*(j+1) ])

        # creation of the square elemetens on the sides 
        self.square_3=[]
        for j in range(nb_elt[2]-1):
            for i in range(nb_elt[1]-1):
                self.square_3.append([ i*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j),(i+1)*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j) ,(i+1)*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j+1),(i)*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j+1) ])

        # creation of the square elemetens on the sides 
        self.square_4=[]
        for j in range(nb_elt[2]-1):
            for i in range(nb_elt[1]-1):
                self.square_4.append([(nb_elt[0]-1)+ i*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j),(nb_elt[0]-1)+(i+1)*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j) ,(nb_elt[0]-1)+(i+1)*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j+1),(nb_elt[0]-1)+ (i)*nb_elt[0]+(nb_elt[1])*(nb_elt[0])*(j+1) ])

        # storage of the mesh elemnts 
        self.cells = {
            "triangle": np.array(self.triangle_bottom+self.triangle_top),
            "quad":np.array(self.square_1+self.square_2+self.square_3+self.square_4),
            'wedge': np.array(self.wedge),
            }
        
        pass


    def v(self,x:float,y:float,z:float):
        '''
        Gives out the velocity field at any place in the 3D space,this function is defined everywhere even out of the domain

        
        :param x: cordinate in the x axis to evaluate the velocity field
        :type x: float

        :param y: cordinate in the y axis to evaluate the velocity field
        :type y: float

        :param z: cordinate in the z axis to evaluate the velocity field
        :type z: float

        :return: gives out a tuple of floats of length 3 contaning the velocity filed in the x,y,z directions 
        :rtype: tuple       
        '''
        if self.type=="3d":
            return (self.vx,-self.vz*(y/self.square_size[2])*(1-2*x/self.square_size[0]),self.vz*(z/self.square_size[2])*(1-2*x/self.square_size[0]))

        if self.type=="compression":
            return (self.vx*x,-0.5*self.vx*y,-0.5*self.vx*(z))


        


    def strain_rate(self,x:float,y:float,z:float):
        '''
        Gives out the strain rate field at any place in the 3D space, this function is defined everywhere even out of the domain
        
        :param x: cordinate in the x axis to evaluate the strain rate field
        :type x: float

        :param y: cordinate in the y axis to evaluate the strain rate field
        :type y: float

        :param z: cordinate in the z axis to evaluate the strain rate field
        :type z: float

        :return: gives out a square matrix of size 3 which is the symmetric strain tensor
        :rtype: np.array 
        '''
        
    
        if self.type=="3d":
            D=np.zeros([3,3])
            D[0,0]=0
            D[1,1]=-(self.vz/self.square_size[2])*(1-2*x/self.square_size[0])
            D[2,2]=(self.vz/self.square_size[2])*(1-2*x/self.square_size[0])
            D[0,1]=D[1,0]=y*self.vz/(self.square_size[0]*self.square_size[2])
            D[0,2]=D[2,0]=-z*self.vz/(self.square_size[0]*self.square_size[2])
            D[1,2]=D[2,1]=0
            return np.array([[D[0,0],D[0,1],D[0,2]],[D[0,1],D[1,1],D[1,2]],[D[0,2],D[1,2],D[2,2]]])


        if self.type=="compression":
            D=np.zeros([3,3])
            D[0,0]=self.vx
            D[1,1]=-0.5*self.vx
            D[2,2]=-0.5*self.vx
            D[0,1]=D[1,0]=0
            D[0,2]=D[2,0]=0
            D[1,2]=D[2,1]=0
            return np.array([[D[0,0],D[0,1],D[0,2]],[D[0,1],D[1,1],D[1,2]],[D[0,2],D[1,2],D[2,2]]])

        


    def phi(self,x0:float,y0:float,z0:float,t:float):
        '''
        Gives out the position of a particule with an initial position at a given time, this function is defined everywhere even out of the domain
        
        :param x0: initial position of the particule in the x direction
        :type x0: float

        :param y0: initial position of the particule in the y direction
        :type y0: float

        :param z0: initial position of the particule in the z direction
        :type z0: float

        :param t: time at which we want to know the particule position can be positive of negative
        :type t: float

        :return: gives out a tuple of floats of size 3 giving out the 3 coordinates of the position of the particule  
        :rtype: tuple    
        '''
        if self.type=="3d":
            lnz=((self.square_size[0]*self.vz-2*self.vz*x0)*t -self.vz*self.vx*t**2)/(self.square_size[0]*self.square_size[2])
            return (self.vx*t+x0,y0*np.exp(-lnz),z0*np.exp(lnz))

        if self.type=="compression":
            return (x0*np.exp(self.vx*t),y0*np.exp(-0.5*self.vx*t),(z0)*np.exp(-0.5*self.vx*t))
        


    def strain(self,x0:float,y0:float,z0:float,t:float):
        '''
        Gives out the strain of a particule with an initial position at a given time, this function is defined everywhere even out of the domain
       
        
        :param x0: initial position of the particule in the x direction
        :type x0: float

        :param y0: initial position of the particule in the y direction
        :type y0: float

        :param z0: initial position of the particule in the z direction
        :type z0: float

        :param t: time at which we want to know the particule strain can be positive of negative
        :type t: float

        :return: gives out a matrix of size 3 representing the symmetric strain tensor 
        :rtype: np.array   

        '''

        if self.type=="3d":
            a=(2*self.vz*t/(self.square_size[0]*self.square_size[2]))
            b=((self.square_size[0]*self.vz-2*self.vz*x0)*t -self.vz*self.vx*t**2)/(self.square_size[0]*self.square_size[2])
            exx=a**2*(y0**2*np.exp(-b) + z0**2*np.exp(b))
            eyy=np.exp(-2*b)-1
            ezz=np.exp(2*b)-1
            exy=a*y0*np.exp(-2*b)
            eyz=0
            exz=-a*z0*np.exp(2*b)
            return 0.5*np.array([[exx,exy,exz],[exy,eyy,eyz],[exz,eyz,ezz]])

        if self.type=="compression":
            return 0.5*np.array([[np.exp(2*self.vx*t)-1,0,0],[0,np.exp(-self.vx*t)-1,0],[0,0,np.exp(-self.vx*t)-1]])
        
    

    def vtu(self,path:str,file_name="mesh"):
        '''
        Creats a .vtu file containing the mesh and the vlaues of strain rate and velocity at each nodes 
        
        :param path: path where the file needs to be saved 
        :type path: str

        :param file_name: name of the vtu file
        :type file_name: str

        .. note:: saves a vtu file at the path given and with the given file name     

        '''

        # creation of the list of velocities at each nodes 
        self.velocity=[]
        for elt in self.points:
            self.velocity.append(self.v(elt[0],elt[1],elt[2]))

        # creation of the list of strain rates at each nodes 
        exx,eyy,ezz,exy,exz,eyz=[],[],[],[],[],[]
        for elt in self.points:
            exx.append(self.strain_rate(elt[0],elt[1],elt[2])[0,0])
            eyy.append(self.strain_rate(elt[0],elt[1],elt[2])[1,1])
            ezz.append(self.strain_rate(elt[0],elt[1],elt[2])[2,2])
            exy.append(self.strain_rate(elt[0],elt[1],elt[2])[0,1])
            exz.append(self.strain_rate(elt[0],elt[1],elt[2])[0,2])
            eyz.append(self.strain_rate(elt[0],elt[1],elt[2])[1,2])

        # storage of the values at each nodes 
        point_data = {
            'velocity': np.array(self.velocity),
            'exx': np.array(exx),
            'eyy': np.array(eyy),
            'ezz': np.array(ezz),
            'exy': np.array(exy),
            'eyz': np.array(eyz),
            'exz': np.array(exz)
            }

        # writting the mesh to the vtu file
        meshio.write_points_cells(
            path+"/"+file_name+".vtu",
            self.points,
            self.cells,
            point_data=point_data,
        )
        pass

    def msh(self,path:str,file_name="mesh"):
        '''
        Creats a .msh file containing the mesh that can be converted as elmer-ice input  

        :param path: path where the file needs to be saved 
        :type path: str

        :param file_name: name of the vtu file
        :type file_name: str

        .. note:: saves a msh file at the path given and with the given file name  

        '''

        # erasing the old file content 
        open(path+"/"+file_name+".msh", 'w').close()
        # writing the begining the of the mesh file 
        f = open(path+"/"+file_name+".msh", "a")
        f.write("$MeshFormat\n")
        f.write("2.2 0 8\n")
        f.write("$EndMeshFormat\n")
        f.write("$Nodes\n")
        f.write(str(len(self.points))+"\n")

        # writting all nodes in the file
        for i  in range(len(self.points)):
            f.write(str(i+1)+" "+str(self.points[i][0])+" "+str(self.points[i][1])+" "+str(self.points[i][2])+"\n")

        f.write("$EndNodes\n")
        f.write("$Elements\n")
        f.write(str(len(self.cells["wedge"])+len(self.cells["quad"])+len(self.cells["triangle"]))+"\n")

        # writting the wedge elements in the file 
        count=0
        for i in range(len(self.cells["wedge"])):
            f.write(str(i+1)+" 6 2 0 1 " +str(self.cells["wedge"][i][0]+1)+" "+str(self.cells["wedge"][i][1]+1)+" "+str(self.cells["wedge"][i][2]+1)+" "+str(self.cells["wedge"][i][3]+1)+" "+str(self.cells["wedge"][i][4]+1)+" "+str(self.cells["wedge"][i][5]+1)+"\n")
        
        # writting the square elements in the file 
        count+=i+1
        for i in range(len(self.square_1)):
            f.write(str(i+1+count) +" 3 2 0 1 " +str(self.square_1[i][0]+1) +" "+str(self.square_1[i][1]+1) +" "+str(self.square_1[i][2]+1) +" "+str(self.square_1[i][3]+1)+"\n")
        # writting the square elements in the file
        count+=i+1
        for i in range(len(self.square_2)):
            f.write(str(i+1+count) +" 3 2 0 2 " +str(self.square_2[i][0]+1) +" "+str(self.square_2[i][1]+1) +" "+str(self.square_2[i][2]+1) +" "+str(self.square_2[i][3]+1)+"\n")
        # writting the square elements in the file
        count+=i+1
        for i in range(len(self.square_3)):
            f.write(str(i+1+count) +" 3 2 0 3 " +str(self.square_3[i][0]+1) +" "+str(self.square_3[i][1]+1) +" "+str(self.square_3[i][2]+1) +" "+str(self.square_3[i][3]+1)+"\n")
        # writting the square elements in the file
        count+=i+1
        for i in range(len(self.square_4)):
            f.write(str(i+1+count) +" 3 2 0 4 " +str(self.square_4[i][0]+1) +" "+str(self.square_4[i][1]+1) +" "+str(self.square_4[i][2]+1) +" "+str(self.square_4[i][3]+1)+"\n")

        # writting the triangle elements in the file
        count+=i+1
        for i in range(len(self.triangle_bottom)):
            f.write(str(i+1+count) +" 2 2 0 5 " +str(self.triangle_bottom[i][0]+1) +" "+str(self.triangle_bottom[i][1]+1) +" "+str(self.triangle_bottom[i][2]+1) +"\n")
        # writting the triangle elements in the file
        count+=i+1
        for i in range(len(self.triangle_top)):
            f.write(str(i+1+count) +" 2 2 0 6 " +str(self.triangle_top[i][0]+1) +" "+str(self.triangle_top[i][1]+1) +" "+str(self.triangle_top[i][2]+1) +"\n")

        f.write("$EndElements")
        f.close()

        pass