import meshio  # usefull to  open the mesh
import pyvista as pv # usfull to open pvtu files 
import numpy as np   
import colorsys  # to convert colors and creat colowheels
import sys # usfull to import other python script
import os # usfull to import other python script
import ctypes #usfull to run the compiled code in c for some functions
import math 
import requests # to add to requierement
import rasterio # map background
from rasterio import warp

import matplotlib.pyplot as plt  # used for colorbars
import matplotlib.colors as colorm # used for color plotting

import multiprocessing as mp  # used to multiprocess time intensive tasks 


from vispy import app, scene,visuals,gloo,io,color  # used for visualisation

from scipy.signal import savgol_filter  # used to filter the angles of the rotation matrix during the path
from scipy import linalg as la 


path=os.path.dirname(os.path.abspath(__file__)) # finding the local path to get the the other python scripts
os.environ["XDG_SESSION_TYPE"] = "xcb" # avoids a warning due to vispy

sys.path.append(path) # positionning in the local repository 

from icetrackpy import diag_matrix_iterative as diagm # importing other parts of icetrackpy for strain calculation

# findind the compiled file with the compiled functions, an error may occure if there is more than one file with the same carracteristics in the repository
files=os.listdir(path)
so_file=None
for file in files :
    if file.startswith("icetrackpy_functions.") and file.endswith(".so"):
        so_file=path+"/"+file

# importing the functions in the c file
c_function = ctypes.CDLL(so_file)


def pvtu_to_vtu(file_path):
    """
    This function converts a pvtu file into a vtu file that can later be read by the library

    :param file_path: it can be a string of the complet path of a single file or a list of files names
    :type file_path: str or list

    :return: save the files with the same names as before but in the vtu format, and it retruns a list of the new files names 
    :rtype: list

    """

    if type(file_path)==str:
        file_path=[file_path]

    out_file_list=[]

    for file in file_path:
        mesh=pv.read(file)
        mesh.save(file[:-4]+"vtu")
        out_file_list.append(file[:-4]+"vtu")

    return out_file_list

class Mesh:
    """
    This object contains the mesh on wich we want to work and all of the values associated with it, it can also add values to the mesh by for example computing the strain field from the velocity field 
    
    """

    def __init__(self,mesh_path=None,compiled=True,parallel=True,nb_processors=8):
        """
        This methode creats an object of Mesh, is is used at the begining to initialise the mesh object

        :param mesh_path: This is the complete path of the mesh on which you want to work on, it should contain the name of the file and the extension (idealy a .vtu), for example it can be something like : '/home/my_mesh.vtu', if no values or given the function "from_save" can be used to restor a saved mesh
        :type mesh_path: str or None

        :param compiled: Defines if later the object can use compiled versions of the code or if the python versions of the algorihtms should be used 
        :type compiled: bool

        :param parallel: Defines if multiprocessing is allowed or not later when dooing operations on the mesh
        :type parallel: bool

        :param nb_processors: If parallel==True, it defines in how many parts the task is splited, this is idealy equal to the number of cores of the processor
        :type nb_processors: int
        
        """

        # only reads the mesh if a path was given
        if mesh_path!=None:
            self.points = meshio.read(mesh_path).points
            self.wedges = meshio.read(mesh_path).cells_dict['wedge']
            self.triangles = meshio.read(mesh_path).cells_dict['triangle']
            self.nbpoints= len(self.points)
            self.nbwedges= len(self.wedges)
            self.nbtriangles= len(self.triangles)
        self.compiled=compiled
        self.mesh_path=mesh_path
        self.points_to_wedges=None
        self.parallel=parallel
        self.nb_processors=nb_processors
        self.data={}
        self.avgvelocity=[0,0,0]
        self.points_coords_wedge=[[1,0,-1],[0,1,-1],[0,0,-1],[1,0,1],[0,1,1],[0,0,1]] # coordinates in the local bases of each node of a wedge 
        pass

    def from_save(self,folder_path):
        """
        Allows the user to creat a mesh from a previous save made before, it will recreat all of the elements needed for the mesh creation
        
        :param folder_path: path to the global folder of the save 
        :type folder_path: str
        
        """

        self.mesh_path=folder_path+"/mesh/mesh.vtu" # we open the vtu file and take every values we need 
        self.points = meshio.read(self.mesh_path).points
        self.wedges = meshio.read(self.mesh_path).cells_dict['wedge']
        self.triangles = meshio.read(self.mesh_path).cells_dict['triangle']
        self.nbpoints= len(self.points)
        self.nbwedges= len(self.wedges)
        self.nbtriangles= len(self.triangles)

        # we open the file where the relations between points and wedges is storded
        # a line looks like this : "509;510;511;608;609;610;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;-1;\n"
        file_points_to_wedge=open(folder_path+"/mesh/mesh_point_to_wedge.txt","r")
        self.points_to_wedges=[]
        for line in file_points_to_wedge:
            line=line[:-1]
            line=list(filter(None,line.split(";")))
            line=[int(elt) for elt in line]
            self.points_to_wedges.append(line)
        self.points_to_wedges=np.array(self.points_to_wedges)
        pass

    def _invert_mesh_parallel_compiled(self,queue,task,mesh_wedges_ctype):
        """
        This methode allows us to find the wedges connected to a given lis of points
        This methode is internal and allows for parallel computing internally with the compiled function, it should not be directly used

        :param queue: queue for multiprocessing
        :type queue: mp.queue object

        :param task: list of the start node and end node for the mesh insertion task
        :type task: list

        :param mesh_wedges_ctype: memory adress where the list of wedges is storded
        :type: ctype object
        
        """
        
        wedge_list_pointer=c_function.point_to_wedge_parallel(mesh_wedges_ctype,task[0],task[1],int(task[1]-task[0]),self.nbwedges) # c function evaluation
        wedge_list=np.array([wedge_list_pointer[i] for i in range(int(20*(task[1]-task[0]))) ]).reshape(-1,20)  # reading the output
        c_function.free_int(wedge_list_pointer) # clearing the memory 
        queue.put([wedge_list,task]) # adding the work to the queue
        pass

    def _invert_mesh_parallel(self,queue,task):
        """
        This methode allows us to find the wedges connected to a given lis of points
        This methode is internal and allows for parallel computing internally, it should not be directly used

        :param queue: queue for multiprocessing
        :type queue: mp.queue object

        :param task: list of the start node and end node for the mesh insertion task
        :type task: list
        
        """

        points_to_wedge=[]
        for point_id in range(task[0],task[1]): # for each point of the mesh
            List_wedges=[]
            for wedge_id in range(self.nbwedges): # we go through eatch wedges
                if point_id in self.wedges[wedge_id]: # to find a wedge that contains the point
                    List_wedges.append(wedge_id)
            List_wedges=np.array(List_wedges+[-1]*(20-len(List_wedges)))
            points_to_wedge.append(np.array(List_wedges))
        queue.put([points_to_wedge,task])
        pass

    def invert_mesh(self):
        """
        This methode allows the user to find the relation between the nodes and the wedges, this is not stored in the .vtu file. 
        It is mendatory to call it after mesh import for the tracker to work properlly
        This operation is quite time consuming (O(n^2) with n the number of elements) thus it is parallelized and compiled
        
        """
        
        if self.compiled==True:
            # we convert the mesh info in somthing that can be read by the c function
            mesh_wedges_ctype=(ctypes.c_int32 * int(self.nbwedges*6))(*np.array(self.wedges, 'int32').flatten())
            # we define the output as an array
            c_function.point_to_wedge.restype = ctypes.POINTER(ctypes.c_int32)
            c_function.point_to_wedge_parallel.restype = ctypes.POINTER(ctypes.c_int32)

            if self.parallel==False:
                # we call the compiled c function
                wedge_list_pointer=c_function.point_to_wedge(mesh_wedges_ctype,self.nbpoints,self.nbwedges)
                # we take the value from memory 
                wedge_list=np.array([wedge_list_pointer[i] for i in range(20*self.nbpoints) ])
                # we clear memory 
                c_function.free_int(wedge_list_pointer)
                # we add the array to the mesh object to be used later 
                self.points_to_wedges=wedge_list.reshape(-1,20)
            
            if self.parallel==True:
                q=mp.Queue() # we start the queue
                processes=[]
                self.points_to_wedges=np.zeros([self.nbpoints,20],dtype=int)
                points_per_task=self.nbpoints//self.nb_processors
                start_end_list=[[points_per_task*i,points_per_task*(i+1)] for i in range(self.nb_processors)] # we can the number of total points in the number of available cores 
                start_end_list[-1][-1]=self.nbpoints # we make shure we got all of them
                for task in start_end_list: # we start the tasks
                    p=mp.Process(target=self._invert_mesh_parallel_compiled,args=(q,task,mesh_wedges_ctype))
                    processes.append(p)
                    p.start()
                for p in processes:
                    rez=q.get() # we get the results of the tasks
                    self.points_to_wedges[rez[1][0]:rez[1][1],:]=rez[0]  # we put the values where they should be in coordination with what info they have
                for p in processes:
                    p.join() # we wait for every task to finish
                
                

        if self.compiled==False:
            if self.parallel==False:
                points_to_wedge=[]
                for point_id in range(self.nbpoints):  # for each point of the mesh
                    List_wedges=[]
                    for wedge_id in range(self.nbwedges):  # we go through eatch wedges
                        if point_id in self.wedges[wedge_id]:   # to find a wedge that contains the point
                            List_wedges.append(wedge_id)
                    List_wedges=np.array(List_wedges+[-1]*(20-len(List_wedges)))
                    points_to_wedge.append(np.array(List_wedges))
                self.points_to_wedges=np.array(points_to_wedge)

            if self.parallel==True:
                q=mp.Queue()  # we start the queue
                processes=[]
                self.points_to_wedges=np.zeros([self.nbpoints,20],dtype=int)
                points_per_task=self.nbpoints//self.nb_processors
                start_end_list=[[points_per_task*i,points_per_task*(i+1)] for i in range(self.nb_processors)]  #  we can the number of total points in the number of available cores 
                start_end_list[-1][-1]=self.nbpoints  # we make shure we got all of them
                for task in start_end_list:  # we start the tasks
                    p=mp.Process(target=self._invert_mesh_parallel,args=(q,task))
                    processes.append(p)
                    p.start()
                for p in processes:
                    rez=q.get()  # we get the results of the tasks
                    self.points_to_wedges[rez[1][0]:rez[1][1],:]=rez[0] # we put the values where they should be in coordination with what info they have
                for p in processes:
                    p.join()  # we wait for every task to finish

        # verification that the memory allocation is correct, since we complete each line with -1 values as we dont know how much wedges a point is toutching, thus we verify that 2à is enough
        max_elems=[]
        for point in self.points_to_wedges:
            i=0
            while point[i]!=-1 and i<9:
                i+=1
            max_elems.append(i)
        if max(max_elems) >=len(self.points_to_wedges[0]):
            print("Memory size allocated is not big enough") # printing error message, this means that a point is in more then 20 wedges, this value 20 must be changed in this file and in the c file

        pass

    def N(self,a1,a2,a3):
        """
        This methode computes the shape functions of a wedge element base on the numeroation on this web site 'https://web.mit.edu/calculix_v2.7/CalculiX/ccx_2.7/doc/ccx/node34.html'
        
        :param a1: coordiate in the 1 direction in the local bass 
        :type a1: float

        :param a2: coordiate in the 2 direction in the local bass 
        :type a2: float

        :param a3: coordiate in the 3 direction in the local bass 
        :type a3: float

        :return: returns an array of size 6 of the shape functions
        :rtype: np.array
        
        """
        return np.array([0.5*a1*(1-a3),0.5*a2*(1-a3),0.5*(1-a1-a2)*(1-a3),0.5*a1*(1+a3),0.5*a2*(1+a3),0.5*(1-a1-a2)*(1+a3)])

    def DN(self,a1:float,a2:float,a3:float):
        """
        This function gives out the derivative of the shape functions, this enables the uder to then calculate the jacobian for example 
        Theses shape functions are for wedges elements only
        The numeroation is made acording to this page https://web.mit.edu/calculix_v2.7/CalculiX/ccx_2.7/doc/ccx/node34.html
        
        :param a1: value of the coordinate of the point in the local bases of the wedge for the direction 1
        :type a1: float

        :param a2: value of the coordinate of the point in the local bases of the wedge for the direction 2
        :type a2: float

        :param a3: value of the coordinate of the point in the local bases of the wedge for the direction 3
        :type a3: float

        :return: returns a matrix of size 6 by 3 of the derivative of the shape functions 
        :rtype: np.array([6,3])
        
        """

        return np.array([[0.5*(1-a3),0,-0.5*a1],[0,0.5*(1-a3),-0.5*a2],[-0.5*(1-a3),-0.5*(1-a3),-0.5*(1-a1-a2)],[0.5*(1+a3),0,0.5*a1],[0,0.5*(1+a3),0.5*a2],[-0.5*(1+a3),-0.5*(1+a3),0.5*(1-a1-a2)]])

    def J(self,a1:float,a2:float,a3:float,wedge_id:int):
        """
        This function calculates the jacobian of the wedge element at the cordinate given and for the given wedge
        
        :param a1: value of the coordinate of the point in the local bases of the wedge for the direction 1
        :type a1: float

        :param a2: value of the coordinate of the point in the local bases of the wedge for the direction 2
        :type a2: float

        :param a3: value of the coordinate of the point in the local bases of the wedge for the direction 3
        :type a3: float

        :param wedge_id: the id of the wedge in wich the jacobian will be calculated
        :type wedge_id: int

        :return: returns a matrix of size 3 by 3 which is the jacobian
        :rtype: np.array([3,3])
        
        """

        DN=self.DN(a1,a2,a3)
        wedge=np.array([self.points[node] for node in self.wedges[wedge_id]])
        j=DN.T @ wedge
        return j.T

    def grad_v(self,a1:float,a2:float,a3:float,wedge_id:int):
        """
        This function calculates the gradient of the velocity field in the given element at the given coordinates

        :param a1: value of the coordinate of the point in the local bases of the wedge for the direction 1
        :type a1: float

        :param a2: value of the coordinate of the point in the local bases of the wedge for the direction 2
        :type a2: float

        :param a3: value of the coordinate of the point in the local bases of the wedge for the direction 3
        :type a3: float

        :param wedge_id: the id of the wedge in wich the jacobian will be calculated
        :type wedge_id: int

        :return: returns a matrix of size 3 by 3 which is the gradient of the velocity 
        :rtype: np.array([3,3])
        
        """
        
        inv_j=la.inv(self.J(a1,a2,a3,wedge_id))
        dNdx=self.DN(a1,a2,a3) @ inv_j
        v_mat=np.array([self.data['velocity'][node] for node in self.wedges[wedge_id]])
        return dNdx.T @ v_mat
    
    def strain_rate(self,a1:float,a2:float,a3:float,wedge_id):
        """
        This function calculates the strain rate a given coordinate in a given wedge 

        :param a1: value of the coordinate of the point in the local bases of the wedge for the direction 1
        :type a1: float

        :param a2: value of the coordinate of the point in the local bases of the wedge for the direction 2
        :type a2: float

        :param a3: value of the coordinate of the point in the local bases of the wedge for the direction 3
        :type a3: float

        :param wedge_id: the id of the wedge in wich the jacobian will be calculated
        :type wedge_id: int

        :return: returns a matrix of size 3 by 3 which is the tensor of strain rate
        :rtype: np.array([3,3])
        
        """
        M=self.grad_v(a1,a2,a3,wedge_id)
        return 0.5*(M+M.T)
    
    def _strain_rate_parallel(self,queue,tasks):
        """
        This function in not meant to used directly by the end user but it allows for strain calculation in parallel

        :param queue: queue for multiprocessing
        :type queue: mp.queue object
        :param task: list of the start node and end node for the mesh insertion task
        :type task: list
        
        """
        rez=[]
        for task in range(tasks[0],tasks[1]):
            for i in range(6):
                rez.append(self.strain_rate(self.points_coords_wedge[i][0],self.points_coords_wedge[i][1],self.points_coords_wedge[i][2],task))
        queue.put([rez,tasks])
        return 
    
    def compute_strain_rate(self,save_to_mesh=True):
        """
        This functions is meant to compute the strain rate if only the velocity field is availabe is can save or not the new filed to the vtu file
        
        :param save_to_mesh: tells if we whant to save the calculated value to the vtu file for future use or visualisation
        :type save_to_mesh: bool
        
        """
        
        # Initializing the values to 0
        self.data["exx"]=np.zeros(self.nbpoints)
        self.data["eyy"]=np.zeros(self.nbpoints)
        self.data["ezz"]=np.zeros(self.nbpoints)
        self.data["exy"]=np.zeros(self.nbpoints)
        self.data["exz"]=np.zeros(self.nbpoints)
        self.data["eyz"]=np.zeros(self.nbpoints)
        s_rate=np.zeros([6*self.nbwedges,3,3])
        count=0
        if self.parallel==False:
            for wedge_id in range(self.nbwedges): # for each wedge we calculate the strain 
                for node in range(6): # at each nodes 
                    s_rate[count,:,:]=(self.strain_rate(self.points_coords_wedge[node][0],self.points_coords_wedge[node][1],self.points_coords_wedge[node][2],wedge_id))
                    count+=1

        if self.parallel==True: # parallelised version of what is done previously
            count=0
            q=mp.Queue()
            processes=[]
            
            nb_node_core=(self.nbwedges)//self.nb_processors # we devide the mesh wedges by the number of cores to creat independ jobs 
            core_task=[[i*nb_node_core,(i+1)*nb_node_core] for i in range(self.nb_processors-1) ] 
            core_task.append([(self.nb_processors-2)*nb_node_core,self.nbwedges])

            for task in core_task:
                p=mp.Process(target=self._strain_rate_parallel,args=(q,task,))
                processes.append(p)
                p.start()
            for p in processes:
                rez=q.get()
                s_rate[rez[1][0]*6:rez[1][1]*6,:,:]=rez[0]
            for p in processes:
                p.join()

        # for each calulated values we sum the value to the node
        count=0
        node_passage=np.zeros(self.nbpoints)
        for wedge_id in range(self.nbwedges):
            for node in range(6):
                node_id=self.wedges[wedge_id][node]  
                node_passage[node_id]+=1
                self.data["exx"][node_id]=self.data["exx"][node_id]+s_rate[count][0,0]
                self.data["eyy"][node_id]=self.data["eyy"][node_id]+s_rate[count][1,1]
                self.data["ezz"][node_id]=self.data["ezz"][node_id]+s_rate[count][2,2]
                self.data["exy"][node_id]=self.data["exy"][node_id]+s_rate[count][0,1]
                self.data["exz"][node_id]=self.data["exz"][node_id]+s_rate[count][0,2]
                self.data["eyz"][node_id]=self.data["eyz"][node_id]+s_rate[count][1,2]
                count+=1
        
        # then we devied by the number of summed values to get the avearage at each nodes 
        for node_id in range(self.nbpoints):
            self.data["exx"][node_id]=self.data["exx"][node_id]/node_passage[node_id]
            self.data["eyy"][node_id]=self.data["eyy"][node_id]/node_passage[node_id]
            self.data["ezz"][node_id]=self.data["ezz"][node_id]/node_passage[node_id]
            self.data["exy"][node_id]=self.data["exy"][node_id]/node_passage[node_id]
            self.data["exz"][node_id]=self.data["exz"][node_id]/node_passage[node_id]
            self.data["eyz"][node_id]=self.data["eyz"][node_id]/node_passage[node_id]

        # if we want to save we take the value calculated, add it to the vtu and save it where the old file is 
        if save_to_mesh==True:
            new_mesh=meshio.read(self.mesh_path)
            new_mesh.point_data["exx"]=self.data["exx"]
            new_mesh.point_data["eyy"]=self.data["eyy"]
            new_mesh.point_data["ezz"]=self.data["ezz"]
            new_mesh.point_data["exy"]=self.data["exy"]
            new_mesh.point_data["exz"]=self.data["exz"]
            new_mesh.point_data["eyz"]=self.data["eyz"]
            new_mesh.write(self.mesh_path)
        pass

    def _compute_deviatoric_stress_glen(self,strain_rate, A, n):
        """
        Compute the deviatoric stress tensor directly from the strain rate tensor using Glen's flow law.
        
        :param strain_rate: Strain rate tensor (3x3 ndarray)
        :param A: flow rate viscosity
        :param n: glen exponent
        
        :return: Deviatoric stress tensor (3x3 ndarray).

        """
        # Compute the effective strain rate
        strain_rate_effective = np.sqrt(0.5 * np.sum(strain_rate**2))

        effective_viscosity=A**(-1/n)/((strain_rate_effective)**((n-1)/n))

        tau = effective_viscosity * strain_rate
        tau = tau-np.trace(tau)*np.eye(3)
        
        return tau
    
    def _compute_deviatoric_stress_cuffey(self,strain_rate,A1,A2,Q1,Q2,T,glen_exponent):
        """
        Compute the deviatoric stress tensor directly from the strain rate tensor using Glen's flow law with cuffey prefactor 
        
        :param strain_rate: Strain rate tensor (3x3 ndarray)
        :param A1: flow rate prefactor dependant on the temeprature for low temp <-10
        :param A2: flow rate prefactor dependant on the temeprature for high temp >-10
        :param Q1: activation energie for the first A1
        :param Q2: activation energie for the first A2
        :param glen_exponent: glen exponent of the flow law 
        
        :return: Deviatoric stress tensor (3x3 ndarray).

        """

        tensor_loop=[[0,0],[1,1],[2,2],[0,1],[0,2],[1,2]]
        tau=np.zeros([3,3])

        # Compute the effective strain rate
        R=8.314
        A1=A1*np.exp(-Q1/(R*T))
        A2=A2*np.exp(-Q2/(R*T))

        if T<-10+273:
            A=A1
        else:
            A=A2


        strain_rate_effective = np.sqrt(0.5 * np.sum(strain_rate**2))

        effective_viscosity=A**(-1/glen_exponent)/((strain_rate_effective)**((glen_exponent-1)/glen_exponent))

        tau = effective_viscosity * strain_rate
        tau = tau-np.trace(tau)*np.eye(3)

        return tau


    def compute_stress_glen(self,glen_exponent=3,viscosity=3.17e-6):
        """
        This function computes an approxiamtion of the stress using the glen law it enable the user to compute stress after the simulation has run, this doent igve the same results has elmer a closer look should be taken to better understand why

        :param glen_exponent: exponent of the glen law, usually 3 for ice 
        :type glen_exponent: float

        :param viscosity: the viscosity that should be used for the glen law 
        :type viscosity: float

        :return: adds the fileds "sxx" -- "syz" to the mesh nodes fields so that they can be advected with the particule 

        """

        # Initializing the values to 0
        self.data["sxx"]=np.zeros(self.nbpoints)
        self.data["syy"]=np.zeros(self.nbpoints)
        self.data["szz"]=np.zeros(self.nbpoints)
        self.data["sxy"]=np.zeros(self.nbpoints)
        self.data["sxz"]=np.zeros(self.nbpoints)
        self.data["syz"]=np.zeros(self.nbpoints)

        for node_id in (range(self.nbpoints)):
            strain_rate=np.array([[float(self.data["exx"][node_id]),float(self.data["exy"][node_id]),float(self.data["exz"][node_id])],[float(self.data["exy"][node_id]),float(self.data["eyy"][node_id]),float(self.data["eyz"][node_id])],[float(self.data["exz"][node_id]),float(self.data["eyz"][node_id]),float(self.data["ezz"][node_id])]])
            stress=self._compute_deviatoric_stress_glen(strain_rate=strain_rate,A=viscosity,n=glen_exponent)
            self.data["sxx"][node_id]=stress[0,0]
            self.data["syy"][node_id]=stress[1,1]
            self.data["szz"][node_id]=stress[2,2]
            self.data["sxy"][node_id]=stress[0,1]
            self.data["sxz"][node_id]=stress[0,2]
            self.data["syz"][node_id]=stress[1,2]
        pass


    def compute_stress_cuffey(self,A1=9125353404000.0,A2=7.660165593599999e+23,Q1=60.0e3,Q2=115.0e3,T=273,glen_exponent=3):
        """
        This function computes an approxiamtion of the stress using the glen law it enable the user to compute stress after the simulation has run, this doent igve the same results has elmer a closer look should be taken to better understand why

        :param A1: flow rate prefactor dependant on the temeprature for low temp <-10
        :type A1: float

        :param A2: flow rate prefactor dependant on the temeprature for high temp >-10
        :type A2: float

        :param Q1: activation energie for the first A1
        :type Q1: float

        :param Q2: activation energie for the first A2
        :type Q2: float
        
        :param T: temperature of the ice in kelins
        :type T: float

        :param glen_exponent: exponent of the glen law, usually 3 for ice 
        :type glen_exponent: float

        :return: adds the fileds "sxx" -- "syz" to the mesh nodes fields so that they can be advected with the particule 

        """

        # Initializing the values to 0
        self.data["sxx"]=np.zeros(self.nbpoints)
        self.data["syy"]=np.zeros(self.nbpoints)
        self.data["szz"]=np.zeros(self.nbpoints)
        self.data["sxy"]=np.zeros(self.nbpoints)
        self.data["sxz"]=np.zeros(self.nbpoints)
        self.data["syz"]=np.zeros(self.nbpoints)

        for node_id in (range(self.nbpoints)):
            strain_rate=np.array([[float(self.data["exx"][node_id]),float(self.data["exy"][node_id]),float(self.data["exz"][node_id])],[float(self.data["exy"][node_id]),float(self.data["eyy"][node_id]),float(self.data["eyz"][node_id])],[float(self.data["exz"][node_id]),float(self.data["eyz"][node_id]),float(self.data["ezz"][node_id])]])
            stress=self._compute_deviatoric_stress_cuffey(strain_rate=strain_rate,A1=A1,A2=A2,Q1=Q1,Q2=Q2,T=T,glen_exponent=glen_exponent)
            self.data["sxx"][node_id]=stress[0,0]
            self.data["syy"][node_id]=stress[1,1]
            self.data["szz"][node_id]=stress[2,2]
            self.data["sxy"][node_id]=stress[0,1]
            self.data["sxz"][node_id]=stress[0,2]
            self.data["syz"][node_id]=stress[1,2]
        pass

    def extract_data(self):
        """
        This function extracts all of the available data at each nodes of the mesh and adds it to the local mesh object 
        
        """

        for key in meshio.read(self.mesh_path).point_data.keys(): # we read every available key of the dictionnary 
            self.data[key]=meshio.read(self.mesh_path).point_data[key]

        self.avgvelocity=[np.max(self.data['velocity'].T[0]),np.max(self.data['velocity'].T[1]),np.max(self.data['velocity'].T[2])]

        pass

    def is_top_surface(self,point):
        """
        This function tells the user if a given point is on the top surface of the mesh or not 

        :param point: this should be the id of the point where we whant to know if we are on top or not 
        :type point: int

        :return: returns true if the point given is on the top surface
        :rtype: bool
        
        """
        for wedge in self.points_to_wedges[point]:
            if wedge !=-1:
                if point in self.wedges[wedge][:3]: # si notre point est dans le bas d'un prisme alors il ne peut pas être au dessus 
                    return False
        return True


    def select_points(self,methode="z_min_max",ice_mask=False,ice_mask_key="icymask",ice_mask_inside_val=1,tol=0.1,**kwargs):
        """
        This methode  enable the user to to select a big number of points at the same time, the selected particules can only be selected on the top and bottom surface not on the edeges or in the volume, each time they or selected at the node 

        :param methode: this parameters helps select the methode of point selection it can take 2 values : "z_min_max" or "z_min_max_y_min_max_x_min_max"
        :type methode: str

        :param ice_mask: says if there is an ice mask to use for point selection
        :type ice_mask: bool

        :param ice_mask_key: key word in the mesh dictionnary for the icemask layer
        :type ice_mask_key: str

        :param ice_mask_inside_val: value where ice is supposed to exist 
        :type ice_mask_inside_val: float 

        :param tol: this is by how much the ice_mask_value is able to change for it to be inside the glacier or not 
        :type tol: float

        :param kwargs: this is to define the boundaries of the selection depending on the methode, if the methode "z_min_max" is selected, the arguments are "z_min":int, "z_max":int and "surf_type":str in ["top","bottom","top_and_bottom"], if the methode "z_min_max_y_min_max_x_min_max" is selected the parameters "x_min":int, "x_max":int, "y_min":int, "y_max":int are added 
        :type kwargs: kwargs

        :return: it returns the list of the coodrinates of each points and the list of the trianlges  of the mesh composed by those points 
        :rtype: tuple(list,list)
        
        """
        point_list=[]
        triangle_list=[]
        
        if methode=="z_min_max":
            # we extract the arguments from the kwargs 
            if "z_min" not in kwargs and "z_max" not in kwargs:
                print("no values of z_min and z_max given")
                return False
            else : # we extract the bounds 
                z_min=kwargs["z_min"]
                z_max=kwargs["z_max"]
                z_min,z_max=min(z_min,z_max),max(z_min,z_max)
            
            # we extract the surface type
            surf_type="top" # top,bottom,top_and_bottom
            if "surf_type" in kwargs:
                surf_type=kwargs["surf_type"]

            # we go trough every triangles that represnts the "top" or and "bottom surface" and see if each points respects the boundary conditions
            for triangle in self.triangles:
                if self.points[triangle[0]][2]<z_max and self.points[triangle[0]][2]>z_min and self.points[triangle[1]][2]<z_max and self.points[triangle[1]][2]>z_min and self.points[triangle[2]][2]<z_max and self.points[triangle[2]][2]>z_min :
                    if (self.is_top_surface(triangle[0]) == True and self.is_top_surface(triangle[1]) == True and  self.is_top_surface(triangle[2]) == True  and (surf_type=="top" or surf_type=="top_and_bottom") ) or (self.is_top_surface(triangle[0]) == False and self.is_top_surface(triangle[1]) == False and  self.is_top_surface(triangle[2]) == False  and (surf_type=="bottom" or surf_type=="top_and_bottom") ):
                        if ice_mask==False or (abs(self.data[ice_mask_key][triangle[0]]-ice_mask_inside_val)<tol and abs(self.data[ice_mask_key][triangle[1]]-ice_mask_inside_val)<tol and abs(self.data[ice_mask_key][triangle[2]]-ice_mask_inside_val)<tol ):
                            if list(self.points[triangle[0]]) not in point_list:
                                point_list.append(list(self.points[triangle[0]]))
                            if list(self.points[triangle[1]]) not in point_list:
                                point_list.append(list(self.points[triangle[1]]))
                            if list(self.points[triangle[2]]) not in point_list:
                                point_list.append(list(self.points[triangle[2]]))
                            triangle_list.append([point_list.index(list(self.points[triangle[0]])),point_list.index(list(self.points[triangle[1]])),point_list.index(list(self.points[triangle[2]]))])

        elif methode=="z_min_max_y_min_max_x_min_max":
            # we do the exact same but for more boun dary conditions
            if "z_min" not in kwargs and "z_max" not in kwargs:
                print("no values of z_min and z_max given")
                return False
            if "x_min" not in kwargs and "x_max" not in kwargs:
                print("no values of x_min and x_max given")
                return False
            if "y_min" not in kwargs and "y_max" not in kwargs:
                print("no values of y_min and y_max given")
                return False

            else :
                z_min=kwargs["z_min"]
                z_max=kwargs["z_max"]
                x_min=kwargs["x_min"]
                x_max=kwargs["x_max"]
                y_min=kwargs["y_min"]
                y_max=kwargs["y_max"]
                z_min,z_max=min(z_min,z_max),max(z_min,z_max)
                x_min,x_max=min(x_min,x_max),max(x_min,x_max)
                y_min,y_max=min(y_min,y_max),max(y_min,y_max)

            surf_type="top" # top,bottom,top_and_bottom
            if "surf_type" in kwargs:
                surf_type=kwargs["surf_type"]

            for triangle in self.triangles:
                if self.points[triangle[0]][2]<z_max and self.points[triangle[0]][2]>z_min and self.points[triangle[1]][2]<z_max and self.points[triangle[1]][2]>z_min and self.points[triangle[2]][2]<z_max and self.points[triangle[2]][2]>z_min and self.points[triangle[0]][0]<x_max and self.points[triangle[0]][0]>x_min and self.points[triangle[1]][0]<x_max and self.points[triangle[1]][0]>x_min and self.points[triangle[2]][0]<x_max and self.points[triangle[2]][0]>x_min and self.points[triangle[0]][1]<y_max and self.points[triangle[0]][1]>y_min and self.points[triangle[1]][1]<y_max and self.points[triangle[1]][1]>y_min and self.points[triangle[2]][1]<y_max and self.points[triangle[2]][1]>y_min:
                    if (self.is_top_surface(triangle[0]) == True and self.is_top_surface(triangle[1]) == True and  self.is_top_surface(triangle[2]) == True  and (surf_type=="top" or surf_type=="top_and_bottom") ) or (self.is_top_surface(triangle[0]) == False and self.is_top_surface(triangle[1]) == False and  self.is_top_surface(triangle[2]) == False  and (surf_type=="bottom" or surf_type=="top_and_bottom") ):
                        if ice_mask==False or (abs(self.data[ice_mask_key][triangle[0]]-ice_mask_inside_val)<tol and abs(self.data[ice_mask_key][triangle[1]]-ice_mask_inside_val)<tol and abs(self.data[ice_mask_key][triangle[2]]-ice_mask_inside_val)<tol ):
                            if list(self.points[triangle[0]]) not in point_list:
                                point_list.append(list(self.points[triangle[0]]))
                            if list(self.points[triangle[1]]) not in point_list:
                                point_list.append(list(self.points[triangle[1]]))
                            if list(self.points[triangle[2]]) not in point_list:
                                point_list.append(list(self.points[triangle[2]]))
                            triangle_list.append([point_list.index(list(self.points[triangle[0]])),point_list.index(list(self.points[triangle[1]])),point_list.index(list(self.points[triangle[2]]))])

        else :
            print("Methode does not exists")
            return False

        return point_list,triangle_list





class ParticuleTracker:
    """
    This class is the instance that enables the user to do some particule tracking and more opperations on each particules
    
    """

    def __init__(self,mesh,eps_top=10e-5,u_min=10e-3,start_acu_level=0,nb_friends_max=3,ellipse_factor=10,outside_iter=1,ini_iter=100,dt=-0.01,n=1000,compiled=True,params_to_track=[],parallel=True,verbose=True):
        """
        This creats an instance of particule tracker, many parameters are set at the beegining that will be used mater for the particule tracking instance

        :param mesh: this should be an object mesh created by icetrackpy, it can be just a mesh openned by meshio for example
        :type mesh: Mesh.object

        :param eps_top: this defines the level at which a particule died, if the projection of the nomalized vector motion on  the normal to the surface is higher then eps_top then  the particule died, if set high it allows the particule to do some surfing, should be between 0 an 1 
        :type eps_top: float

        :param u_min: This defines the minimum speed at which a particule can go, if the particule is slower then said speed it will die, the speed is both calculated by taking the norm of the velocity field and also by calculating the real speed between 2 cordiantes 
        :type u_min: float

        :param start_acu_level: this is the level at which the particules are allowed to melt under if time goes in the normal direction and fall if time is reversed 
        :type start_acu_level:

        :param nb_friends_max: this defines the number of elements the code will look thru when looking for an wedge arround a point, this size is the small axis of an ellips good values between 2 and 4, smaller is faster but less accurate
        :type nb_friends_max: int

        :param ellipse_factor: this defines how our research radius looks like an ellipse, for meshesh where elements can have a really bad shapes it is a good idea to set the value higher, value between 1 and 10 is good higher is slower
        :type ellipse_factor: int

        :param outside_iter: this is the number of random iterations added to the normal iteration to find the good value of wedge 
        :type outside_iter: int 

        :param ini_iter: number of initial tryins to find the node on the first step time, if the mesh is really non concave this value need to be high arround 100 is a good start 
        :type ini_iter: int

        :param dt: this is the step time of the particule tracker higher is faster but less precise 
        :type dt: float 

        :param n: number of maximum iterations of the particule tracker
        :type n: int

        :param compiled: tels the programm if he is able to use compiled code recources 
        :type compiled: bool 

        :param params_to_track: list of strings of the parameters that should be added to the dictionnary during the tracking
        :type params_to_track: list[str]

        :param parallel: tels the code if he is allowded to multi process the task
        :type parallel: bool 

        :param verbose: tells if time steps are printed or not
        :type verbose: bool

        """
        
        self.eps_top=eps_top
        self.u_min=u_min
        self.start_acu_level=start_acu_level
        self.nb_friends_max=nb_friends_max
        self.ellipse_factor=ellipse_factor
        self.outside_iter=outside_iter
        self.ini_iter=ini_iter
        self.dt=dt
        self.n=n
        self.compiled=compiled
        self.mesh=mesh
        self.particule=[]
        self.print_iter=100
        self.params_to_track=params_to_track
        self.parallel=parallel
        self.verbose=verbose
        self.max_speed_factor=0

        # on convertie en un format ctype compatible
        self.mesh_wedges_ctype=(ctypes.c_int * int(self.mesh.nbwedges*6))(*np.array(self.mesh.wedges, 'int').flatten() )
        self.mesh_points_ctype=(ctypes.c_float * int(self.mesh.nbpoints*3))(*np.array(self.mesh.points, 'float').flatten() )
        self.point_wedge_list_ctype=(ctypes.c_int * int(20*self.mesh.nbpoints))(*np.array(self.mesh.points_to_wedges, 'int').flatten() )

        # defines the output of the c function
        c_function.is_inside_wedge.restype = ctypes.POINTER(ctypes.c_float)
        c_function.find_best_sommet.restype = ctypes.c_int

        pass

    def from_save(self,folder_path):
        """
        This methode allows the user to restor previous particule tracking instances that have run previouslly

        :param path: path to the folder where every_thing is saved, something like /home/savetracking_folder
        :type path: str 
        
        """
        
        files=os.listdir(path=folder_path+'/particules')  # looks for the folder where the particules are storded 
        files=[i for i in files if i.startswith('particule_')]
        max_id_file=max([int(file[10:-4]) for file in files])
        self.particule=[{} for __ in range(max_id_file+1)]
        for file in files: # we look in each files that is available in the folder 
            id_file=int(file[10:-4])
            particule_file=open(folder_path+'/particules/'+file,'r')
            keys=list(filter(None,particule_file.readline().split(";")))[:-1]
            keys=[key.split(":")[0] for key in keys]
            nb_keys=len(keys)
            for key in keys:
                self.particule[id_file].update({key:[]})
            for i,line in enumerate(particule_file): # for eatch line we go trough each column and stroe the value 
                if i!=0:
                    line=list(filter(None,line.split(";")))[:-1]
                    for j in range(nb_keys):
                        if line[j][0].isalpha()==True:
                            self.particule[id_file][keys[j]].append((line[j]))
                        elif line[j][0:2]=="[[":  # if the value is a 2D array we parse it corectly
                            line[j]=list(filter(None,line[j][2:-2].split("],[")))
                            list_2D=[]
                            for elt in line[j]:
                                elt=list(filter(None,elt.split(",")))
                                elt=[float(element) for element in  elt]
                                list_2D.append(np.array(elt))
                            self.particule[id_file][keys[j]].append(np.array(list_2D))
                        elif line[j][0]=="[":  # if the value is a 1D array we pars it correctly
                            line[j]=list(filter(None,line[j][1:-1].split(",")))
                            line[j]=[float(elt) for elt in  line[j]]
                            self.particule[id_file][keys[j]].append(np.array(line[j]))
                        else:
                            self.particule[id_file][keys[j]].append(float(line[j]))
        pass            



    def is_inside_wedge(self,point_of_interest,id_wedge):
        """
        This methode helps to know weather a point is inside a given wedge, it is an essantial funtion in the particule tracking code 

        :param point_of_interest: point id for which we desiere to know wether it is inside the wedge 
        :type point_of_interest: int

        :param id_wedge: id of the wedge for which we whant to know if we are inside or not 
        :type id_wedge: int

        :return: return where it is inside or not, a list of the coordinates in the local bases and a score to know how far ethe point is fro mthe given wedge 
        :rtype: bool, list(floats), float

        """

        if self.compiled==False:
            tetra_shape=[[0,1,5,2],[0,5,4,3],[0,1,5,4]] # this defines the tetrahedral shapes that we put inside the wedge

            tetra_1=np.array([[1,0,0,1],[0,0,1,0],[0,1,1,1],[1,1,1,1]]) # defines the change in bases between the tetra in the wedge and the reference shape 
            tetra_2=np.array([[1,0,0,0],[0,1,0,1],[0,0,1,1],[1,1,1,1]])

            N=np.eye(4)
            N[3,0:3]=-1

            coords=np.ones(4)
            coords[0:3]=point_of_interest

            def score(A,X): #this function gives the distance between the point and the nearest point inside the wedge 
                """
                This subfuntion calculates the distance between a given point A and the nearest point in the tetra represneted by X

                :param A: local coordinate of the point in the wedge
                :type A: np.array([3])

                :param X: real coordiantes of each points of the tetra nodes
                :type X: np.array([4,4])

                """

                pA=[0,0,0]# initial nearest point 
                if A[0]>=0 and A[1]>=0 and A[2]>=0 and (1-A[0]-A[1]-A[2])<0: # this is  in case the point is frond of the big slanted face   of the tetra   
                    pA=[(1+2*A[0] -A[1] - A[2])/3,(1-A[0] +2*A[1] - A[2])/3,(1-A[0] -A[1] +2*A[2])/3]
                    if (pA[0]>=0  and pA[1]>=0  and pA[2]<0 ) or (pA[0]<0  and pA[1]>=0  and pA[2]<0 ):  # in case we are under the z=0
                        pA=[(1+pA[0]-pA[1])/2,(1-pA[0]+pA[1])/2,0]
                        if pA[0]<0: # on one side
                            pA=[0,1,0]
                        if pA[1]<0: # or the other 
                            pA=[1,0,0]
                    if (pA[0]<0  and pA[1]>=0  and pA[2]>=0 ) or (pA[0]<0  and pA[1]<0  and pA[2]>=0 ): # if we are  on the left side 
                        pA=[0,(1+pA[1]-pA[2])/2,(1-pA[1]+pA[2])/2]
                        if pA[1]<0:
                            pA=[0,0,1] # top left 
                        if pA[2]<0:
                            pA=[0,1,0] # bottom right
                    if (pA[0]>=0  and pA[1]<0  and pA[2]>=0 ) or (pA[0]>=0  and pA[1]<0  and pA[2]<0 ): # if we are an the right side
                        pA=[(1+pA[0]-pA[2])/2,0,(1-pA[0]+pA[2])/2]
                        if pA[0]<0:  # top left 
                            pA=[0,0,1]
                        if pA[2]<0:  # top right
                            pA=[1,0,0]

                elif A[0]<0 and A[1]>=0 and A[2]>=0: # if we are closer to the left side
                    pA=[0,A[1],A[2]]
                    pA=[max(0,pA[0]),max(0,pA[1]),max(0,pA[2])]
                    if (1-pA[1])<pA[2]:
                        pA=[0,(1+pA[1]-pA[2])/2,(1-pA[1]+pA[2])/2]
                elif A[0]>=0 and A[1]<0 and A[2]>=0:  # if we are closer to the bottom side
                    pA=[A[0],0,A[2]]
                    pA=[max(0,pA[0]),max(0,pA[1]),max(0,pA[2])]
                    if (1-pA[0])<pA[2]:
                        pA=[(1+pA[0]-pA[2])/2,0,(1-pA[0]+pA[2])/2]
                elif A[0]>=0 and A[1]>=0 and A[2]<0:  # if we are closer to the right side
                    pA=[A[0],A[1],0]
                    pA=[max(0,pA[0]),max(0,pA[1]),max(0,pA[2])]
                    if (1-pA[0])<pA[1]:
                        pA=[(1+pA[0]-pA[1])/2,(1-pA[0]+pA[1])/2,0]
                elif (A[0]>=0 and A[1]<0 and A[2]<0) or (A[0]<0 and A[1]>=0 and A[2]<0) or (A[0]<0 and A[1]<0 and A[2]>=0) or (A[0]<0 and A[1]<0 and A[2]<0):  # if we are just bewhing th 0 point 
                    pA=[0,0,0]

                pA=np.array(pA+[1])
                rpA=X @ N @ pA.T # we give the result in the units of the real element 

                return la.norm(np.array(rpA[:3])-np.array(point_of_interest),ord=2)

            A_list=[]
            score_list=[]
            for tetra in range(3):
                X=np.ones([4,4])
                for i in range(4):
                    for  j in range(3):
                        X[j,i]=self.mesh.points[self.mesh.wedges[id_wedge][tetra_shape[tetra][i]]][j] # we creat the coord matrix for the tetra element

                A=la.solve( X @ N, coords.T)  # finding the local coords in the local bases 


                if A[0]>=0 and A[1]>=0 and A[2]>=0  and (1-A[0]-A[1]-A[2])>=0: # if the point is inside we just need to put the point in the wedge element
                    if tetra == 0:
                        A[2]=2*A[2]-1
                        return True, A[:3],0
                    if tetra == 1:
                        A=tetra_1 @  N @ A
                        A[2]=2*A[2]-1
                        return True, A[:3],0
                    if tetra == 2:
                        A=tetra_2 @  N @ A
                        A[2]=2*A[2]-1
                        return True, A[:3],0

                else : # if the point is outisde we calculate the score and give the local coords 
                    if tetra == 0:
                        score_list.append(score(A,X))
                        A[2]=2*A[2]-1
                        A_list.append(A)

                    if tetra == 1 :
                        score_list.append(score(A,X))
                        A=tetra_1 @  N @ A
                        A[2]=2*A[2]-1
                        A_list.append(A)

                    if tetra == 2:
                        score_list.append(score(A,X))
                        A=tetra_2 @  N @ A
                        A[2]=2*A[2]-1
                        A_list.append(A)

            id_tetra=score_list.index(min(score_list)) # calculation of the score 


            return False ,A_list[id_tetra][:3],score_list[id_tetra]

        elif self.compiled==True: # compiled version of this function,returns the exact same thing 
            point_ctype=(ctypes.c_float * int(3))(*np.array(point_of_interest, 'float').flatten() )
            return_list=c_function.is_inside_wedge(self.mesh_wedges_ctype,self.mesh_points_ctype,point_ctype,int(id_wedge))
            return_vect=return_list[0]==1,np.array([return_list[i] for i in range(1,4) ]),(return_list[4])
            c_function.free_float(return_list)
            return return_vect

    ## point finder

    def find_best_sommet(self,coord_objectif,start_wedge):
        """
        Given the coordinate of a point and the coordinates of a wedge this function finds the nearest node of the mesh to the coordinate given
        This function is subject to compilation

        :param coord_objectif: coordinate of the traget point
        :type coord_objectif: np.array([3])

        :param start_wedge: id of the id of the start of the search 
        :type start_wedge: int

        :return: returns the id of the best node of the mesh
        :rtype: int

        """
        if self.compiled==False: # python version

            coord_objectif=np.array(coord_objectif)

            best_sommet=0
            old_best_sommet=0
            i=0

            while (best_sommet != old_best_sommet or i <= 2) and i<self.mesh.nbpoints: # we loop until we end 2 times on the same node are we do to much iterations
                old_best_sommet=best_sommet
                i+=1

                # Trouver le sommet du wedge le plus proche de l'objectif
                distance_list=[la.norm(self.mesh.points[sommet]-coord_objectif,ord=1) for sommet in self.mesh.wedges[start_wedge]] # we create the distance list of the wedge nodes to the point
                best_sommet=self.mesh.wedges[start_wedge][distance_list.index(min(distance_list))] # find the best node

                distance_list=[]
                for wedge in self.mesh.points_to_wedges[best_sommet]: # from a wedge we look  from this node woch one has the best wedge 
                    if wedge !=-1:
                        distance_list.append(np.mean([la.norm(self.mesh.points[sommet]-coord_objectif) for sommet in self.mesh.wedges[wedge]])) # for each point of the potential wedge we look at the closest 
                start_wedge=self.mesh.points_to_wedges[best_sommet][distance_list.index(min(distance_list))]

            return best_sommet

        elif self.compiled==True: # compiled version 
            coord_objectif_ctype=(ctypes.c_float * int(3))(*np.array(coord_objectif, 'float') )
            start_wedge_ctype=(ctypes.c_int)(start_wedge)
            best_sommet=c_function.find_best_sommet(self.mesh_wedges_ctype,self.mesh_points_ctype,self.point_wedge_list_ctype,coord_objectif_ctype,start_wedge_ctype )
            return best_sommet

    ## Wedge finder

    def find_wedge_near_point(self,coord_objectif:np.array,start_wedge=None):
        """
        This function finds the best wedge for a given point, if the point is inside the wedge what will be returned is the wedge over the point, eles it will return the nerest wedge
        This funtion may call compiled functions

        :param coord_objectif: cooridnates of the point we want to know the nearest wedge 
        :type coord_objectif: np.array([3])

        :param start_wedge: gives a initial idea of where we are in the mesh
        :type start_wedge: int of None

        :return: the wedge id in which we are the closes are inside, if we are inside or non, the local coodrinates of the given wedge
        :rtype: int, bool, np.array([3])

        """


        if start_wedge==None:
            start_wedge=np.random.randint(self.mesh.nbwedges-1) # take a random start wedge is none are given
            nb_rd_iter=self.ini_iter
        else:
            nb_rd_iter=self.outside_iter

        list_iter_wedge_coords=[]
        list_iter_wedge_wedge_id=[]
        list_iter_wedge_score=[]


        is_inside,local_coords,score=self.is_inside_wedge(coord_objectif,start_wedge) # we look is the point is not alreday inside the wedge 


        if is_inside:
            return start_wedge,True,local_coords

        for __ in range(nb_rd_iter): # we loop over the number of iterations 

            best_sommet=self.find_best_sommet(coord_objectif,start_wedge) # we find the best node for the point

            list_score=[]
            list_coords=[]
            list_wedge_id=[]

            explore_list=list(self.mesh.points_to_wedges[best_sommet]) # we creat an explore list arround this node in the ellips form

            in_line_wedge=explore_list[0]
            for __ in range(self.ellipse_factor): # we explore the top of the ellipse bottom
                for wedge in self.mesh.points_to_wedges[self.mesh.wedges[in_line_wedge][0]]: # we loop in a sphere like shape arround the back bonne of the ellips
                    if (self.mesh.wedges[wedge][3:]==self.mesh.wedges[in_line_wedge][0:3]).all():
                        in_line_wedge=wedge
                        explore_list.append(wedge)

            in_line_wedge=explore_list[0]
            for __ in range(self.ellipse_factor): # we explore the top of the ellipse top
                for wedge in self.mesh.points_to_wedges[self.mesh.wedges[in_line_wedge][3]]:
                    if (self.mesh.wedges[wedge][:3]==self.mesh.wedges[in_line_wedge][3:]).all():
                        in_line_wedge=wedge
                        explore_list.append(wedge)

            explore_list=list(np.unique(explore_list)) # we remove all of the double elements 

            for nb_friends in range(self.nb_friends_max):
                for wedge in explore_list: # look over all elements in the serach list
                    if wedge !=-1 : 

                        is_inside,local_coords,score=self.is_inside_wedge(coord_objectif,wedge)

                        list_score.append(float(score))

                        list_coords.append(local_coords)
                        list_wedge_id.append(wedge)

                        if is_inside==True: # if we are inside we return the wanted values 
                            return wedge, True, local_coords

                # exploration en forme de boule


                if nb_friends<self.nb_friends_max: # if the ellipse diameter is under the max dimentiosn we mak the search list bigger 
                    explore_list_new=[]
                    for wedge in explore_list:
                        if wedge!=-1:
                            for point in self.mesh.wedges[wedge]:
                                for order_2_point in self.mesh.points_to_wedges[point]:
                                    if order_2_point!=-1:
                                        explore_list_new.append(order_2_point)
                    explore_list=list(np.unique(explore_list_new))


                # exploration en forme de tube

            index=list_score.index(min(list_score))

            list_iter_wedge_coords.append(list_coords[index])
            list_iter_wedge_wedge_id.append(list_wedge_id[index])
            list_iter_wedge_score.append(list_score[index])

            start_wedge=np.random.randint(self.mesh.nbwedges-1) # if we do numerous iterations we start over with a new random starting wedge 

        # we claculate the values we need 

        id_wedge=list_iter_wedge_score.index(min(list_iter_wedge_score))

        return list_iter_wedge_wedge_id[id_wedge], False , list_iter_wedge_coords[id_wedge]# In this case the point might not be inside the solid thus we return the closest one


    ## Particule tracker

    def flow_path(self,particule_coord,particule_id=None):
        """
            This function cumputs the flow path of an initial particule

            :param particule_coord: inital coordinates of the particule
            :type particule_coord: np.arrray([3])

            :return: returns a dictionnary with the following fields, "status":  tels what is the status of the particule, "is_inside": bool if the particule is inside the glacier are touching a wall, "path" : coordiantes of the particule at each step, "velocity" velocity of the particule at each time step, "time_step" : time step of the simulaiton at each step, add all of the additional fields asked when the object was created 
            :rtype: dic

        """
        is_inside_list=[]
        particule_path=[particule_coord]
        u_particule=0

        # we ensure that the additional asked parameters are in the good conditionning ["sxx"]
        if type(self.params_to_track)==str:
            self.params_to_track=[self.params_to_track]
        elif type(self.params_to_track)!=list:
            return False # if nothing is asked we put false 

        data_to_collect_dic={"velocity":[]} # creat the particule status and make it as alive by default 
        status=[]
        
        # we add the different elements to the dictionnary
        for elt in self.params_to_track:
            data_to_collect_dic.update({elt:[]})

        # we loop over the maximum number of time step
        id_wedge=None
        max_speed_factor=0
        for i in range(self.n-1):
            # We take the local coordinates of the initial point 

            id_wedge,is_inside,local_particule_coords=self.find_wedge_near_point(particule_coord,id_wedge)

            is_inside_list.append(is_inside*1)

            if i%self.print_iter==0 and self.verbose==True: # we print as needed the status of the particule 
                print("Time step : " +str(i+1)+" / Time : " +str(round(i*self.dt,2)) + " year / Inside glacier = " +str(is_inside))


            if is_inside==False and i>1: # we kill the particule hits the surface and is under the conditions to be killed 
                if  local_particule_coords[2]>1+self.eps_top and np.sign(self.dt)>0 and particule_coord[2]<self.start_acu_level: # cheks if the angle of the particule to the surface and altitude is inside the requiered prarameters
                    if  self.mesh.wedges[id_wedge][3:] in self.mesh.triangles :# if the particule is in the top surface 
                        status.append("Dead_particule_melted") # we up date the status 
                        break
                if  local_particule_coords[2]>1+self.eps_top and np.sign(self.dt)<0 and particule_coord[2]>self.start_acu_level: # we do the same if the time is inverted 
                    if  self.mesh.wedges[id_wedge][3:] in self.mesh.triangles :
                        status.append("Dead_particule_fell_from_the_sky")
                        break
            # if the particule is outside we force it inside the wedge 
            if local_particule_coords[0]<0:
                local_particule_coords[0]=0
            if local_particule_coords[1]<0:
                local_particule_coords[1]=0
            if local_particule_coords[2]<-1:
                local_particule_coords[2]=-1
            if local_particule_coords[2]>1:
                local_particule_coords[2]=1
            if (1-local_particule_coords[0])<local_particule_coords[1]:
                local_particule_coords[1]=local_particule_coords[1]/(local_particule_coords[0]+local_particule_coords[1])
                local_particule_coords[0]=local_particule_coords[0]/(local_particule_coords[0]+local_particule_coords[1])

            u_wedge=np.array([self.mesh.data['velocity'][id_point] for id_point in self.mesh.wedges[id_wedge] ]).T # we calculate the local velocity of the wedge

            u_particule=u_wedge @ self.mesh.N(local_particule_coords[0],local_particule_coords[1],local_particule_coords[2]) # actual velocity of the particule 

            if la.norm(u_particule,ord=1) < self.u_min and i >1: # if the mesh velocity is too low we kill the particule to avoid over iterations
                status.append("Dead_field_velocity_too_low")
                break

            for elt in data_to_collect_dic : # we update the dictionnary to add the coponent at the new point
                if elt!="velocity":
                    data_to_collect_dic[elt].append(float((np.array([self.mesh.data[elt][id_point] for id_point in self.mesh.wedges[id_wedge] ]).T @ self.mesh.N(local_particule_coords[0],local_particule_coords[1],local_particule_coords[2]))))

            data_to_collect_dic["velocity"].append(u_particule)

            if  is_inside==True:
                particule_coord=particule_coord+self.dt*u_particule
            else :
                particule_coord =   np.array([self.mesh.points[point] for point in self.mesh.wedges[id_wedge]]).T @ self.mesh.N(local_particule_coords[0],local_particule_coords[1],local_particule_coords[2]) +self.dt*u_particule

            particule_path.append(particule_coord)

            
            # computing the relative speed of the particule in comapraison to the average speed, this should be a small valeu like 10 max, if the value is too high this means that the tracking is not working correctly
            if la.norm((particule_path[-1]-particule_path[-2])/(self.dt)-np.array(self.mesh.avgvelocity))/la.norm(np.array(self.mesh.avgvelocity)) > self.max_speed_factor:
                max_speed_factor = la.norm((particule_path[-1]-particule_path[-2])/(self.dt)-np.array(self.mesh.avgvelocity))/la.norm(np.array(self.mesh.avgvelocity))

            if la.norm(particule_path[-1]-particule_path[-2])/abs(self.dt) < self.u_min and i >1: # if the calculated velocity from the cordiantes is too low we kill the particule
                status.append("Dead_particule_velocity_too_low")
                break

            status.append("Alive")

        for elt in data_to_collect_dic : # we update the dictionnary for the final step
            if elt != "velocity":
                data_to_collect_dic[elt].append( float(
                (np.array([self.mesh.data[elt][id_point] for id_point in
                self.mesh.wedges[id_wedge] ]).T @
                self.mesh.N(local_particule_coords[0],local_particule_coords[1],local_particule_coords[2]))))

        data_to_collect_dic["velocity"].append(data_to_collect_dic["velocity"][-1])
        status.append(status[-1])

        for elt in data_to_collect_dic: # we add all the default elements 
            data_to_collect_dic[elt]=np.array(data_to_collect_dic[elt])
        data_to_collect_dic.update({"path":np.array(particule_path)})
        data_to_collect_dic.update({"time_step":self.dt*np.ones(len(particule_path))})
        data_to_collect_dic.update({"is_inside":is_inside_list+[0]})
        data_to_collect_dic.update({"status":status})
        data_to_collect_dic.update({"id":[particule_id]*len(status)})

        return data_to_collect_dic,max_speed_factor 

    def _flow_path_parallel(self,queue,point:np.array,particule_id):
        """
        Called function for parallelisation of the tracking shloud no be used directly by the end user 

        :param queue: queue of the storted results:
        :type queue: queue.object

        :param point: point to be computed
        :type point: np.array
        
        """
        
        data_dic,max_speed_factor=self.flow_path(point,particule_id=particule_id)
        queue.put([data_dic,max_speed_factor])
        pass

    def compute_path(self,points:np.array):
        """
        This functions calculates the tracking of a list of points, it is parallelised and in part compiled 

        :param points: list of the starting points  for the tracking
        :type points: list(np.array([3]))

        """

        if not isinstance(points[0], (list, np.ndarray)):
            points = [points]

        self.nb_particules=len(points)

        if self.parallel ==True: #parallelized version 
            q=mp.Queue()
            processes = []
            rez_list=[]
            for point_id in range(len(points)):
                p=mp.Process(target=self._flow_path_parallel,args=(q,points[point_id],point_id,)) # we creat a process for each point we want to track
                processes.append(p)
                p.start()
            for p in processes:
                rez_list.append(q.get())
            for p in processes:
                p.join()

            for i in range(len(rez_list)):
                if self.max_speed_factor<rez_list[i][1]:
                    self.max_speed_factor=rez_list[i][1]
                rez_list[i]=rez_list[i][0]

                
            id_points=[np.array(rez_list[i]["id"][0]) for i in range(self.nb_particules)]

            for i in range(self.nb_particules):
                for j in range(self.nb_particules):
                    if (i==id_points[j]).all():
                        self.particule.append(rez_list[j])


        elif self.parallel ==False:#non parallelised version
            for point_id in range(len(points)):
                rez=self.flow_path(points[point_id],particule_id=point_id)
                self.particule.append(rez[0])
                if rez[1]>self.max_speed_factor:
                    self.max_speed_factor=rez[1]

        print("Particule speed factor : "+str(abs(self.max_speed_factor-1))) # should be arround 10
        pass

    ## Time inversion when going back into time

    def time_inversion(self):
        """
        This function must be called when negative time is used to flip elements in the dictionnary to make shure that for post processing it looks like time was going forward
        
        """

        for particule_id in range(len(self.particule)) :
            if self.particule[particule_id]["time_step"][0]<0: # avoids dooble flipping
                for item,value in self.particule[particule_id].items():
                    self.particule[particule_id][item]=np.flip(value,0)
                    if item == "time_step":
                        self.particule[particule_id][item]=[-elt for elt in np.flip(value,0)]
        pass


    ## Compute every thing related to the  matrix


    def roation_matrix(self,xyz_component:np.array):
        '''
        This function computes the rotation matrix that is needed to go from the x axis to the normal of the flow line

        :param xyz_component: components of the vector we want to to know the orientation
        :type xyz_component: np.array([3,n]) or np.array([3])

        :retrun: returns an arrray of roation matricies or simply the rotation matrix depending on the input
        :rtype: np.array([n,3,3]) or np.array([3,3])

        '''

        if type(xyz_component[0])==np.ndarray or type(xyz_component[0])==list:
            len_path=len(xyz_component[0])
            for i in range(len_path):
                xyz_component[1][i]=max(-1,min(1,xyz_component[1][i])) # we make sur that the value is between (-1 and 1) elese we might get an error even if the value is 1+eps
        else :
            xyz_component[1]=max(-1,min(1,xyz_component[1]))


        psi_rot_z=np.arcsin(xyz_component[1]) # we calculate the angles of the rotation acording to this reference : https://en.wikiversity.org/wiki/PlanetPhysics/Euler_321_Sequence
        theta_rot_yp=np.arctan2(-xyz_component[2],xyz_component[0])

        # we make shure that if we are working with a list of vectors  that the angles a continiuous and not dooing a +2pi 
        # not evry usfull jsute for this function but handi if we want to output the angles
        if type(xyz_component[0])==np.ndarray or type(xyz_component[0])==list:
            for i in range(len_path-2):
                if abs(theta_rot_yp[i] - theta_rot_yp[i+1])>np.pi:
                    if theta_rot_yp[i+1]-theta_rot_yp[i]>0:
                        theta_rot_yp[i+1:]=theta_rot_yp[i+1:]-2*np.pi
                    else :
                        theta_rot_yp[i+1:]=theta_rot_yp[i+1:]+2*np.pi
        # we calculate the roation matrix matrix 
        mat_rot23=np.array([[ np.cos(theta_rot_yp)*np.cos(psi_rot_z), -np.cos(theta_rot_yp)*np.sin(psi_rot_z), np.sin(theta_rot_yp) ],[ np.sin(psi_rot_z),np.cos(psi_rot_z),np.zeros(len(theta_rot_yp))],[-np.sin(theta_rot_yp)*np.cos(psi_rot_z), np.sin(theta_rot_yp)*np.sin(psi_rot_z),np.cos(theta_rot_yp)]])
        
        return mat_rot23



    def compute_rotation_matrix(self):
        '''
            This functions computes the roation matrix from the field "path" it takes the vector at each time step and calculate the angle relative to base coordinate system. The time step vectors are filtered to keep the algorythm stable even with very noisy data. It can be used with a reversed time step or a foward time step

        '''

        nb_particules=len(self.particule)
        # we loop over each particules 
        for id_particule in range(nb_particules):
            # We define the filter variables for later 
            window_size=max(2,int(len(self.particule[id_particule]["path"])/10))
            poly_order=max(1,min(2,window_size-1))
            len_path=len(self.particule[id_particule]["path"])
            angle_list=[]
            for i in range(len_path-1):
                # we calculate the vector between each time step
                vec=(self.particule[id_particule]["path"][i+1]-self.particule[id_particule]["path"][i])
                # we nomalize the vector
                norm=la.norm(vec)
                if norm!=0:
                    vec=vec/norm
                # we add the vector to the list of vectors 
                angle_list.append(vec)

            # we first filter the vector with a polynomial filter
            xyz_component=([savgol_filter(np.array(angle_list).T[0],window_size,poly_order),savgol_filter(np.array(angle_list).T[1],window_size,poly_order),savgol_filter(np.array(angle_list).T[2],window_size,poly_order)])


            # we then filter the signal using a filter of convolution
            box_pts=2*max(1,int(window_size/(2*poly_order)))
            box = np.ones(box_pts) / box_pts

            #  we add values on the edges to help with the filter of convolution and make the end list the same length as the initial one
            xyz_component[0]=np.hstack( [xyz_component[0][0]*np.ones(box_pts//2) , xyz_component[0],xyz_component[0][-1]*np.ones(box_pts//2) ] )
            xyz_component[0]=np.convolve(xyz_component[0], box, mode="valid" )
            xyz_component[1]=np.hstack( [xyz_component[1][0]*np.ones(box_pts//2) , xyz_component[1],xyz_component[1][-1]*np.ones(box_pts//2) ] )
            xyz_component[1]=np.convolve(xyz_component[1], box, mode="valid" )
            xyz_component[2]=np.hstack( [xyz_component[2][0]*np.ones(box_pts//2) , xyz_component[2],xyz_component[2][-1]*np.ones(box_pts//2) ] )
            xyz_component[2]=np.convolve(xyz_component[2], box, mode="Valid" )

            # we get the direction of time 
            sign=np.sign(self.particule[id_particule]["time_step"][0])
            for i in range(len_path-1):
                norm=la.norm([xyz_component[0][i],xyz_component[1][i],xyz_component[2][i]])
                if norm!=0:
                    # we norm back the vector as the fitering may have caused some denormalisation
                    xyz_component[0][i]=sign*xyz_component[0][i]/norm
                    xyz_component[1][i]=sign*xyz_component[1][i]/norm
                    xyz_component[2][i]=sign*xyz_component[2][i]/norm

            # we calculate the rotation martices 
            mat_rot23=self.roation_matrix(xyz_component)

            # we make it the right shape
            mat_rot23=mat_rot23.transpose([2,0,1])

            # we add it to the particule dictionnary
            self.particule[id_particule]["rotation_matrix"]=np.array(mat_rot23)
        pass

   

    def compute_in_particule_coord(self,tensor_type:str):
        """
        This function function helps us convert ensy tensor field that we have on the particule flowline to the local bases 

        :param tensor_type: give what field we should convert to the local base for strain rate it will be "e" for example
        :type tensor_type: str

        :return: adds the fields of each component of the tensor with a "L" before to indicate the local carrater of the field

        """
        
        nb_particules=len(self.particule)
        component_list=[tensor_type+"xx",tensor_type+"yy",tensor_type+"zz",tensor_type+"xy",tensor_type+"yz",tensor_type+"xz"]
        for id_particule in range(nb_particules):
            len_path=len(self.particule[id_particule]["path"])

            for component in component_list: # verifies that everything is there 
                if component not in self.particule[id_particule]:
                    return False
            if "rotation_matrix" not in self.particule[id_particule]:
                print("no rotation matrix")
                return False
            
            # creats the elemtns in the dictionnary
            for component in component_list:
                self.particule[id_particule].update({"L"+component: [] })

            # we creat the tensor list at each time step
            for step in range(len_path-1):
                tensor=np.array([[self.particule[id_particule][component_list[0]][step],self.particule[id_particule][component_list[3]][step],self.particule[id_particule][component_list[5]][step]  ],
                                [self.particule[id_particule][component_list[3]][step],self.particule[id_particule][component_list[1]][step],self.particule[id_particule][component_list[4]][step]],
                                [self.particule[id_particule][component_list[5]][step],self.particule[id_particule][component_list[4]][step],self.particule[id_particule][component_list[2]][step]]])

                # we compute it in the loacl bases 
                local_tensor=self.particule[id_particule]["rotation_matrix"][step] @ tensor @ self.particule[id_particule]["rotation_matrix"][step].T

                # we add to the field the desiered values
                self.particule[id_particule]["L"+component_list[0]].append(local_tensor[0][0])
                self.particule[id_particule]["L"+component_list[1]].append(local_tensor[1][1])
                self.particule[id_particule]["L"+component_list[2]].append(local_tensor[2][2])
                self.particule[id_particule]["L"+component_list[3]].append(local_tensor[0][1])
                self.particule[id_particule]["L"+component_list[4]].append(local_tensor[1][2])
                self.particule[id_particule]["L"+component_list[5]].append(local_tensor[0][2])

            for component in component_list: # we duplicate the last value to make sure the list is the same length as the over for eas of plotting
                self.particule[id_particule]["L"+component].append(self.particule[id_particule]["L"+component][-1])

        pass



    def compute_dev(self,field:str):
        """
        This function computes the deviatoric version of the tensor field given

        :param field: type of tensor field for which we want to know the deviatoric version, for example "e"
        :type: "str"

        :return: adds to the particule dictionnary each filed of the tensor with the addition of the letter "D" for deviatoric, for example "Dexx"
        
        """

        for particule in self.particule:
            fxx=particule[field+"xx"]
            fyy=particule[field+"yy"]
            fzz=particule[field+"zz"]
            tr=fxx/3+fyy/3+fzz/3 # we calculate the trace 
            fxx=fxx-tr
            fyy=fyy-tr
            fzz=fzz-tr
            particule.update({"D"+field+"xx":fxx})
            particule.update({"D"+field+"yy":fyy})
            particule.update({"D"+field+"zz":fzz})
            particule.update({"D"+field+"xy":particule[field+"xy"]})
            particule.update({"D"+field+"yz":particule[field+"yz"]})
            particule.update({"D"+field+"xz":particule[field+"xz"]})
        pass


    def time_int_over_path(self,param_to_int:str):
        """
        This function does time integration over the path for a parameter that is defined allong the flow line

        :param param_to_int: type of parameter we whant to integrate for example "1" if we have addted a list of 1ones in the particule dic and want to kwon the time
        :type param_to_int: str
        
        :return: adds to the particule dic the parameter with a "I" before for integrated

        """

        if param_to_int not in self.particule[0] and param_to_int!=1:
            print("Parameter not in path data")
            return None
        nb_particules=len(self.particule)
        for id_particule in range(nb_particules): # for each particule
            len_path=len(self.particule[id_particule]["path"])
            int_data=np.zeros(len_path)
            if np.sign(self.particule[id_particule]["time_step"][0])==1:  # if time is positive 
                for i in range(len_path-1):
                    int_data[i+1]=int_data[i]+self.particule[id_particule][param_to_int][i]*self.particule[id_particule]["time_step"][i] # we integrate

            else : # time inversion should be done 
                print("time step is negative, use Mesh.time_inversion()")
                return None

            self.particule[id_particule].update({"I"+param_to_int:int_data})

        return None

    def int_strain_rate_over_path_small_displacement(self,param_to_int:str):
        """
        This funciton computes the strain rate with the small displacement approximation which is not correct in many cases 

        :param param_to_int: parameter we want to icompute like "exx"
        :type param_to_int: str

        :return: adds to the particule dic the parameter with a "I" before for integrated like "Iexx"

        """

        if param_to_int not in self.particule[0] :
            print("Parameter not in path data")
            return None
        nb_particules=len(self.particule)
        for id_particule in range(nb_particules):
            len_path=len(self.particule[id_particule]["path"])
            int_data=np.zeros(len_path)
            if np.sign(self.particule[id_particule]["time_step"][0])==1:
                for i in range(len_path-1):
                    int_data[i+1]=(1+int_data[i])*(1+self.particule[id_particule][param_to_int][i]*self.particule[id_particule]["time_step"][i]) - 1
            else :
                print("time step is negative, use Mesh.time_inversion()")
                return None
            self.particule[id_particule].update({"I"+param_to_int:int_data})
        pass

    def int_strain_particule(self,particule:dict,param_to_int:str):
        """
        This function computes the strain tensor in the high deformation context 

        :param particule: particule dictionnary of which we want to compute the strain
        :type particule: dict

        :param param_to_int: parameter wich we want to integrate like "e"
        :type param_to_int: str 

        :return: returns the eigen values of the strain tensor, the eigen vectors of the strain tensor at each time step and the initial start coordinate
        :rtype: np.array, np.array, np.array
        
        """

        strain_list=[]
        for i in range(len(particule["path"])):
            eps=particule["time_step"][i]*np.array([[particule[param_to_int+"xx"][i],particule[param_to_int+"xy"][i],particule[param_to_int+"xz"][i]],[particule[param_to_int+"xy"][i],particule[param_to_int+"yy"][i],particule[param_to_int+"yz"][i]],[particule[param_to_int+"xz"][i],particule[param_to_int+"yz"][i],particule[param_to_int+"zz"][i]]])
            strain_list.append(eps)

        D_tot,P_tot=diagm.int_strain(strain_list)
        return D_tot,P_tot,particule["id"][0]
    

    def _int_strain_particule_parallel(self,queue,particule:dict,param_to_int:str):
        """
        This function calls  int_strain_particule, it is used for parallization it should not be used by the end user 

        :param particule: this is the dictionnary of the particule element
        :type particule: dict

        :param param_to_int: parameter wich we want to integrate like "e"
        :type param_to_int: str 

        """

        rez=self.int_strain_particule(particule,param_to_int)
        queue.put(rez)
        pass

    def compute_strain(self,param_to_int):
        """
        This function calculates the strain rate in the high deformation context, it is the parallelised or not version depending on the object config
        
        :param param_to_int: parameter wich we want to integrate like "e"
        :type param_to_int: str 

        :return: adds to the diconnary 2 fileds that are "eigval_I"+param to int and "eigvect_I"+param wich are the rigen values and aigen vectors of the strain for each time step
       
        """
        
        if self.parallel==False : # non parallelised version of the 
            rez=[]
            for particule in self.particule:
                rez.append(self.int_strain_particule(particule,param_to_int))

        if self.parallel==True: # parallelised version
            rez=[]
            q=mp.Queue()
            processes = []
            for particule in self.particule:
                p=mp.Process(target=self._int_strain_particule_parallel,args=(q,particule,param_to_int,))
                processes.append(p)
                p.start()
            for p in processes:
                rez.append(q.get())
            for p in processes:
                p.join()


        for i in range(len(rez)):
            for j in range(len(rez)):
                if (rez[i][2]==self.particule[j]["id"][0]): # adding the values to the field 
                    self.particule[j].update({"eigval_I"+param_to_int:rez[i][0]})
                    self.particule[j].update({"eigvect_I"+param_to_int:rez[i][1]})

        pass


    def __repr__(self):
        """
        This function prints what is in the dictionnary of the first particule to know what fields are available to the end user and it also gives the number of particules 

        """

        string="Nombres de particules : " + str(len(self.particule))+"\n"+"Elements du dictionnaire d'une particule : \n"

        for elt in self.particule[0]:
            string=string+"     - " +elt +"\n"

        return string
    
    def save_tracking(self,path,name,things_to_save="all",header=None,particule_to_save="all",save_mesh=True,auto_delete=True):
        """
        Allows the user to save the data of a particule are all of them and save the usefull parameters of said particule in one convinient folder, make shure that all of the elements of your particules are floats, np.arrays (up to 2 dimentions) or lists 

        :param path: path to the folder where the file should be saved
        :type path: str

        :param name: name of the folder where everything will be saved
        :type name: str

        :param things_to_save: what field should be saved for each partticule
        :type things_to_save: str ("all") or list of str or None

        :param header: additionnal text to put allong the parameter name for more explicit description
        :type header: None or list of str

        :param particule_to_save: wich particule is saved 
        :type particule_to_save: int or str ("all") of None or list 

        :param save_mesh: saves the mesh for easy import 
        :type save_mesh: bool

        :param auto_delete: delets the file if it already exists, if set to False and the file is there it will print an error
        :type auto_delete: bool

        """

        os.chdir(path)

        if name in os.listdir():
            if auto_delete==False:
                print("warning :file already exits")
            else:
                os.system("rm -r " + name )

        os.system("mkdir " + name)
        os.chdir(path+"/"+name)

        #saving the mesh
        if save_mesh:
            os.system("mkdir mesh")
            # copying the vtu file to the save folder
            os.system("cp "+ self.mesh.mesh_path +" "+path+"/"+name+"/mesh/mesh.vtu" )
            # opening the file for the point to wedge list 
            mesh_file=open("mesh/mesh_point_to_wedge.txt","w")
            for wedges_list in self.mesh.points_to_wedges:
                line=""
                for wedge in wedges_list:
                    line = line+str(wedge)+";"
                line=line+"\n"
                mesh_file.write(line)
            mesh_file.close()
        
        if things_to_save==None or particule_to_save == None:
            return 
        
        else :
            # creating a file to save the particules 
            os.system("mkdir particules")
            # creating the list of particules to save
            if particule_to_save=="all":
                particule_to_save=self.particule
            if type(particule_to_save)==int:
                particule_to_save=[particule_to_save]

            for i in range(len(particule_to_save)):
                # creating the file for one particule
                particule_file=open("particules/particule_"+str(i)+".txt","w")
                # creating the list of things to save 
                if things_to_save == "all":
                    things_to_save=list(self.particule[0].keys())
                elif type(things_to_save)==str:
                    things_to_save=[things_to_save]
                
                # adding the key to the top of the file 
                line=""
                for j in range(len(things_to_save)):
                    things_to_add=""
                    if type(header)==list and len(header)==len(things_to_save) and header !=None:
                        things_to_add=":"+header[j]
                    line=line+things_to_save[j]+things_to_add+";"
                particule_file.write(line+"\n")

                # saving the info 
                for j in range(len(self.particule[i]["path"])):
                    line=""
                    for key in things_to_save:
                        tts=self.particule[i][key][j]
                        # on decompose les tableau 2D pour pouvoir les applatir mais en gardnt les parenthèses
                        if type(tts)==np.ndarray: 
                            if len(np.shape(tts))==2:
                                tmp_tts="["
                                for elt in tts:
                                    tmp_tts=tmp_tts+str(elt).replace(" ",",")+","
                                tts=tmp_tts[:-1]+"]"
                        line=line+str(tts).replace(" ",",")+";"
                        while ',,' in line:
                            line=line.replace(",,",",")
                        line=line.replace("[,","[").replace(",]","]")
                    particule_file.write(line+"\n")
                particule_file.close()
        pass

    def extract_scalar(self,field):
        """
        This function extracts the data that are formed in tensors or vectors into all of the bases components and adds it to the field of the particule 
        
        :param field: field of tensor or vector that should be extracted
        :type field: str

        :retrun: addts to the particule dictionnary each element with a "_1" or "_11" at the end to telle the user where the value comes from
        
        """
        
        # part for the vectors
        if len(self.particule[0][field][0].shape)==1:
            for particule in self.particule:
                for i in range(len(particule[field][0])):
                    particule.update({field+"_"+str(i+1):[]})
                for i in range(len(particule["path"])):
                    for j in range(len(particule[field][i])):
                        particule[field+"_"+str(j+1)].append(particule[field][i][j])

        # part for the tensors 
        if len(self.particule[0][field][0].shape)==2:
            for particule in self.particule:
                for i in range(len(particule[field][0])):
                    for j in range(len(particule[field][0][0])):
                        particule.update({field+"_"+str(i+1)+str(j+1):[]})
                for i in range(len(particule["path"])):
                    for j in range(len(particule[field][i])):
                        for k in range(len(particule[field][i][0])):
                            particule[field+"_"+str(j+1)+str(k+1)].append(particule[field][i][j][k])
    pass

    def extract_vector(self,field,direction=1):
        """
        This function extracts the data that are formed in tensors into all of the bases vectors and adds it to the field of the particule 
        
        :param field: field of tensor or vector that should be extracted
        :type field: str

        :param direction: defines wether we extract line : 0 or if we extract line : 1
        :type direction: int

        :retrun: adds to the particule dictionnary each element with a "_1"  at the end to telle the user where the value comes from
        
        """

        if len(self.particule[0][field][0].shape)==2:
            for particule in self.particule:
                for i in range(len(particule[field][0])):
                    particule.update({field+"_"+str(i+1):[]})
                for i in range(len(particule["path"])):
                    for j in range(len(particule[field][i])):
                        if direction==0:
                            particule[field+"_"+str(j+1)].append(particule[field][i][j])
                        if direction==1:
                            particule[field+"_"+str(j+1)].append(particule[field][i][:,j])

    def compact_tensor(self,field): 
        """
        This function helps the user compact a tensor that is in the form of many components in the form of xx or _11

        :param filed: filed name of the tensor that should be compacted for example "e"
        :type field: str

        :return: adds to the said filed the values of the said tensor 

        """

        suffix_list_1=["xx","yy","zz","xy","xz","yz"]
        suffix_list_2=["_11","_22","_33","_12","_13","_23"]
        
        for particule in self.particule:
            if field+suffix_list_1[0] in list(particule.keys()):
                suffix_list=suffix_list_1
            elif field+suffix_list_2[0] in list(particule.keys()):
                suffix_list=suffix_list_2
            particule.update({field:[]})
            for i in range(len(particule["path"])):
                particule[field].append(np.array([[particule[field+suffix_list[0]][i],particule[field+suffix_list[3]][i],particule[field+suffix_list[4]][i]],[particule[field+suffix_list[3]][i],particule[field+suffix_list[1]][i],particule[field+suffix_list[5]][i]],[particule[field+suffix_list[4]][i],particule[field+suffix_list[5]][i],particule[field+suffix_list[2]][i]]]))

        pass

    def compact_vector(self,field): 
        """
        This function helps the user compact a vector that is in the form of many components in the form of x or _1

        :param filed: filed name of the vector that should be compacted for example "velocity"
        :type field: str

        :return: adds to the said filed the values of the said vector

        """

        suffix_list_1=["x","y","z"]
        suffix_list_2=["_1","_2","_3"]
        
        for particule in self.particule:
            if field+suffix_list_1[0] in list(particule.keys()):
                suffix_list=suffix_list_1
            elif field+suffix_list_2[0] in list(particule.keys()):
                suffix_list=suffix_list_2
            particule.update({field:[]})
            for i in range(len(particule["path"])):
                particule[field].append(np.array([particule[field+suffix_list[0]][i],particule[field+suffix_list[1]][i],particule[field+suffix_list[2]][i]] ))
        pass

    def normalise_vector(self,field):
        """
        This function is helpfull to nomalize a filed when we want to plot it where it needs to be nomed to 1

        :param field: field to normalise
        :type field: str

        :return: adds to the particule dic the vector field with a "N" before  to indicate the normalisation

        """

        for particule in self.particule:
            particule.update({"N"+field:[]})
            for i in range(len(particule["path"])):
                norm=la.norm(particule[field][i],ord=2)
                if norm!=0:
                    particule["N"+field].append(particule[field][i]/norm)
                else:
                    particule["N"+field].append(particule[field][i])

                if particule["N"+field][-1][2]<0:
                    particule["N"+field][-1][2]=-particule["N"+field][-1][2]

        pass

    def compute_relative_anisotropy(self,tensor_eigval):
        """
        This function  computes the relative anisotropy of a tensor field 

        :param tensor_eigval: eigen values field of the tensor we want to calculate
        :type tensor_eigval: str

        :return: retruns the relative anisotropy at each step time in the field relative_anisotropy

        """

        for particule in self.particule:
            particule.update({"relative_anisotropy":[]})
            for i in range(len(particule["path"])):
                std=np.std(particule[tensor_eigval][i])
                avg=np.mean(particule[tensor_eigval][i])
                particule["relative_anisotropy"].append(std/avg)

        pass

    def compute_fractional_anisotropy(self,tensor_eigval):
        """
        This function  computes the fractional anisotropy of a tensor field 

        :param tensor_eigval: eigen values field of the tensor we want to calculate
        :type tensor_eigval: str

        :return: retruns the fractional anisotropy at each step time in the field relative_anisotropy

        """

        for particule in self.particule:
            particule.update({"fractional_anisotropy":[]})
            for i in range(len(particule["path"])):
                std=np.std(particule[tensor_eigval][i])
                avg=np.mean([particule[tensor_eigval][i][0]**2,particule[tensor_eigval][i][1]**2,particule[tensor_eigval][i][2]**2])
                particule["fractional_anisotropy"].append(std/np.sqrt(avg))

        pass

    def compute_volume_ratio_anisotropy(self,tensor_eigval):
        """
        This function  computes the volume_ratio anisotropy of a tensor field 

        :param tensor_eigval: eigen values field of the tensor we want to calculate
        :type tensor_eigval: str

        :return: retruns the volume_ratio anisotropy at each step time in the field relative_anisotropy

        """

        for particule in self.particule:
            particule.update({"volume_ratio_anisotropy":[]})
            for i in range(len(particule["path"])):
                avg=np.mean(particule[tensor_eigval][i])
                particule["volume_ratio_anisotropy"].append(1-np.prod(particule[tensor_eigval][i])/avg**3)

        pass
    
    def compute_flatness_anisotropy(self,tensor_eigval):
        """
        This function  computes the flatness anisotropy of a tensor field 

        :param tensor_eigval: eigen values field of the tensor we want to calculate
        :type tensor_eigval: str

        :return: retruns the flatness anisotropy at each step time in the field flatness_anisotropy

        """

        for particule in self.particule:
            particule.update({"flatness_anisotropy":[]})
            for i in range(len(particule["path"])):
                mid=np.sort(particule[tensor_eigval][i])[1]
                if mid==0:
                    mid=10e-15
                particule["flatness_anisotropy"].append(np.max(particule[tensor_eigval][i])/mid)

        pass

    def compute_deformation_eigval(self,strain_eigval:str):
        """
        This function is used when the strain has been calculated and that we know the eigen value of the strain, in this situation we can compute the déformation tensor of the particule, this is usfull to use the anisotropy factor

        :param strain_eigval: this is the name of the field of the eigen values of the strain, this field should be a 1D array
        :type strain_eigval: str

        :return: adds the field deformation_eigval to the particule trunk where the eigen values of the deformation tensor are storded a 1D np array 
        
        """

        for particule in self.particule:
            particule.update({"deformation_eigval":[]})
            for i in range(len(particule["path"])):
                particule["deformation_eigval"].append((np.sqrt(((particule[strain_eigval][i])*2)+1)))

        pass

    def diag_tensor_field(self,field):
        """
        This function takes field of symmetric tensor and creats 2 new keys in the dictionnary with the eigen values and the eigen vectors of the the field tensor 

        :param field: field to diagonalize
        :type field: str

        :retrun: add to the dictionnary of the particule 2 fields that are "eigval_"+field and "eigvect_"+field

        """

        for particule in self.particule:
            particule.update({"eigval_"+field:[]})
            particule.update({"eigvect_"+field:[]})
            for i in range(len(particule["path"])):
                val,vect=la.eigh(particule[field][i])
                particule["eigval_"+field].append(val)
                particule["eigvect_"+field].append(vect)
        pass

    def compute_age(self):
        """
        adds the filed I1 that represent the integration of the time step over the time, this represents the age of the particule

        :return: adds the age of the particule in the field "I1"
        """
        [particule.update({"1":[1]*len(particule["time_step"])}) for particule in self.particule]
        self.time_int_over_path("1")
        pass

    def compute_flinn(self,field,top_cutoff=1):
        """
        Computes the vector of the field in the flinn plane for easy visualisation of the anisotropy

        :param field: field name taht will go through the flinn process calculation, idealy it is the field of the aigen values of the transfomation vector 
        :type field: str

        :param top_cutoff: to help visualisation, this parametters compresses the to value, if set to one 1 every thing has the proper value, if set to 2 the maximum bound is set to half of the highest value, value shoud be an int higher then 1 
        :type top_cutoff: int
        
        """
        n_max=0
        n_min=0
        for particule in self.particule:
            particule.update({"flinn_vector":[]})
            for  i in range(len(particule["path"])):
                vp=np.sort(particule[field][i])
                if vp[0]==0:
                    vp[0]=10e-15
                if vp[1]==0:
                    vp[1]=10e-15
                if vp[2]==0:
                    vp[2]=10e-15

                vect=np.array([(np.log(abs(vp[2]/vp[1]))),(np.log(abs(vp[1]/vp[0]))),0])
                if la.norm(vect) >n_max:
                    n_max=la.norm(vect)
                if la.norm(vect) <n_min:
                    n_min=la.norm(vect)
                particule["flinn_vector"].append(vect)
        
        for particule in self.particule:
            for  i in range(len(particule["path"])):
                if la.norm(particule["flinn_vector"][i])<n_max/top_cutoff:
                    particule["flinn_vector"][i]=(particule["flinn_vector"][i]/la.norm(particule["flinn_vector"][i]))*(la.norm(particule["flinn_vector"][i])-n_min)/(n_max/top_cutoff-n_min)
                else:
                    particule["flinn_vector"][i]=(particule["flinn_vector"][i]/la.norm(particule["flinn_vector"][i]))
                if la.norm(particule["flinn_vector"][i])<1:
                    particule["flinn_vector"][i][2]=np.sqrt(1-particule["flinn_vector"][i][1]**2-particule["flinn_vector"][i][0]**2)
                # print(particule["flinn_vector"][i])
                # if la.norm(particule["flinn_vector"][i])!=1:
                #     print(particule["flinn_vector"][i])
        pass


## Tracker Transient 

class ParticuleTrackerTransient:
    """
    This class is the instance that enables the user to do some particule tracking in the case of a transient simulation 
    
    """

    def __init__(self,mesh_list,eps_top=10e-5,u_min=10e-3,start_acu_level=0,nb_friends_max=3,ellipse_factor=10,outside_iter=1,ini_iter=100,dt=-0.01,n=1000,compiled=True,params_to_track=[],parallel=True,verbose=True):
        """
        This creats an instance of particule tracker, many parameters are set at the beegining that will be used mater for the particule tracking instance

        :param mesh_list: this should be a list of objects mesh created by icetrackpy, it can be just a mesh openned by meshio for example
        :type mesh_list: [Mesh.object]

        :param eps_top: this defines the level at which a particule died, if the projection of the nomalized vector motion on  the normal to the surface is higher then eps_top then  the particule died, if set high it allows the particule to do some surfing, should be between 0 an 1 
        :type eps_top: float

        :param u_min: This defines the minimum speed at which a particule can go, if the particule is slower then said speed it will die, the speed is both calculated by taking the norm of the velocity field and also by calculating the real speed between 2 cordiantes 
        :type u_min: float

        :param start_acu_level: this is the level at which the particules are allowed to melt under if time goes in the normal direction and fall if time is reversed 
        :type start_acu_level:

        :param nb_friends_max: this defines the number of elements the code will look thru when looking for an wedge arround a point, this size is the small axis of an ellips good values between 2 and 4, smaller is faster but less accurate
        :type nb_friends_max: int

        :param ellipse_factor: this defines how our research radius looks like an ellipse, for meshesh where elements can have a really bad shapes it is a good idea to set the value higher, value between 1 and 10 is good higher is slower
        :type ellipse_factor: int

        :param outside_iter: this is the number of random iterations added to the normal iteration to find the good value of wedge 
        :type outside_iter: int 

        :param ini_iter: number of initial tryins to find the node on the first step time, if the mesh is really non concave this value need to be high arround 100 is a good start 
        :type ini_iter: int

        :param dt: this is the step time of the particule tracker higher is faster but less precise 
        :type dt: float 

        :param n: number of maximum iterations of the particule tracker for each mesh thus the total maximum number of iteration is n*len(self.mesh_list)
        :type n: int

        :param compiled: tels the programm if he is able to use compiled code recources 
        :type compiled: bool 

        :param params_to_track: list of strings of the parameters that should be added to the dictionnary during the tracking
        :type params_to_track: list[str]

        :param parallel: tels the code if he is allowded to multi process the task
        :type parallel: bool 

        :param verbose: tells if time steps are printed or not
        :type verbose: bool

        """
        
        self.eps_top=eps_top
        self.u_min=u_min
        self.start_acu_level=start_acu_level
        self.nb_friends_max=nb_friends_max
        self.ellipse_factor=ellipse_factor
        self.outside_iter=outside_iter
        self.ini_iter=ini_iter
        self.dt=dt
        self.n=n
        self.compiled=compiled
        self.mesh_list=mesh_list
        self.particule=[]
        self.print_iter=100
        self.params_to_track=params_to_track
        self.parallel=parallel
        self.verbose=verbose
        self.max_speed_factor=0
        
        pass

    def compute_path(self,points:np.array):
        """
        This functions calculates the tracking of a list of points, it is parallelised and in part compiled 

        :param points: list of the starting points  for the tracking
        :type points: list(np.array([3]))

        :return: returns an object of the class ParticuleTracker that way is can be used as any other tracking instances for further analysis 
        :rtype: <ParticuleTracker.object>

        """
        
        ini_point_list=points
        print("Start of the particle tracking")
        for my_mesh_id in range(len(self.mesh_list)):
            print("Working on mesh : " +str(my_mesh_id))
            my_mesh=self.mesh_list[-my_mesh_id-1]
            tracker=ParticuleTracker(my_mesh,params_to_track=self.params_to_track,dt=self.dt,start_acu_level=self.start_acu_level,n=self.n,u_min=self.u_min,nb_friends_max=self.nb_friends_max,outside_iter=self.outside_iter,verbose=self.verbose)
            tracker.compute_path(ini_point_list)

            if tracker.max_speed_factor > self.max_speed_factor:
                self.max_speed_factor=tracker.max_speed_factor 

            if len(tracker.particule)!=len(ini_point_list):
                break
        
            if my_mesh_id == 0: 
                particule_data=tracker.particule
                order_list=[]
            else:
                for particule_id in range(len(tracker.particule)):
                    for key in tracker.particule[particule_id ].keys():
                        particule_data[order_list[particule_id]][key]=list(particule_data[order_list[particule_id]][key])+list(tracker.particule[particule_id][key])
            
            ini_point_list=[]
            new_order_list=[]

            for particule_id in range(len(tracker.particule)):
                particule=tracker.particule[particule_id]
                if particule["status"][-1]=='Alive':
                    ini_point_list.append(particule["path"][-1])
                    
                    if len(order_list)!=0:
                        new_order_list.append(order_list[particule_id])
                    else:
                        new_order_list.append(particule_id)

            order_list=new_order_list    

            if my_mesh_id<len(self.mesh_list)-1:
                del tracker
            else:
                tracker.particule=particule_data

        print("Transient particule speed factor : " +str(abs(self.max_speed_factor-1)))
        return tracker



## Plot results

class Plot:
    """ 
    This methode helps the user to plot the different meshes and particules allong there flow line with the color representing vectors or saclars

    """

    def __init__(self,cmap="hsv",bgcolor="white",min_val=None,max_val=None,projection_2D=False,canvas_size=(1200,800),projection_type="steriographic",cwheel_shadding="classic",cwheel_symetry=True,projection_base=np.array([[1,0,0],[0,1,0],[0,0,1]]),cbar_symmetric=False):
        """ 
        This creats the object that will be plotted it, creats the window of the plot, thus a display option needs to be present on the computure for this to work


        :param cmap: this is the color map wanted for the plotting of the colors =, only available for the color bar not for the vector plot, by default set to "hsv"
        :type cmap: str

        :param bgcolor: color of the backgroud of the plot by default "white"
        :type bgcolor: str

        :param canvas_size: size of the canvas, higer will give better results when saving the figure
        :type canvas_size: tuple

        :param min_val: minimum value for the color bar by default set to none and will be calculated during plotting
        :type min_val: float

        :param max_val: maximum value for the color bar by default set to none and will be calculated during plotting
        :type max_val: float 

        :param projection_2D: this tells if we want to project the plot in the xy plane for 2D mapping
        :type projection_2D: bool 

        :param projection_type: allows for all of the different types of projections 
        :type projection_type: "steriographic" or "equal_angle" or "equal_area"

        :param cwheel_shadding: allows for different visual of the color ball and wheel, the sqrt version is to make the dark area easyer to read 
        :type cwheel_shadding: "classic" or "none" or "full" or "full_sqrt" or "full_sqrt"

        :param cwheel_symetry: allows to chose if we want a symmetric or not colorwheel
        :type cwheel_symetry: bool 

        :param projection_base: bases from which the angles are calculated for vector plot, usfull if we want to change the orientation of the cwheel
        :type projection_base: np.array

        :param cbar_symmetric: tells if we want the color bar to have symmetric values arround 0, usfful when using a diverging colormap
        :type cbar_symmetric: bool

        """

        if cmap in plt.colormaps() or cmap=="ige":
            self.cmap=cmap
        else :
            print("Color map not valid")
            return False

        
        self.plot_name=""

        self.axis_width=3

        self.min_val=min_val
        self.max_val=max_val

        self.cbar_symmetric=cbar_symmetric

        self.bgcolor=bgcolor

        self.projection_type=projection_type # steriographic,equal_angle,equal_area
        self.cwheel_shadding=cwheel_shadding #classic, none, full
        self.cwheel_symetry=cwheel_symetry  # True or False

        self.projection_base=projection_base

        self.canvas = scene.SceneCanvas(keys='interactive', bgcolor=self.bgcolor,show=True)
        self.canvas.size=canvas_size
        self.grid = self.canvas.central_widget.add_grid(margin=10)

        self.view = self.canvas.central_widget.add_view()

        self.projection_2D=projection_2D

        if self.projection_2D==True:
            self.view.camera = scene.PanZoomCamera(aspect=1)
            gloo.set_state(depth_test=True)
        else:
            self.view.camera = "arcball"

        self.grid.add_widget(self.view,row=0,col=0)

        self.camera_range_set=False

        self.ige_cmap= [(0.06274, 0.423529, 0.717647),(0.0, 0.3411764705882353, 1.0), (0.0, 0.8235294117647058, 1.0),(0.0, 1.0, 0.6588235294117647),(0.0, 1.0, 0.17647058823529413),(0.5176470588235295, 1.0, 0.0), (1.0, 0.9647058823529412, 0.0),(1.0, 0.4823529411764706, 0.0),(1.0, 0.0, 0.0),(0.5, 0.0, 0.0)]
        self.ige_cmap_level=100

        self.ctrl=0
        self.lines=[]
        self.lines_colors=[]

        pass

    def set_camera_range(self,mesh):
        """
        This function sets the range of the camera in function of the mesh so that every point is in frame

        :param mesh: Mesh object from icetrackpy
        :type mesh: Mesh.object

        """
        vertices=np.array(mesh.points,dtype=np.float32)
        self.view.camera.set_range(x=[np.min(vertices.T[0]),np.max(vertices.T[0])],y=[np.min(vertices.T[1]),np.max(vertices.T[1])],z=[np.min(vertices.T[2]),np.max(vertices.T[2])])
        self.camera_range_set=True
        pass

    def mesh(self,mesh,mesh_color="black",mesh_width=0.1):
        """
        This function plots the mesh frame, it only plots the top and the bottom surface of the mesh for better visibility 

        :param mesh: Mesh object from icetrackpy
        :type mesh: Mesh.object

        :param mesh_color: color of the mesh line, by default it is "black"
        :type mesh_color: str

        :param mesh_width: size of the mesh line by default set to 0.1  
        :type mesh_width: float

        """
        self.mesh_color=mesh_color
        self.mesh_width=mesh_width
        self.plot_mesh=mesh
        vertices=np.array(mesh.points,dtype=np.float32)
        if self.projection_2D==True:
            vertices=np.array([[elt[0],elt[1],0] for elt in vertices])
        faces=np.array(mesh.triangles,dtype=np.uint32)
        nf = len(faces)
        mask = np.tile([0, 1, 1, 2, 2, 0], nf) + np.repeat(np.arange(0, nf * 3, 3), 6)
        edges = faces.reshape(-1)[mask]
        edge_visual = scene.visuals.Line(pos=vertices[edges], connect="segments", color=self.mesh_color,width=self.mesh_width) # visiual entity of the mesh
        self.view.add(edge_visual) # mesh additiion to the view
        self.set_camera_range(mesh) # we set the camera range
        pass

    def mesh_full(self,mesh,mesh_color=(1, 1, 1, 0.2),mesh_width=0.1):
        """
        This function plots the mesh frame, it only plots the top and the bottom surface of the mesh for better visibility it plot a background of the mesh to mask goegraphic data 

        :param mesh: Mesh object from icetrackpy
        :type mesh: Mesh.object

        :param mesh_color: color of the mesh line, by default it is "black"
        :type mesh_color: str

        :param mesh_width: size of the mesh line by default set to 0.1  
        :type mesh_width: float

        """
        self.plot_mesh=mesh
        start_points_list=mesh.points
        triangle_list=mesh.triangles
        if self.projection_2D==True:
            start_points_list=[[elt[0],elt[1],2] for elt in start_points_list] #making shure the layering is ok
        
        mesh_face=scene.visuals.Mesh(np.array(start_points_list),np.array(triangle_list),color=mesh_color)
        self.view.add(mesh_face)
        pass


    def find_min_max(self,liste_particule_data,plot_field):
        """
        This function sets the min and max value of the color bar if the value has not been defined by the user to th value minimum and maximum  value of the given field
        
        :param liste_particule_data: list of all the particule dictionnaries
        :type liste_particule_data: list(dict)

        :param plot_field: name of the filed we want to set the maximum and minimum value to
        :type plot_field: str 
        
        """

        if self.max_val==None:
            self.max_val=np.max([max(liste_particule_data[i][plot_field]) for i in range(len(liste_particule_data))])

        if self.min_val==None:
            self.min_val=np.min([min(liste_particule_data[i][plot_field]) for i in range(len(liste_particule_data))])

        if self.max_val==self.min_val:
            self.max_val+=1

        if self.cbar_symmetric==True:
            self.max_val=max(abs(self.max_val),abs(self.min_val))
            self.min_val=-self.max_val
        
        pass

    def particule_scalar(self,liste_particule_data, plot_field,linewidth=10):
        """
        This function plot the particule path with the data plotted as the color of the path

        :param liste_particule_data: list of all the particule dictionnaries
        :type liste_particule_data: list(dict)

        :param plot_field: name of the filed we want to plot allong the flow line
        :type plot_field: str 

        :param linewidth: size of the lien of the particule, set to default to 10
        :type linewidth: float 

        """
        self.list_particule=liste_particule_data
        self.linewidth=linewidth
        self.plot_name=plot_field
        Plot3Dline = scene.visuals.create_visual_node(visuals.LinePlotVisual)
        if self.cmap!="ige":
            cmap=plt.colormaps.get_cmap(self.cmap)
        elif self.cmap=="ige": 
            cmap=colorm.LinearSegmentedColormap.from_list("ige_color_map",self.ige_cmap, N=self.ige_cmap_level)

        self.find_min_max(liste_particule_data,plot_field)
        

        for num_particule in range(len(liste_particule_data)):
            if "path" in liste_particule_data[num_particule]:
                pos=liste_particule_data[num_particule]["path"]
                if self.projection_2D==True:
                    pos=[[elt[0],elt[1],2] for elt in pos] #making shure the layering is ok

                nb_time_step=len(liste_particule_data[num_particule]["path"])
                col=[]
                for i in range(nb_time_step):
                    val=liste_particule_data[num_particule][plot_field][i]
                    col.append(cmap((val-self.min_val)/(self.max_val-self.min_val)))

                Line=Plot3Dline(pos, width=self.linewidth, color=col,marker_size=0)
                self.view.add(Line)
                self.lines.append(Line)
                self.lines_colors.append(col)

        pass

    def surface_scalar(self,start_points_list:list,triangle_list:list,liste_particule_data:list,plot_field="is_inside",time_step=-1):
        """
        This function plots a surface of scalars values on the surface of the mesh and interpolates between each nodes 

        :param start_points_list: list of points that represent a node of the mesh on which we kown the value
        :type start_points_list: list 

        :param triangle_list: list of trizngles that represent the mesh on which we plot everything
        :type triangle_list: list

        :param liste_particule_data: list of all the particule dictionnaries
        :type liste_particule_data: list

        :param plot_field: field that we went to represent 
        :type plot_field: str

        :param time_step: time moment at wich we want to plot the value on the mesh 
        :type time_step: int 

        """
        
        if self.cmap!="ige":
            cmap=plt.colormaps.get_cmap(self.cmap)
        elif self.cmap=="ige": 
            cmap=colorm.LinearSegmentedColormap.from_list("ige_color_map",self.ige_cmap, N=self.ige_cmap_level)
        self.cmap_real=cmap

        self.find_min_max(liste_particule_data,plot_field)
        self.plot_name=plot_field

        if self.projection_2D==True:
            start_points_list=[[elt[0],elt[1],1] for elt in start_points_list] #making shure the layering is ok
        v_color=[ cmap((particule[plot_field][time_step]-self.min_val)/(self.max_val-self.min_val)) for particule in liste_particule_data]
        mesh_face=scene.visuals.Mesh(np.array(start_points_list),np.array(triangle_list),vertex_colors=v_color)
        self.view.add(mesh_face)

        pass

    def angle_from_vect(self,vect):
        """
        This function calculates the rotations that are needed to to convert a vector to the bases represented by the value Mesh.projection_base

        :param vect: vector for which we want to know the angles
        :type vect: np.array or lsit 

        :retrun: returns the angles of the roation in radiants
        :rtype: float,float

        """
        vect=np.array(vect)/la.norm(vect)

        psi=np.arccos(self.projection_base[2].T @ vect)

        vect=vect - (self.projection_base[2].T @ vect)*vect

        if la.norm(vect)!=0:
            vect=np.array(vect)/la.norm(vect)
            theta=np.arctan2(self.projection_base[1].T @ vect,self.projection_base[0].T @ vect)
        else:
            theta=0

        if psi >np.pi/2:
            psi =np.pi-psi
            if theta < 0 :
                theta =np.pi+theta
            elif theta > 0 :
                theta=-np.pi+theta

        return theta,psi



    def angles_to_color(self,angles):
        """
        This function converts 2 angles and gives the color corresponding on the color wheel chosen 

        :param angles: the 2 angles for which we want to know the color
        :type angles: list or array

        :return: returs an array of the color in the rbg space
        :rtype: np.array
        """

        angles=np.array(angles)
        if self.cwheel_symetry ==False:
            h=(angles[0]+np.pi)/(2*np.pi)
        if self.cwheel_symetry ==True:
            if angles[0]>=0:
                h=(np.pi-angles[0])/(np.pi)

            if angles[0]<0:
                h=(-angles[0])/(np.pi)

        if self.projection_type == "steriographic":
            sv=np.tan(angles[1]/2)
        elif self.projection_type == "equal_angle":
            sv=angles[1]*2/np.pi
        elif self.projection_type == "equal_area":
            sv=np.sqrt(2)*np.sin(angles[1]/2)
        else :
            print("projection_type does not exists")


        if self.cwheel_shadding=="full":
            s=min(2-2*sv,1)
            v=min(2*sv,1)
        elif self.cwheel_shadding=="full_sqrt":
            s=np.sqrt(min(2-2*sv,1))
            v=np.sqrt(min(2*sv,1))
        elif self.cwheel_shadding=="classic":
            s=1
            v=sv
        elif self.cwheel_shadding=="classic_sqrt":
            s=1
            v=np.sqrt(sv)
        elif self.cwheel_shadding=="none":
            s=1
            v=1
        else :
            print("cwheel_shadding does not exists")

        return colorsys.hsv_to_rgb(h,s,v)

    def particule_vector(self,liste_particule_data, plot_field,linewidth=10):
        """
        This function plots the flow line with the data of a vector 

        :param liste_particule_data: list of dictionnary of the particule data  to plot
        :type liste_particule_data: list

        :param plot_field: field that we want to plot, this should be a vector field 
        :type: str

        :param linewidth: size of the line 
        :type linewidht: float

        """
        self.list_particule=liste_particule_data
        self.linewidth=linewidth
        self.plot_name=plot_field
        Plot3Dline = scene.visuals.create_visual_node(visuals.LinePlotVisual)
        # self.find_min_max(liste_particule_data,plot_field)

        for num_particule in range(len(liste_particule_data)):
            if "path" in liste_particule_data[num_particule]:
                pos=liste_particule_data[num_particule]["path"]
                if self.projection_2D==True:
                    pos=[[elt[0],elt[1],2] for elt in pos] #making shure the layering is ok
                nb_time_step=len(liste_particule_data[num_particule]["path"])
                col=[]
                for i in range(nb_time_step):
                    val=self.angle_from_vect(liste_particule_data[num_particule][plot_field][i])
                    col.append(self.angles_to_color(val))

                Line=Plot3Dline(pos, width=self.linewidth, color=col,marker_size=0)
                self.view.add(Line)
                self.lines.append(Line)
                self.lines_colors.append(col)
        pass

    def surface_vector(self,start_points_list,triangle_list,liste_particule_data,plot_field,time_step=-1):
        """
        This function plots a surface of vectors values on the surface of the mesh and interpolates between each nodes 

        :param start_points_list: list of points that represent a node of the mesh on which we kown the value
        :type start_points_list: list 

        :param triangle_list: list of trizngles that represent the mesh on which we plot everything
        :type triangle_list: list

        :param liste_particule_data: list of all the particule dictionnaries
        :type liste_particule_data: list

        :param plot_field: field that we went to represent this should be a vector field it should be a normalised vector of dimention 3
        :type plot_field: str

        :param time_step: time moment at wich we want to plot the value on the mesh 
        :type time_step: int 
        
        """
        if self.projection_2D==True:
            start_points_list=[[elt[0],elt[1],1] for elt in start_points_list] #making shure the layering is ok
        v_color=[ self.angles_to_color(self.angle_from_vect(particule[plot_field][time_step])) for particule in liste_particule_data]
        mesh_face=scene.visuals.Mesh(np.array(start_points_list),np.array(triangle_list),vertex_colors=v_color)
        self.view.add(mesh_face)
        self.plot_name=plot_field
        pass

    def cbar(self,title="",number_of_digits=4):
        """
        This function prints the color bar on the right side of the plot 

        :param title: title name for the plot, if not defined it will be the data that is plotted 
        :type title: str

        :param number_of_digits: number of decimals that we keep on the displayed values
        :type number_of_digits: int 
        
        """

        cbar_view=scene.ViewBox(parent=self.canvas.scene, border_color=None)
        cbar_view.size = (50,self.canvas.size[1]/1.5)

        if title=="":
            title=self.plot_name

        if self.min_val==None or self.max_val==None :
            print("Min or Max vals not defined")
            self.min_val=0
            self.max_val=1

        cbar_widget = scene.ColorBarWidget(label=title, clim=(round(self.min_val,number_of_digits), round(self.max_val,number_of_digits)),cmap=self.cmap, orientation="right",border_width=1)
        cbar_widget.width_max=150
        cbar_widget.label.font_size = 15
        
        cbar_view.add_widget(cbar_widget)
        # self.grid.add_widget(cbar_widget,row=0,col=1)

        def update_position(event):
            cbar_view.size = (50,self.canvas.size[1]/1.5)
            cbar_view.transform = visuals.transforms.STTransform(translate=(self.canvas.size[0]-100,(self.canvas.size[1]-self.canvas.size[1]/1.5)/2))
        self.canvas.events.resize.connect(update_position)
        update_position(None)
        pass

    def cwheel(self,size=75,border_color=None):
        """
        This function plots the colorwheel at the bottom right of the plot

        :param size: size of the view box of the colorwheel
        :type size: int

        :param border_color: color of the border of the colorwheel  by default set to None
        :type border_color: str

        """

        # cwheel_view=self.grid.add_view(row=0,col=1)
        cwheel_view=scene.ViewBox(parent=self.canvas.scene, border_color=border_color)
        cwheel_view.size = (2*size, 2*size)

        img=np.zeros([2*size,2*size,4])

        for x in range(2*size):
            for y in range(2*size):
                if (x-size)**2+(y-size)**2<(size)**2:
                    r=(np.sqrt((x-size)**2+(y-size)**2)/size)
                    theta=r*np.pi/2
                    if r!=0:
                        phi=-np.arctan2((x-size)/r,(y-size)/r)
                    else :
                        phi=0

                    vect=[np.cos(phi)*np.sin(theta),np.sin(phi)*np.sin(theta),np.cos(theta)]

                    img[x,y,:3]=self.angles_to_color(self.angle_from_vect(vect))
                    img[x,y,3]=1

        for x in range(size//2):
            for y in range(self.axis_width):
                img[-1-x,y,:3]= colorm.to_rgb("green")
                img[-1-x,y,3]=1
        for y in range(size//2):
            for x in range(self.axis_width):
                img[-1-x,y,:3]= colorm.to_rgb("red")
                img[-1-x,y,3]=1

        for x in range(self.axis_width):
            for y in range(self.axis_width):
                img[-1-x,y,:3]= colorm.to_rgb("blue")
                img[-1-x,y,3]=1

        img=np.array(img,dtype="float32")

        scene.visuals.Image(img, interpolation='nearest',
                            parent=cwheel_view, method='subdivide')
        def update_position(event):
            cwheel_view.transform = visuals.transforms.STTransform(translate=(self.canvas.size[0]-2*size-10,self.canvas.size[1]-2*size-10))
        self.canvas.events.resize.connect(update_position)
        update_position(None)

        pass

    def cquarter(self,size=150,border_color=None):
        """
        This function plots the quarter of a colorwheel at the bottom right of the plot

        :param size: size of the view box of the colorwheel
        :type size: int

        :param border_color: color of the border of the colorwheel  by default set to None
        :type border_color: str

        """

        # cwheel_view=self.grid.add_view(row=0,col=1)
        cwheel_view=scene.ViewBox(parent=self.canvas.scene, border_color=border_color)
        cwheel_view.size = (1*size, 1*size)

        img=np.zeros([2*size,2*size,4])

        for x in range(2*size):
            for y in range(2*size):
                if (x-size)**2+(y-size)**2<(size)**2:
                    r=(np.sqrt((x-size)**2+(y-size)**2)/size)
                    theta=r*np.pi/2
                    if r!=0:
                        phi=-np.arctan2((x-size)/r,(y-size)/r)
                    else :
                        phi=0

                    vect=[np.cos(phi)*np.sin(theta),np.sin(phi)*np.sin(theta),np.cos(theta)]
                    img[x,y,:3]=self.angles_to_color(np.array(self.angle_from_vect(vect))*1)
                    img[x,y,3]=1

        # for x in range(size//2):
        #     for y in range(self.axis_width):
        #         img[-1-x,y,:3]= colorm.to_rgb("green")
        #         img[-1-x,y,3]=1
        # for y in range(size//2):
        #     for x in range(self.axis_width):
        #         img[-1-x,y,:3]= colorm.to_rgb("red")
        #         img[-1-x,y,3]=1

        # for x in range(self.axis_width):
        #     for y in range(self.axis_width):
        #         img[-1-x,y,:3]= colorm.to_rgb("blue")
        #         img[-1-x,y,3]=1


        img=np.array(img,dtype="float32")
        img=img[:size,size:,:]

        scene.visuals.Image(img, interpolation='nearest',
                            parent=cwheel_view, method='subdivide')
        def update_position(event):
            cwheel_view.transform = visuals.transforms.STTransform(translate=(self.canvas.size[0]-size-10,self.canvas.size[1]-size-10))
        self.canvas.events.resize.connect(update_position)
        update_position(None)

        pass



    def cball(self,size=200,border_color=None):
        """
        This function plots a color ball at the bottom of the frame that rotates with the motion of the main figure

        
        :param size: size of the view box of the colorball
        :type size: int

        :param border_color: color of the border of the colorball  by default set to None
        :type border_color: str

        """

        nodes=np.array([[6.123233995736766e-17,-1.499759782661858e-32,1],[6.123233995736766e-17,-1.499759782661858e-32,-1],[0.3090169943749472,-7.568733460868295e-17,-0.9510565162951536],[0.5877852522924729,-1.439658655611993e-16,-0.8090169943749476],[0.8090169943749473,-1.981520145234183e-16,-0.5877852522924734],[0.9510565162951535,-2.329416636978185e-16,-0.3090169943749477],[1,-2.449293598294706e-16,-2.449293598294706e-16],[0.9510565162951536,-2.329416636978185e-16,0.3090169943749472],[0.8090169943749476,-1.981520145234184e-16,0.5877852522924729],[0.5877852522924734,-1.439658655611994e-16,0.8090169943749472],[0.3090169943749477,-7.568733460868307e-17,0.9510565162951535],[0.1226908274643368,0.9780016036692962,-0.1687003973806862],[0.1948937990409069,0.8669629584977053,-0.4586846800235524],[0.3938985527430998,0.9022343547399705,-0.1755479970658746],[-0.7568831829665075,0.504739086036797,0.4151702089134373],[-0.5704585889194869,0.5303387618556195,0.6271505369537974],[-0.7212759179921135,0.2751786043575752,0.6356396666574946],[0.3551504135648694,0.2271912224072588,0.9067813034055112],[0.6030999233620794,0.2422198587075593,0.760000014794963],[0.6794864645820838,0.2584963202171561,-0.6866424083057572],[0.5030393903652973,0.226681849846265,-0.8340064212529693],[-0.2595917665691817,-0.4730059549119344,-0.8419486215603305],[-0.2514534430327692,-0.2208641800996514,-0.9423323086554311],[-0.45009282368703,-0.2921765254617925,-0.8438301535466191],[0.1415616570802856,-0.2619461690644693,0.9546436517136287],[0.3916345810569825,-0.2397959099647347,0.8883244207408162],[0.2750628155818377,-0.4891383465721424,0.8276980883129202],[0.1159711332390721,0.6777745649232403,0.7260663436617605],[-0.1095266507487787,0.577549039353615,0.8089752900537022],[-0.02899235556394921,0.8204075771175493,0.5710436504567458],[-0.281754102804002,0.7119399822340512,0.6432387482495833],[0.222982973834644,-0.406009708624158,-0.8862475443592409],[0.01754010453100376,-0.6501780746327882,-0.7595793678081587],[0.3561431898012214,-0.5908553321579207,-0.7239143629040415],[-0.02056808110254377,-0.3956085127415272,-0.9181889014174567],[-0.502422935579166,-0.8531352188748158,-0.1404688297076379],[-0.4423431406422774,-0.877515333535537,0.1852009323322688],[-0.2359162224838242,-0.9675335091241631,-0.09067769676632623],[-0.6962954212916298,-0.7143180657104384,0.07015972696646913],[0.2743636122575656,0.3435624470437064,-0.8981589242724923],[0.234760445317431,0.5487950309281577,-0.802316363626546],[-0.05415969206302702,0.5143418667003501,-0.8558733387101374],[0.07238447742022033,0.7031431915657103,-0.7073543239306511],[0.5046264910690912,-0.3307771946353029,-0.7974575549961862],[0.3490222726514379,-0.2189068360716175,-0.9111878238400354],[-0.5999285932451272,0.7612293122687706,0.2462023906255534],[-0.618054812351397,0.7860356516443572,-0.01249813079265709],[-0.3698727497225897,0.922418609664307,0.1110768092700598],[0.07195224741841937,0.1861247967406577,0.9798879701933724],[0.1109760515910499,0.4298655973941441,0.8960468091289853],[-0.125789289086056,0.326168108852948,0.9369052350790426],[-0.3537677018137657,0.9263917639854259,-0.1290221406324407],[0.6784689224378728,0.6871659791590868,0.259774591467921],[0.7435424265310911,0.6660660181672285,0.0591668774831853],[0.8477847925254456,0.4962898892384378,0.186968690967997],[0.5305846880668177,0.8457647849130364,0.05623003992567605],[0.8418185863139984,-0.463809622822236,-0.2760835046028581],[0.8324481200245697,-0.553742040670723,-0.01999699630902307],[0.9588773013530404,-0.2590225643361246,-0.1160242738165119],[0.685014440865419,-0.6619534250020603,-0.3042579151540338],[0.5586415483250103,-0.7143415686703387,0.4214685560687],[0.7022079461086517,-0.5160214747136286,0.4905362759839899],[0.7970599221474247,-0.5346905300129416,0.2807160800895113],[0.6644179110270789,0.7278479241331679,-0.1696650784382306],[0.8630981181235086,0.502657034425021,-0.04896472439115021],[-0.09649001372309859,0.9584978878893619,-0.2682750010033524],[-0.3515299264345326,0.4906638808085202,0.7972927109229423],[0.4509980001352629,-0.6219896125351404,0.640101340236356],[0.2287961163921801,-0.7179733601107451,0.6573937870828582],[-0.4630258702957335,-0.7324192339220389,0.4991684176887919],[-0.2159772292039457,-0.8756780136331492,0.4319049118786314],[-0.7122972573029958,-0.6050701928720544,0.3557002655838264],[0.01646926967377085,-0.6495763571794341,0.7601179641015655],[0.9567975562890575,0.2720151024122813,0.1026947921704112],[-0.87201590508937,-0.4728126862215711,-0.1266350070837803],[-0.890655415124694,-0.258380151221256,-0.3741291608040461],[-0.967657513363497,-0.1835432912617906,-0.1730629858287523],[-0.2483197148250676,0.3427351778665017,-0.9060187178430777],[-0.2266156848663494,0.0840102463256312,-0.9703543733527779],[0.02017564326463843,0.2370829899136059,-0.971279876921417],[0.8904384981281259,-0.233924137601474,-0.3903828619429063],[0.728499259360282,-0.2650584404599931,-0.6316904718708656],[0.734707677924419,-0.4827326065356715,-0.4766275889897528],[-0.5448657826419356,0.2949894980622199,0.784921954680203],[-0.6128402238323857,0.02128546432228586,0.789920115620343],[-0.8075317950149676,-0.004448038624029646,0.5898072693620379],[-0.5450639174450728,-0.7241826628537984,-0.4224509400171961],[-0.2707493625349314,-0.8781958558321769,-0.3942928118620968],[-0.3022001179256139,-0.7253782128929375,-0.6184670864209294],[-0.03453493697558777,-0.8443624416621164,-0.5346582134770599],[0.4966051112852714,-0.6927877977928034,-0.5229038445782752],[0.5954172269288299,-0.4889279061935127,-0.6375169240275977],[0.226927027065869,-0.7772045074600518,-0.5869048287165687],[-0.5290657149373634,0.7148574261340832,0.4572399037472236],[-0.7617718637071176,-0.5156078937574851,-0.3922398852222385],[-0.7097483000771503,-0.672880573642579,-0.2085403657617131],[-0.8950545821227861,0.2410154191707571,0.3752184200475092],[0.07355231980553767,-0.9970492869905915,0.02197215425890298],[-0.1378530030389679,-0.9802635979078947,0.1417033103699689],[0.1042696218029086,-0.9516147817891031,0.2890625417613915],[0.3550377929954794,0.4624615861106981,0.812451504347722],[0.5507367086699301,0.4726934121315947,0.6879316941752306],[-0.9373338777370427,-0.06755278211695924,0.3418213323868147],[0.3796372927738158,0.6925574906116539,-0.6133837690492445],[-0.1275230830461097,0.8643182507518399,-0.4865098402989341],[-0.6314531911983151,0.160464972539618,-0.7586289342711123],[-0.434545163402865,0.4635495784957321,-0.7721996433822061],[-0.7190062189218748,0.3941059984470947,-0.5724600589907455],[-0.2023130808813671,0.04104049843258009,0.9784605739592652],[-0.391473571585165,0.1648653395382451,0.9052998743893063],[-0.4003393269643994,-0.2494585862970059,0.881758944955134],[0.2722320201860478,0.7933589832666748,0.5444917362600569],[0.5802446947742117,0.6352691561772756,0.5096559559116268],[0.39953131821493,0.6342414211061382,0.6619007066914996],[-0.8630205997883014,-0.4870501537922705,0.1340805430775327],[-0.9899914971431509,0.0672826670912413,-0.1240559482384933],[-0.941394835579654,0.3270338407296813,-0.08261132223581419],[-0.9811683681802742,0.1537503468037412,0.1169164836119667],[-0.9773898107904866,-0.2003108566233474,0.06771054926492277],[-0.8016126664055023,-0.3096072684055536,0.5114298313637095],[-0.6745339111911073,-0.2140160005538874,0.7065416860738974],[-0.7985537460424852,0.5781893890230568,-0.1673587317789497],[-0.5557927354374063,0.7931918257075451,-0.2488798161075681],[-0.8522168939527958,0.510797738959312,0.1131902625471996],[0.4998892446231781,0.4615229914190423,-0.7328760273754948],[-0.4279920292975438,-0.03209869635614639,-0.9032123208581725],[-0.6417324398533908,-0.1593922091037966,-0.7501823773702136],[-0.2798041963653372,-0.6208047662406582,0.7323326115292351],[-0.07802846111095191,-0.4400578493025214,0.894572886088046],[-0.5907400084528518,-0.4782983800894087,0.6498129746449914],[0.6403526499677611,0.5480147637679472,-0.5381712574744466],[-0.257751382102177,0.7061981387755553,-0.6594303707097203],[-0.4439296406204817,0.236424539556385,-0.8643089212047514],[0.2057840116292393,0.974228532142705,0.09236724374391353],[-0.09051748293022745,0.9763747556133361,0.1962114213927774],[-0.1008001632395245,0.9945086717990054,-0.02813945286360995],[0.08920504256754096,0.9250221035245537,0.3692919825443453],[0.2861423102596496,-0.9497677293071501,-0.1267431996045958],[0.4058269191268533,-0.8937741705582883,0.1909770765166597],[0.5911377662627161,-0.8041206690054592,-0.0628179192280304],[0.6634829225557737,-0.7299806703123664,0.1640689868535722],[-0.8290349873132904,0.0287208859160545,-0.558458682914546],[-0.8900736243437565,0.2966424884308456,-0.3460811715553521],[-0.9476123104260672,-0.008652256710825932,-0.319305570860862],[-0.6895427829109039,0.6023198431242339,-0.4021710545455557],[0.8530856313327233,-0.2933651434877883,0.4314878888219671],[0.7826998999952033,0.4927760879660947,0.3802007281375594],[0.4734965270144979,-0.8210245786760867,-0.318935228714175],[0.4154246366029044,0.843431128637453,0.3406556950187344],[-0.2765833248134272,0.8839779744271328,0.3769411163071144],[0.7877794837393283,0.541056563039358,-0.2943828809415295],[0.8541627841695496,0.2845339548924225,-0.4352543700561779],[0.9467485296787828,0.2729030464313565,-0.1708541740770413],[0.1454207992936509,-0.1243472819326012,-0.9815246021413676],[0.1262702708030557,-0.2713436606301144,-0.9541637367596676],[-0.5698177192391972,-0.4826589749398174,-0.6650925354799014],[-0.1215501615679655,-0.1825843796405033,0.9756477348582807],[-0.7712089066604123,-0.2897849367464771,-0.5667993584351447],[0.2952974078681786,-0.8311235192524529,0.4712039225767886],[-0.00506322251228107,-0.9618775193484865,-0.2734337242364101],[0.2523147947095926,-0.8821831419952242,-0.3976055185106092],[0.5359638066300506,0.7493383148458338,-0.3888893002984675],[0.9515042911311909,-0.2697288440670835,0.1479389557796217],[0.7503065484831063,0.3319957303160341,0.5716807836155517],[0.01939298299302567,-0.8249691138705499,0.5648449994204356],[-0.3051168255887924,0.8963969073357337,-0.3215218612498229],[0.6163842751274176,-0.315047976888371,0.7216752715967151],[-0.429658082101959,0.7714983852367649,-0.4692378651190997],[-0.8906566702114455,-0.3440055159447911,0.2973060726043427],[-0.5421707553235996,0.60171156229053,-0.5865100748305413],[-0.0648894565704415,-0.1606885230822799,-0.9848698172731402],[0.8948386613989499,0.2647170408036803,0.3594282381420223]])

        triangles=np.array([[11, 13, 12], [14, 16, 15], [9, 18, 17], [9, 17, 10], [3, 20, 19], [3, 19, 4],[21, 23, 22], [24, 26, 25], [24, 25, 10], [27, 29, 28], [28, 29, 30], [31, 33, 32], [31, 32, 34], [35, 37, 36], [35, 36, 38], [39, 41, 40], [40, 41, 42], [31, 44, 43], [43, 44, 3], [45, 47, 46], [17, 49, 48], [48, 49, 50], [46, 47, 51], [52, 54, 53], [52, 53, 55], [56, 58, 57], [56, 57, 59], [60, 62, 61], [53, 64, 63],[11, 12, 65], [28, 66, 50], [67, 26, 68], [36, 70, 69], [36, 69, 71], [68, 26,72], [73, 7, 6], [74, 76, 75], [77, 79, 78], [80, 82, 81], [80, 81, 4], [83, 16,84], [84, 16, 85], [86, 88, 87], [89, 88, 32], [33, 91, 90], [33, 90, 92], [87,88, 89], [15, 93, 14], [94, 74, 75], [74, 94, 95], [38, 74, 95], [16, 96, 85], [95, 35, 38], [97, 99, 98], [18, 101, 100], [85, 96, 102], [48, 10, 17], [2, 3, 44], [17, 18, 100], [103, 40, 42], [42, 104, 12], [105, 107, 106], [50, 108, 48], [0, 48, 108], [12, 103, 42], [84, 110, 109], [108, 109, 110], [111, 113, 112],[36, 71, 38], [71, 114, 38], [115, 117, 116], [115, 118, 117], [119, 120, 85], [121, 46, 122], [123, 46, 121], [124, 19, 20], [23, 126, 125], [50, 49, 28], [43, 33, 31], [127, 128, 110], [127, 110, 129], [10, 48, 0], [130, 124, 103], [130,19, 124], [42, 131, 104], [41, 131, 42], [78, 132, 77], [132, 78, 125], [28, 49, 27], [108, 50, 109], [100, 113, 27], [27, 49, 100], [30, 15, 66], [66, 15, 83], [133, 135, 134], [133, 134, 136], [137, 139, 138], [125, 22, 23], [138, 139, 140], [138, 97, 137], [141, 143, 142], [141, 142, 107], [144, 107, 142], [144, 142, 121], [21, 22, 34], [14, 96, 16], [22, 125, 78], [56, 80, 58], [5, 58, 80], [6, 58, 5], [79, 2, 1], [61, 62, 145], [105, 132, 125], [62, 60, 140], [141, 105,126], [126, 105, 125], [25, 9, 10], [146, 54, 52], [60, 61, 67], [56, 59, 82], [59, 147, 90], [59, 90, 82], [55, 133, 148], [133, 136, 148], [149, 29, 136], [149, 136, 134], [40, 124, 39], [115, 143, 76], [150, 152, 151], [44, 154, 153], [1, 78, 79], [39, 20, 2], [138, 140, 60], [20, 39, 124], [2, 79, 39], [23, 21, 155], [88, 155, 21], [156, 0, 108], [126, 155, 157], [126, 23, 155], [158, 60, 67], [74, 118, 76], [45, 149, 47], [11, 65, 135], [135, 65, 51], [149, 45, 93], [45, 14, 93], [159, 160, 137], [45, 46, 123], [160, 147, 137], [161, 130, 103], [43, 81, 91], [43, 91, 33], [58, 162, 57], [13, 161, 12], [116, 117, 123], [30, 66,28], [155, 88, 86], [155, 86, 94], [116, 123, 121], [124, 40, 103], [162, 62, 57], [117, 96, 123], [96, 14, 123], [12, 161, 103], [138, 60, 158], [115, 116, 142], [142, 116, 121], [57, 62, 140], [139, 57, 140], [24, 10, 0], [120, 84, 85], [156, 24, 0], [101, 163, 112], [113, 101, 112], [34, 32, 21], [70, 164, 127], [127, 69, 70], [128, 156, 110], [24, 156, 128], [41, 39, 79], [79, 77, 41], [65, 104, 165], [133, 13, 11], [135, 133, 11], [55, 53, 63], [133, 55, 13], [59, 57, 139], [63, 161, 13], [13, 55, 63], [141, 126, 157], [72, 128, 127], [72, 127, 164], [2, 20, 3], [119, 129, 120], [84, 109, 83], [101, 113, 100], [119, 71, 129], [129, 71, 69], [157, 155, 94], [37, 98, 36], [145, 166, 61], [165, 104, 167], [141, 107, 105], [89, 160, 159], [152, 5, 151], [145, 8, 166], [61, 166, 67], [67,166, 26], [63, 64, 150], [26, 128, 72], [128, 26, 24], [161, 63, 150], [105, 106, 132], [142, 143, 115], [50, 66, 109], [152, 64, 73], [83, 109, 66], [6, 152, 73], [117, 118, 102], [5, 152, 6], [118, 114, 168], [118, 168, 102], [122, 167, 144], [122, 144, 121], [110, 156, 108], [97, 138, 99], [158, 99, 138], [164, 68,72], [164, 70, 99], [106, 41, 77], [134, 47, 149], [106, 77, 132], [93, 30, 149], [68, 164, 158], [93, 15, 30], [158, 164, 99], [36, 98, 70], [98, 99, 70], [47, 135, 51], [101, 18, 163], [18, 8, 163], [120, 110, 84], [120, 129, 110], [81, 43, 3], [17, 100, 49], [4, 81, 3], [46, 51, 122], [141, 157, 75], [97, 159, 137], [37, 97, 98], [90, 147, 160], [141, 75, 143], [157, 94, 75], [119, 85, 102], [167, 104, 131], [167, 131, 169], [112, 148, 111], [148, 136, 111], [29, 111, 136], [89, 32, 92], [92, 32, 33], [147, 139, 137], [97, 37, 159], [6, 162, 58], [127, 129, 69], [111, 29, 27], [82, 90, 91], [144, 167, 169], [107, 144, 169], [149, 30, 29], [87, 159, 37], [6, 7, 162], [95, 94, 86], [95, 86, 35], [37, 35, 87],[5, 80, 4], [19, 151, 4], [151, 5, 4], [106, 131, 41], [32, 88, 21], [131, 106,169], [106, 107, 169], [54, 73, 64], [53, 54, 64], [44, 153, 2], [154, 31, 34],[166, 9, 25], [8, 9, 166], [91, 81, 82], [26, 166, 25], [44, 31, 154], [145, 62, 162], [154, 34, 170], [160, 89, 92], [8, 18, 9], [90, 160, 92], [14, 45, 123],[86, 87, 35], [52, 148, 112], [148, 52, 55], [15, 16, 83], [7, 145, 162], [171,163, 8], [163, 171, 146], [143, 75, 76], [7, 171, 8], [54, 171, 73], [146, 171,54], [171, 7, 73], [147, 59, 139], [118, 74, 114], [74, 38, 114], [170, 34, 22], [64, 152, 150], [80, 56, 82], [102, 96, 117], [145, 7, 8], [159, 87, 89], [12,104, 65], [1, 153, 170], [168, 71, 119], [102, 168, 119], [71, 168, 114], [115,76, 118], [67, 68, 158], [27, 113, 111], [135, 47, 134], [165, 122, 51], [112, 146, 52], [130, 151, 19], [151, 130, 150], [122, 165, 167], [65, 165, 51], [161,150, 130], [146, 112, 163], [1, 170, 78], [153, 154, 170], [170, 22, 78], [153,1, 2]])


        ball_view=scene.ViewBox(parent=self.canvas.scene, border_color=border_color)
        ball_view.size = (size, size)

        if self.projection_2D==True:
            ball_view.camera = scene.cameras.panzoom.PanZoomCamera(rect=(-1, -1, 1, 1),aspect=1)
            ball_view.camera.zoom(2,(-1,-1,0))
        else:
            ball_view.camera = "arcball"

        # Defininition des axes

        colors=np.array([ (self.angles_to_color(self.angle_from_vect(nodes[i]))) for i in range(len(nodes))])

        mesh = scene.visuals.Mesh(nodes, triangles, vertex_colors=colors)

        ball_view.add(mesh)
        # Fonction pour synchroniser les caméras
        def sync_cameras(event):
                ball_view.camera._quaternion = self.view.camera._quaternion
                ball_view.camera.view_changed()

        # Utiliser un timer pour périodiquement synchroniser les caméras
        # self.timer_ball = Timer(interval=0.01, connect=sync_cameras, start=True)
        if self.projection_2D==False:
            self.canvas.events.mouse_move.connect(sync_cameras)
       

        def update_position(event):
            ball_view.transform = visuals.transforms.STTransform(translate=(self.canvas.size[0]-size-10,self.canvas.size[1]-size-10))
        self.canvas.events.resize.connect(update_position)
        update_position(None)

        pass


    def axis(self,size=150,border_color=None):
        """
        This function plots the axis xyz of the main plot on the bottom let side 

        :param size: size of the view box of the axis
        :type size: int

        :param border_color: color of the border of the axis by default set to None
        :type border_color: str

        """

        axis_view=scene.ViewBox(parent=self.canvas.scene, border_color=border_color)
        axis_view.size = (size, size)

        if self.projection_2D==True:
            axis_view.camera = scene.cameras.panzoom.PanZoomCamera(rect=(-1, -1, 1, 1),aspect=1)
            axis_view.camera.zoom(4,(-1,-1,0))
        else:
            axis_view.camera = "arcball"


        # Defininition des axes
        axis_length = 1

        x_axis = scene.visuals.Arrow(pos=np.array([[-0.01*axis_length, 0, 0], [axis_length, 0, 0]]),
                            color='red', arrow_size=10, width=self.axis_width, parent=axis_view.scene)
        y_axis = scene.visuals.Arrow(pos=np.array([[0, -0.01*axis_length, 0], [0, axis_length, 0]]),
                            color='green', arrow_size=10, width=self.axis_width, parent=axis_view.scene)
        z_axis = scene.visuals.Arrow(pos=np.array([[0, 0, -0.01*axis_length], [0, 0, axis_length]]),
                            color='blue', arrow_size=10, width=self.axis_width, parent=axis_view.scene)

        # Fonction pour synchroniser les caméras
        def sync_cameras(event):
            axis_view.camera._quaternion = self.view.camera._quaternion
            axis_view.camera.view_changed()

        # Utiliser un timer pour périodiquement synchroniser les caméras
        # self.timer_axis = Timer(interval=0.01, connect=sync_cameras, start=True)
        if self.projection_2D==False:
            self.canvas.events.mouse_move.connect(sync_cameras)

        def update_position(event):
            axis_view.transform = visuals.transforms.STTransform(translate=(10,self.canvas.size[1]-size-10))

        self.canvas.events.resize.connect(update_position)
        update_position(None)

        pass

    def _download_image(self,url, output_file):
        """
        Download an image from the given URL and save it to a file.
        
        :param url: URL of the image to download.
        :param output_file: Path where the image will be saved.
        """
        try:
            # Send HTTP GET request
            response = requests.get(url, stream=True)
            response.raise_for_status()  # Check if the request was successful
            
            # Save the image to a file
            with open(output_file, 'wb') as file:
                for chunk in response.iter_content(chunk_size=8192):
                    file.write(chunk)
        
        except requests.exceptions.RequestException as e:
            print(f"An error occurred: {e}")


    def background_geotiff(self,dem_crs,image_path):
        """
        This function helps the user to plot a geotiff image in the background of the 2 plot of the glacier, the geotiff can be from any sources and in theory in any coordinate system, the image should allways be put in the right place

        :param dem_crs: this is the coordinate system that is used for the glacier dem (the one used in elmer) for example for argentirere example it is 'EPSG:27592'
        :type dem_crs: str

        :param dem_crs: this is the complete path to the geotiff file, the file should be a .tif only
        :type dem_crs: str

        """

        # we first define a funciton that reprepojects dem in  a different bases 
        def reproject_geotiff(input_file, output_file, dst_crs):
            
            # Open the input GeoTIFF file
            with rasterio.open(input_file) as src:
                # Calculate the transform and dimensions of the reprojected raster
                transform, width, height = warp.calculate_default_transform(
                    src.crs, dst_crs, src.width, src.height, *src.bounds)

                # Update the metadata for the new projection
                kwargs = src.meta.copy()
                kwargs.update({
                    'crs': dst_crs,
                    'transform': transform,
                    'width': width,
                    'height': height
                })

                # Create the output file with the updated metadata
                with rasterio.open(output_file, 'w', **kwargs) as dst:
                    for i in range(1, src.count + 1):
                        # Reproject and write each band
                        warp.reproject(
                            source=rasterio.band(src, i),
                            destination=rasterio.band(dst, i),
                            src_transform=src.transform,
                            src_crs=src.crs,
                            dst_transform=transform,
                            dst_crs=dst_crs,
                            resampling=warp.Resampling.nearest)

            return transform, width, height
        
        # we overright the initial image with the new geotiff in the same coordiante system as the one of the glacier 
        reproject_geotiff(image_path,image_path,dem_crs)

        # we open the image using pyplot and then save is a a png for easy use in vispy later
        with rasterio.open(image_path) as src:
        # Read all the bands into an array
            bands = []
            for i in range(1, src.count + 1):
                bands.append(src.read(i))

            # Stack the bands into a single array
            image_data = np.dstack(bands)

            # Normalize the data to range 0-1 for display (optional, depending on the data)
            image_data = image_data / np.max(image_data)

            # extracting the initia parameters of the geotiff to place correctly the png 
            width=abs(src.bounds.left-src.bounds.right) 
            height=abs(src.bounds.top-src.bounds.bottom)
            x_shift=src.bounds.left
            y_shift=src.bounds.top
            
            # Plot and save the PNG
            plt.figure(figsize=(50, 50))
            plt.imshow(image_data, cmap='gray')
            plt.axis('off')  # Turn off the axis
            plt.savefig(image_path[:-4]+".png", bbox_inches='tight', pad_inches=0) # saving the new png image 
            plt.close()
        
        # including the png in the visual
        img_data = io.read_png(io.load_data_file(image_path[:-4]+".png"))
        image = scene.visuals.Image(img_data, parent=self.view.scene, method='auto')
        image.transform= visuals.transforms.MatrixTransform()
        image.transform.scale((width/img_data.shape[1],-height/img_data.shape[0], 1))
        image.transform.translate((x_shift,y_shift, 2))

        pass





    def _on_mouse_click(self,event):
        """
        This function is internal is regulates when a mousse click is pressed, this is for the selection of the line 

        :param event: useless
        :type event: all

        """
        
        if event.button == 3:  # right mouse button
            # Get the mouse position
            x, y = event.pos

            # Render the pick_canvas to get the color under the mouse cursor
            self.canvas.render()
            
            img = self.pick_canvas.render(alpha=False)
            if y>img.shape[0] or x > img.shape[1]:
                return None
            color_under_mouse = img[y, x]

            if self.selected_line!=None:
                for i in range(len(self.list_particule)):
                    if i !=self.selected_line:
                        self.lines[i].set_data(color=self.lines_colors[i])

            # Find the line corresponding to the color
            selected_line_index = None
            for i, c in enumerate(self.pick_colors):
                if np.allclose(color_under_mouse , (c[:3] * 255).astype(int),atol=1):
                    selected_line_index = i
                    break

            if selected_line_index is not None:
                print(f"Selected line index: {selected_line_index}")
                self.selected_line=selected_line_index
                for i in range(len(self.list_particule)):
                    if i !=self.selected_line:
                        self.lines[i].set_data(color=(0, 0, 0,0))

        pass     



    def line_selection(self):
        """
        This function activates the mousse selection of a flow line, when the scroll wheel button is pressed on a flow line it will change its color and print id of the particule

        """

        self.selected_line=None
        nb_lines=len(self.list_particule)
        self.pick_colors = color.get_colormap('hsv').map(np.linspace(0, 1, nb_lines))
        self.pick_canvas = scene.SceneCanvas(size=self.canvas.size, show=False)
        self.pick_view = self.pick_canvas.central_widget.add_view()
        self.pick_view.camera = self.view.camera
        Plot3Dline = scene.visuals.create_visual_node(visuals.LinePlotVisual)

        for num_particule in range(nb_lines):
            if "path" in self.list_particule[num_particule]:
                pos=self.list_particule[num_particule]["path"]
                nb_time_step=len(self.list_particule[num_particule]["path"])
                col=[self.pick_colors[num_particule]]*nb_time_step

                Line=Plot3Dline(pos, width=10, color=col,marker_size=0)
                self.pick_view.add(Line)

        self.canvas.events.mouse_press.connect(self._on_mouse_click)
        pass



    def _photo_and_close(self,event):
        """
        This function is internal and closes the window 

        :param event: is usless can be anything
        :type event: all 

        """

        # img=self.canvas.render()
        img=gloo.util._screenshot()
        io.write_png(self.image_path,img)
        app.quit()
        pass

    def _on_key_press(self,event):
        """
        This function is internal and is called when a key is pressed, it saves the image when "ctrl+s" is pressed

        :param event: is usless can be anything
        :type event: all 

        """
        
        if event.key == 'Control':
            self.ctrl=1

        if event.key == 'S' and self.ctrl==1:
            if self.auto_delete==True :
                # delets old file before saving the new one
                folder_path="/".join(self.image_path.split("/")[:-1])
                file_name=self.image_path.split("/")[-1]
                files=os.listdir(folder_path)
                if file_name in files:
                    os.system("rm " +self.image_path)

            # img=self.canvas.render()
            img=gloo.util._screenshot()
            io.write_png(self.image_path,img)
            print("Screenshot taken and saved")
        
        if event.key !='Control':
            self.ctrl=0

    def show(self,path="",auto_close=False,save_fig=False,auto_delete=True,key_save=True):
        """
        This function finelizes the plot  and makes the figure appear for the user 

        :param path: full path to the image and where it needs to be saved and the extension like : "home/img.png"
        :type path: str

        :param show: tells if we want the view to automaticly close once the rendering is done 
        :type show: bool

        :param save_fig: tells if we want to save the figure or not automaticaly
        :type save_fig: bool

        :param auto_delete: this parameter tells if we want to errase the picture that has the same name when we save a new one
        :type auto_delete: bool

        :param:

        """

        self.auto_delete=auto_delete


        if path!="":
            self.image_path=path

        if path!="" and key_save==True:
            self.canvas.events.key_press.connect(self._on_key_press)

        if path!="" and save_fig==True and auto_delete==True:
            folder_path="/".join(path.split("/")[:-1])
            file_name=path.split("/")[-1]
            files=os.listdir(folder_path)
            if file_name in files:
                os.system("rm " +path)
            

        if auto_close==False:
            if self.camera_range_set==False:
                self.view.camera.set_range(x=self.view.get_scene_bounds()[0],y=self.view.get_scene_bounds()[1],z=self.view.get_scene_bounds()[2])
            app.run()

        if path!="" and save_fig==True and  auto_close==False:
            # img=self.canvas.render()
            img=gloo.util._screenshot()
            io.write_png(path,img)

        if auto_close==True and save_fig==True and path!="":
            timer = app.Timer(interval=0.1, start=True, iterations=1, connect=self._photo_and_close)
            if self.camera_range_set==False:
                self.view.camera.set_range(x=self.view.get_scene_bounds()[0],y=self.view.get_scene_bounds()[1],z=self.view.get_scene_bounds()[2])
            app.run()

        pass




