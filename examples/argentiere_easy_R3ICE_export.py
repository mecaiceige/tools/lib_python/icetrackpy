from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import numpy as np
import os


path=os.path.dirname(os.path.abspath(__file__))

glacier=thg.Glacier()
glacier.vtu(path,file_name="mesh")


def creat_save():
    my_mesh=it.Mesh(path+"/"+"argentiere.vtu")
    my_mesh.invert_mesh()
    my_mesh.extract_data()

    start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max",z_min=2300,z_max=2700)

    tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz","sxx","syy","szz","sxy","sxz","syz"],dt=-0.5,start_acu_level=2800,n=1000,u_min=0.01)
    tracker.compute_path(start_points_list)
    tracker.time_inversion()
    [particule.update({"1":[1]*len(particule["time_step"])}) for particule in tracker.particule]
    tracker.time_int_over_path("1")
    tracker.compute_rotation_matrix()
    tracker.compute_in_particule_coord(tensor_type="s")
    tracker.compute_in_particule_coord(tensor_type="e")
    tracker.compute_strain("Le")
    tracker.save_tracking(path=path,name="save_tracking")
    
    
# creat_save()

my_mesh=it.Mesh()
my_mesh.from_save(folder_path =path+"/save_tracking")

start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max",z_min=2300,z_max=2700)

tracker=it.ParticuleTracker(my_mesh)
tracker.from_save(folder_path= path+"/save_tracking")
tracker.compute_deformation_eigval("eigvals_ILe")
tracker.compute_relative_anisotropy("deformation_eigval")
tracker.compute_fractional_anisotropy("deformation_eigval")
tracker.compute_volume_ratio_anisotropy("deformation_eigval")
tracker.compute_flatness_anisotropy("deformation_eigval")
tracker.compact_tensor("e")
tracker.diag_tensor_field("e")

print(tracker)

plot=it.Plot(cmap="jet",projection_2D=True,cbar_symmetric=False)
plot.mesh(my_mesh)
plot.particule_scalar([tracker.particule[323],tracker.particule[359]],"fractional_anisotropy")
plot.cbar()
plot.axis()
plot.show(path=path+"/img_fractional_anisotropy.png",auto_close=True,save_fig=True)

plot=it.Plot(cmap="jet",projection_2D=True,cbar_symmetric=False)
plot.mesh(my_mesh)
plot.particule_scalar([tracker.particule[323],tracker.particule[359]],"relative_anisotropy")
plot.cbar()
plot.axis()
plot.show(path=path+"/img_relative_anisotropy.png",auto_close=True,save_fig=True)

plot=it.Plot(cmap="jet",projection_2D=True,cbar_symmetric=False)
plot.mesh(my_mesh)
plot.particule_scalar([tracker.particule[323],tracker.particule[359]],"volume_ratio_anisotropy")
plot.cbar()
plot.axis()
plot.show(path=path+"/img_volume_ratio_anisotropy.png",auto_close=True,save_fig=True)

plot=it.Plot(cmap="jet",projection_2D=True,cbar_symmetric=False)
plot.mesh(my_mesh)
plot.particule_scalar([tracker.particule[323],tracker.particule[359]],"flatness_anisotropy")
plot.cbar()
plot.axis()
plot.show(path=path+"/img_flatness_anisotropy.png",auto_close=True,save_fig=True)


tracker.save_tracking(path=path,name="import_R3ICE",things_to_save=["I1","eigval_e","eigvect_e"],header=["Time (years)","Strain rate eigen values (ans-1)","Strain rate eigen vectors (s.u)"],particule_to_save=[323,359],save_mesh=False)
        


# plot.line_selection()

