from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import os

# to have the save file put this line at the end of your code : tracker.save_tracking(path=path,name="save_tracking")

path=os.path.dirname(os.path.abspath(__file__))

my_mesh=it.Mesh()
my_mesh.from_save(path+"/save_tracking")
start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max_y_min_max_x_min_max",z_min=9.5,z_max=10.5,x_min=30,x_max=50,y_min=-1.5,y_max=1.5)


tracker=it.ParticuleTracker(my_mesh)
tracker.from_save(path+"/save_tracking")

plot=it.Plot()
plot.mesh(my_mesh)
plot.particule_vector(tracker.particule,"Strain_angles")
plot.surface_vector(start_points_list,triangle_list,tracker.particule,"Strain_angles",-1)
plot.cwheel()
plot.axis()
plot.show()

