from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import numpy as np
import os


path=os.path.dirname(os.path.abspath(__file__))

final_deformation=0.25

dt=0.1
n=250

vx=-0.5*np.log(2*final_deformation+1)/(n*dt)


glacier=thg.Glacier(type="compression",vx=vx)
glacier.vtu(path,file_name="mesh")

my_mesh=it.Mesh(path+"/"+"mesh.vtu")
my_mesh.invert_mesh()
my_mesh.extract_data()

start_points_list =[[50,-1,0],[50,0,0],[50,1,0],[25,-1,0],[25,0,0],[25,1,0]]


tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=dt,n=n,u_min=0)
tracker.compute_path(start_points_list)
tracker.time_inversion()
[particule.update({"1":[1]*len(particule["time_step"])}) for particule in tracker.particule]
tracker.time_int_over_path("1")
tracker.compute_rotation_matrix()
tracker.compute_in_particule_coord("e")
tracker.compute_strain("Le")
tracker.extract_scalar("eigval_ILe")
tracker.compact_tensor("e")
tracker.diag_tensor_field("e")
tracker.save_tracking(path=path,name="import_R3ICE_dome",things_to_save=["I1","eigval_e","eigvect_e"],header=["Time (years)","Strain rate eigen values (ans-1)","Strain rate eigen vectors (s.u)"],particule_to_save=[1,0],save_mesh=False)
   

print(tracker)

plot=it.Plot(projection_2D=True)
plot.mesh(my_mesh)
plot.particule_scalar(tracker.particule,"eigval_ILe_1")
plot.cbar()
plot.axis()
plot.show(path=path+"/strain.png",auto_close=False,save_fig=True)

