from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import numpy as np
import os
from scipy import linalg as la

path=os.path.dirname(os.path.abspath(__file__))

glacier=thg.Glacier()
glacier.vtu(path,file_name="mesh")

my_mesh=it.Mesh(path+"/"+"argentiere.vtu")
my_mesh.invert_mesh()
my_mesh.extract_data()

start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max",z_min=2300,z_max=2400)


tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.5,start_acu_level=2800,n=1000,u_min=0.01)
tracker.compute_path(start_points_list)
tracker.time_inversion()
[particule.update({"1":[1]*len(particule["time_step"])}) for particule in tracker.particule]
tracker.time_int_over_path("1")

for particule in tracker.particule:
    p_eig_val=[]
    p_eig_vect=[]
    for i in range(len(particule["path"])):
        strain_tensor=np.array([[particule["exx"][i],particule["exy"][i],particule["exz"][i]],[particule["exy"][i],particule["eyy"][i],particule["eyz"][i]],[particule["exz"][i],particule["eyz"][i],particule["ezz"][i]]])
        eig_val,eig_vect=la.eigh(strain_tensor)
        p_eig_val.append(eig_val)
        p_eig_vect.append(eig_vect)
    particule.update({"e_eig_val":p_eig_val})
    particule.update({"e_eig_vect":p_eig_vect})

tracker.save_tracking(path=path,name="import_R3ICE",things_to_save=["I1","e_eig_val","e_eig_vect"],header=["Time (years)","Strain rate eigen values (ans-1)","Strain rate eigen vectors (s.u)"],particule_to_save=[0,1],save_mesh=False)
        

plot=it.Plot(cmap="brg",projection_2D=False)
plot.mesh(my_mesh)
plot.surface_scalar(start_points_list,triangle_list,tracker.particule,"I1",-1)
plot.cbar()
plot.axis()
plot.show()

