from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import os


path=os.path.dirname(os.path.abspath(__file__))

glacier=thg.Glacier()
glacier.vtu(path,file_name="mesh")
print("generating glacier done")

my_mesh=it.Mesh(path+"/"+"mesh.vtu",compiled=True,parallel=True)
my_mesh.invert_mesh()
my_mesh.extract_data()
my_mesh.compute_strain_rate()
print("test 1 done")

my_mesh=it.Mesh(path+"/"+"mesh.vtu",compiled=False,parallel=True)
my_mesh.invert_mesh()
my_mesh.extract_data()
print("test 2 done")

my_mesh=it.Mesh(path+"/"+"mesh.vtu",compiled=True,parallel=False)
my_mesh.invert_mesh()
my_mesh.extract_data()
print("test 4 done")

my_mesh=it.Mesh(path+"/"+"mesh.vtu",compiled=False,parallel=False)
my_mesh.invert_mesh()
my_mesh.extract_data()
my_mesh.compute_strain_rate()
print("test 5 done")

start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max_y_min_max_x_min_max",z_min=9.5,z_max=10.5,x_min=30,x_max=50,y_min=-1.5,y_max=1.5)


tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.1,parallel=True,compiled=True)
tracker.compute_path(start_points_list)
tracker.time_inversion()
print("test 6 done")

tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.1,parallel=True,compiled=False)
tracker.compute_path(start_points_list)
tracker.time_inversion()
print("test 7 done")

tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.1,parallel=False,compiled=True)
tracker.compute_path(start_points_list)
tracker.time_inversion()
print("test 8 done")

tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.1,parallel=False,compiled=False)
tracker.compute_path(start_points_list)
tracker.time_inversion()
print("test 9 done")

print("Test successful")

