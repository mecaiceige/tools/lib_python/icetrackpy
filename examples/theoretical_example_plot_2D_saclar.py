from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import os
import time

path=os.path.dirname(os.path.abspath(__file__))

glacier=thg.Glacier(mesh_factor=[1,1,1])
glacier.vtu(path,file_name="mesh")

print("Importing mesh")
my_mesh=it.Mesh(path+"/"+"mesh.vtu",parallel=False)
print("Inverting mesh")
my_mesh.invert_mesh()
print("Extracting nodal data")
my_mesh.extract_data()
print("Computing strain rate")
t=time.time()
my_mesh.compute_strain_rate()
print(time.time()-t)
print("End of mesh 1")



start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max_y_min_max_x_min_max",z_min=9.5,z_max=10.5,x_min=30,x_max=50,y_min=-1.5,y_max=1.5)


tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.1)
tracker.compute_path(start_points_list)
tracker.time_inversion()


plot=it.Plot(projection_2D=True)
plot.surface_scalar(start_points_list,triangle_list,tracker.particule,"eyy",-1)
plot.cbar()
plot.axis()
plot.show()

