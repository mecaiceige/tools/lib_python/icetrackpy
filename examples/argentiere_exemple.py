from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import os


path=os.path.dirname(os.path.abspath(__file__))

glacier=thg.Glacier()
glacier.vtu(path,file_name="mesh")

my_mesh=it.Mesh(path+"/"+"argentiere.vtu")
my_mesh.invert_mesh()
my_mesh.extract_data()

start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max",z_min=2300,z_max=2400)


tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.5,start_acu_level=2800,n=100,u_min=0.01)
tracker.compute_path(start_points_list)
tracker.time_inversion()
[particule.update({"1":[1]*len(particule["time_step"])}) for particule in tracker.particule]
tracker.time_int_over_path("1")
tracker.compute_rotation_matrix()
tracker.compute_dev("e")
tracker.compute_in_particule_coord("De")
tracker.compute_strain("LDe")
# [particule.update({"Strain_angles": [np.array(((particule["rotation_matrix"][i].T @ particule["eigvect_ILDe"][i])[:,2])) for i in range(len(particule["path"]))] }) for particule in tracker.particule]
tracker.compute_deformation_eigval("eigval_ILDe")
tracker.compute_flinn("deformation_eigval",top_cutoff=4)

plot=it.Plot(cwheel_shadding="classic_sqrt",cwheel_symetry=True,projection_2D=True)


# plot.mesh_full(my_mesh)
plot.mesh(my_mesh)
# plot.particule_vector(tracker.particule,"flinn_vector")
plot.surface_vector(start_points_list,triangle_list,tracker.particule,"flinn_vector",-1)
# plot.cball()
# plot.cwheel()
plot.background_geotiff(dem_crs='EPSG:27592',image_path=path+"/argentiere.tif")
plot.cquarter()
plot.axis()
plot.show()
