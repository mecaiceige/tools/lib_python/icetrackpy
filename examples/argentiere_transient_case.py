from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import os


path=os.path.dirname(os.path.abspath(__file__))

# list of the differents files for each time step in chronological order
file_list=[path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0001.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0002.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0003.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0004.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0005.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0006.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0007.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0008.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0009.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0010.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0011.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0012.pvtu",path+"/argentiere_transient/RunHistorical_MaskAccu_AsInvNew_visc_k0_36_t0013.pvtu"]

# converting pvtu to vtu file as is allows us to work on a unified mesh
file_list=it.pvtu_to_vtu(file_list)

my_mesh_list=[]

# Openning every mesh and inverting it to creat a lsit of all of the mesh 
print("Openning Meshs, inverting them extracting data")
for file in file_list:
    print("Openning mesh "+file)
    my_mesh=it.Mesh(file)
    my_mesh.invert_mesh()
    my_mesh.extract_data()
    my_mesh_list.append(my_mesh)

# we select the start point list
start_points_list,triangle_list =my_mesh_list[-1].select_points(methode="z_min_max",ice_mask=True,ice_mask_key="icymask",ice_mask_inside_val=1,z_min=2300,z_max=2350)

# we creat an instance of transient tracking with the meshes and all of the parameters (all of the parameters are the same as in it.ParticuleTracker)
tracker_transient=it.ParticuleTrackerTransient(my_mesh_list,params_to_track=[],dt=-0.0520361991,start_acu_level=2800,n=170,u_min=0.01,nb_friends_max=4,ellipse_factor=5,outside_iter=5,verbose=False)
# running the tracking algorithm an taking the result as a ParticuleTracker instance 
tracker=tracker_transient.compute_path(start_points_list)

# working exactly know like any other example
tracker.time_inversion()
tracker.compute_age()


#  converting the time to mesh_id just to see from wich file each time step is computed 
for particule in tracker.particule:
    particule["mesh_id"]=[]
    for time in particule["I1"]:
        particule["mesh_id"].append(time//(0.0520361991*170))


plot=it.Plot(projection_2D=False)
plot.mesh(my_mesh_list[0])
plot.particule_scalar(tracker.particule,"mesh_id")
plot.axis()
plot.cbar()
plot.show()

