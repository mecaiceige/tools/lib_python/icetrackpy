from icetrackpy import theoretical_glacier as thg
from icetrackpy import icetracker as it
import numpy as np
import os


path=os.path.dirname(os.path.abspath(__file__))

glacier=thg.Glacier()
glacier.vtu(path,file_name="mesh")

my_mesh=it.Mesh(path+"/"+"mesh.vtu")
my_mesh.invert_mesh()
my_mesh.extract_data()

start_points_list,triangle_list =my_mesh.select_points(methode="z_min_max_y_min_max_x_min_max",z_min=9.5,z_max=10.5,x_min=30,x_max=50,y_min=-1.5,y_max=1.5)



tracker=it.ParticuleTracker(my_mesh,params_to_track=["exx","eyy","ezz","exy","exz","eyz"],dt=-0.1)
tracker.compute_path(start_points_list)
tracker.time_inversion()
tracker.compute_rotation_matrix()
tracker.compute_dev("e")
tracker.compute_in_particule_coord("De")
tracker.compute_strain("LDe")
[particule.update({"Strain_angles": [np.array(((particule["rotation_matrix"][i].T @ particule["eigvect_ILDe"][i])[:,2])) for i in range(len(particule["path"]))] }) for particule in tracker.particule]


plot=it.Plot()


plot.mesh(my_mesh)
plot.particule_vector(tracker.particule,"Strain_angles")
plot.surface_vector(start_points_list,triangle_list,tracker.particule,"Strain_angles",-1)
plot.cball()
# plot.cwheel()
plot.axis()
plot.show()

