# Librairy icetrackpy

[![PyPI version](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/icetrackpy/-/blob/main/LICENSE)
[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_aita/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/icetrackpy/-/commits/main)
[![PyPI version](https://badge.fury.io/py/icetrackpy.svg)](https://badge.fury.io/py/icetrackpy)
[![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/icetrackpy/)


This package is meant to be use with [Elmer-ice](https://github.com/ElmerCSC/elmerfem/tree/devel) outputs files, it is capable of doing particule tracking, variable integration in a small and high deformation context, and visualisation 

## Installation

### Form pypi (Does not work yet)

Run this command in your environment :

`pip install icetrackpy'

### From main

You should clone the environnement.

`git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/icetrackpy`

Go in the folder.

`cd icetrackpy`

Creat a conda environement from teh environement file 

`conda env create -f environment.yml -n icetrackpy-env`

Activate the new environement 

`conda activate icetrackpy-env`

For mac users you need to install pyqt6

`pip install PyQt6`

Use pip to install icetrackpy.

`pip install -e .`

