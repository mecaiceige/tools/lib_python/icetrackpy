#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Note: To use the 'upload' functionality of this file, you must:
#   $ pip install twine

import io
import os
import sys
from shutil import rmtree
import numpy as np

from setuptools import find_packages, setup, Command,Extension

# Package meta-data.
NAME = 'icetrackpy'
DESCRIPTION = 'Tools to work on output from Elmer ice flow simulation'
URL = 'https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/icetrackpy'
EMAIL = 'martin.thiriet1@gmail.com'
AUTHOR = 'Martin Thiriet'
REQUIRES_PYTHON = '>=3.8'
VERSION = '0.0.4'

# What packages are required for this module to be executed?
REQUIRED = [
    'meshio','numpy','matplotlib','vispy','scipy',
]


# What packages are optional?
EXTRAS = {
}

# Define the C extension with linker arguments
MODULE = Extension(
    'icetrackpy.icetrackpy_functions',
    sources=['icetrackpy/icetrackpy_functions.c'],
    include_dirs=[
        np.get_include(),
        os.path.join(os.environ['CONDA_PREFIX'], 'include')
    ],
    library_dirs=[os.path.join(os.environ['CONDA_PREFIX'], 'lib')],
    libraries=['lapack', 'openblas'],  # Link against lapacke and blas
    # If you need to specify the directory for the libraries, you can use:
    # library_dirs=['/path/to/lapacke', '/path/to/blas']
)

# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!
# If you do change the License, remember to change the Trove Classifier for that!

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
try:
    with io.open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    with open(os.path.join(here, NAME, '__version__.py')) as f:
        exec(f.read(), about)
else:
    about['__version__'] = VERSION


class UploadCommand(Command):
    """Support setup.py upload."""

    description = 'Build and publish the package.'
    user_options = []

    @staticmethod
    def status(s):
        """Prints things in bold."""
        print('\033[1m{0}\033[0m'.format(s))

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            self.status('Removing previous builds…')
            rmtree(os.path.join(here, 'dist'))
        except OSError:
            pass

        self.status('Building Source and Wheel (universal) distribution…')
        os.system('{0} setup.py sdist bdist_wheel --universal'.format(sys.executable))

        self.status('Uploading the package to PyPI via Twine…')
        os.system('twine upload dist/*')

        self.status('Pushing git tags…')
        os.system('git tag v{0}'.format(about['__version__']))
        os.system('git push --tags')

        sys.exit()


# Where the magic happens:
setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    #dependency_links=['https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_uvecs/tarball/main#egg=xarrayuvecs'],
    ext_modules=[MODULE],
    packages=find_packages(exclude=('tests',)),
    # If your package is a single module, use this instead of 'packages':
    #py_modules=[NAME],

    #entry_points={
    #    'console_scripts': ['mycli='+NAME+':cli'],
    #},
    install_requires = REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license='GPL-3.0',
    classifiers=[
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: C'
    ],
    # $ setup.py publish support.
    cmdclass={
        'upload': UploadCommand,
    },
)
