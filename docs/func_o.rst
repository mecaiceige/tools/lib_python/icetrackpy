Functions overview
==================

Tracking of a particule and operations allong a flow line
*********************************************************
.. automodule:: icetrackpy.icetracker
    :special-members:
	:private-members:
	:undoc-members:
	:members:

Creating a simple example of a glacier 
**************************************
.. automodule:: icetrackpy.theoretical_glacier
	:special-members:
	:private-members:
	:undoc-members:
	:members:
	
Strain calculation from elementray strains
******************************************
.. automodule:: icetrackpy.diag_matrix_iterative
	:special-members:
	:private-members:
	:undoc-members:
	:members:



