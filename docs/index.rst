.. icetrackpy documentation master file, created by
   sphinx-quickstart on Mon Aug  2 09:58:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to icetrackpy documentation!
=======================================

In this documentation detail of the functions are given. 

Contact
=======
:Author: Martin Thiriet
:Contact: martin.thiriet1@gmail.com

:Organization: `IGE <https://www.ige-grenoble.fr/>`_
:Status: This is a "work in progress"


Functions Overview
==================

.. toctree::
    :maxdepth: 0
    :numbered: 
    
    func_o
